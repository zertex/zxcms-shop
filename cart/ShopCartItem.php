<?php

namespace common\modules\shop\cart;

use common\modules\shop\entities\product\ShopModification;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\helpers\ShopDiscountHelper;
use Yii;

class ShopCartItem
{
    private $_product;
    private $_modificationId;
    private $_quantity;

    public function __construct(ShopProduct $product, $modificationId, $quantity)
    {
        if (!$product->canBeCheckout($modificationId, $quantity)) {
            throw new \DomainException(Yii::t('shop_public', 'Quantity is too big.'));
        }
        $this->_product = $product;
        $this->_modificationId = $modificationId;
        $this->_quantity = $quantity;
    }

    public function getId(): string
    {
        return md5(serialize([$this->_product->id, $this->_modificationId]));
    }

    public function getProductId(): int
    {
        return $this->_product->id;
    }

    public function getProduct(): ShopProduct
    {
        return $this->_product;
    }

    public function getModificationId(): ?int
    {
        return $this->_modificationId;
    }

    public function getModification(): ?ShopModification
    {
        if ($this->_modificationId) {
            return $this->_product->getModification($this->_modificationId);
        }
        return null;
    }

    public function getQuantity(): int
    {
        return $this->_quantity;
    }

    public function getPrice(): int
    {
        if ($this->_modificationId) {
            return $this->_product->getModificationPrice($this->_modificationId);
        }
        return $this->_product->price_new;
    }

    public function getCalculatedPrice(): int
    {
        return ShopDiscountHelper::discountedPrice($this->getPrice(), $this->getProduct());
    }

    public function getWeight(): int
    {
        return $this->_product->weight * $this->_quantity;
    }

    public function getCost(): int
    {
        return $this->getPrice() * $this->_quantity;
    }

    public function getCalculatedCost(): int
    {
        return $this->getCalculatedPrice() * $this->_quantity;
    }

    public function plus($quantity)
    {
        return new static($this->_product, $this->_modificationId, $this->_quantity + $quantity);
    }

    public function changeQuantity($quantity)
    {
        return new static($this->_product, $this->_modificationId, $quantity);
    }
}
