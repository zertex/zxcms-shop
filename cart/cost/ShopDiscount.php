<?php

namespace common\modules\shop\cart\cost;

final class ShopDiscount
{
    private $_value;
    private $_name;

    public function __construct(float $value, string $name)
    {
        $this->_value = $value;
        $this->_name = $name;
    }

    public function getValue(): float
    {
        return $this->_value;
    }

    public function getName(): string
    {
        return $this->_name;
    }
}
