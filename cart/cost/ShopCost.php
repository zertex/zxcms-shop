<?php

namespace common\modules\shop\cart\cost;

final class ShopCost
{
    private $_value;
    private $_discounts = [];

    public function __construct(float $value, array $discounts = [])
    {
        $this->_value = $value;
        $this->_discounts = $discounts;
    }

    public function withDiscount(ShopDiscount $discount): self
    {
        return new static($this->_value, array_merge($this->_discounts, [$discount]));
    }

    public function getOrigin(): float
    {
        return $this->_value;
    }

    public function getTotal(): float
    {
        return $this->_value - array_sum(array_map(function (ShopDiscount $discount) {
            return $discount->getValue();
        }, $this->_discounts));
    }

    /**
     * @return ShopDiscount[]
     */
    public function getDiscounts(): array
    {
        return $this->_discounts;
    }
}
