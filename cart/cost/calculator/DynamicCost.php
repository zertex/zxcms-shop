<?php

namespace common\modules\shop\cart\cost\calculator;

use common\modules\shop\cart\cost\ShopCost;
use common\modules\shop\cart\cost\ShopDiscount as CartDiscount;
use common\modules\shop\entities\ShopDiscount as DiscountEntity;

class DynamicCost implements CalculatorInterface
{
    private $_next;

    public function __construct(CalculatorInterface $next)
    {
        $this->_next = $next;
    }

    public function getCost(array $items): ShopCost
    {
        /** @var DiscountEntity[] $discounts */
        $discounts = DiscountEntity::find()->active()->orderBy('sort')->all();

        $cost = $this->_next->getCost($items);

        foreach ($discounts as $discount) {
            if ($discount->isActive()) {
                $new = new CartDiscount($cost->getOrigin() * $discount->percent / 100, $discount->translation->name);
                $cost = $cost->withDiscount($new);
            }
        }

        return $cost;
    }
}
