<?php

namespace common\modules\shop\cart\cost\calculator;

use common\modules\shop\cart\cost\ShopCost;

class SimpleCost implements CalculatorInterface
{
    public function getCost(array $items): ShopCost
    {
        $cost = 0;
        foreach ($items as $item) {
            $cost += $item->getCost();
        }
        return new ShopCost($cost);
    }
}
