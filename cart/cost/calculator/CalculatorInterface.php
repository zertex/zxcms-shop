<?php

namespace common\modules\shop\cart\cost\calculator;

use common\modules\shop\cart\ShopCartItem;
use common\modules\shop\cart\cost\ShopCost;

interface CalculatorInterface
{
    /**
     * @param ShopCartItem[] $items
     * @return ShopCost
     */
    public function getCost(array $items): ShopCost;
}
