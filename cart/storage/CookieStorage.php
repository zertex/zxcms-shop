<?php

namespace common\modules\shop\cart\storage;

use common\modules\shop\cart\ShopCartItem;
use common\modules\shop\entities\product\ShopProduct;
use Yii;
use yii\helpers\Json;
use yii\web\Cookie;

class CookieStorage implements StorageInterface
{
    private $_key;
    private $_timeout;

    public function __construct($key, $timeout)
    {
        $this->_key = $key;
        $this->_timeout = $timeout;
    }

    public function load(): array
    {
        if ($cookie = Yii::$app->request->cookies->get($this->_key)) {
            return array_filter(array_map(function (array $row) {
                if (isset($row['p'], $row['q']) && $product = ShopProduct::find()->active()->andWhere(['id' => $row['p']])->one()) {
                    /** @var ShopProduct $product */
                    return new ShopCartItem($product, $row['m'] ?? null, $row['q']);
                }
                return false;
            }, Json::decode($cookie->value)));
        }
        return [];
    }

    public function save(array $items): void
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => $this->_key,
            'value' => Json::encode(array_map(function (ShopCartItem $item) {
                return [
                    'p' => $item->getProductId(),
                    'm' => $item->getModificationId(),
                    'q' => $item->getQuantity(),
                ];
            }, $items)),
            'expire' => time() + $this->_timeout,
        ]));
    }
}
