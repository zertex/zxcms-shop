<?php

namespace common\modules\shop\cart\storage;

use common\modules\shop\cart\ShopCartItem;

interface StorageInterface
{
    /**
     * @return ShopCartItem[]
     */
    public function load(): array;

    /**
     * @param ShopCartItem[] $items
     */
    public function save(array $items): void;
}
