<?php

namespace common\modules\shop\cart\storage;

use common\modules\shop\cart\ShopCartItem;
use yii\db\Connection;
use yii\web\User;

class HybridStorage implements StorageInterface
{
    private $_storage;
    private $_user;
    private $_cookieKey;
    private $_cookieTimeout;
    private $_db;

    public function __construct(User $user, $cookieKey, $cookieTimeout, Connection $db)
    {
        $this->_user = $user;
        $this->_cookieKey = $cookieKey;
        $this->_cookieTimeout = $cookieTimeout;
        $this->_db = $db;
    }

    public function load(): array
    {
        return $this->getStorage()->load();
    }

    public function save(array $items): void
    {
        $this->getStorage()->save($items);
    }

    private function getStorage()
    {
        if ($this->_storage === null) {
            $cookieStorage = new CookieStorage($this->_cookieKey, $this->_cookieTimeout);
            if ($this->_user->isGuest) {
                $this->_storage = $cookieStorage;
            } else {
                $dbStorage = new DbStorage($this->_user->id, $this->_db);
                if ($cookieItems = $cookieStorage->load()) {
                    $dbItems = $dbStorage->load();
                    $items = array_merge($dbItems, array_udiff($cookieItems, $dbItems, function (ShopCartItem $first, ShopCartItem $second) {
                        return $first->getId() === $second->getId();
                    }));
                    $dbStorage->save($items);
                    $cookieStorage->save([]);
                }
                $this->_storage = $dbStorage;
            }
        }
        return $this->_storage;
    }
}
