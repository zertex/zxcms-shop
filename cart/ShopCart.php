<?php

namespace common\modules\shop\cart;

use common\modules\shop\cart\cost\calculator\CalculatorInterface;
use common\modules\shop\cart\storage\StorageInterface;
use common\modules\shop\helpers\ShopDiscountHelper;

class ShopCart
{
    private $_storage;
    private $_calculator;
    /**
     * @var ShopCartItem[]
     * */
    private $_items;

    public function __construct(StorageInterface $storage, CalculatorInterface $calculator)
    {
        $this->_storage = $storage;
        $this->_calculator = $calculator;
    }

    /**
     * @return ShopCartItem[]
     */
    public function getItems(): array
    {
        $this->loadItems();
        return $this->_items;
    }

    public function getAmount(): int
    {
        $this->loadItems();
        return count($this->_items);
    }

    public function getSumAmount(): int
    {
        $this->loadItems();
        $allQuantity = 0;
        foreach ($this->_items as $item) {
            $allQuantity += $item->getQuantity();
        }
        return $allQuantity;
    }

    public function add(ShopCartItem $item): void
    {
        $this->loadItems();
        foreach ($this->_items as $i => $current) {
            if ($current->getId() == $item->getId()) {
                $this->_items[$i] = $current->plus($item->getQuantity());
                $this->saveItems();
                return;
            }
        }
        $this->_items[] = $item;
        $this->saveItems();
    }

    public function set($id, $quantity): void
    {
        $this->loadItems();
        foreach ($this->_items as $i => $current) {
            if ($current->getId() == $id) {
                $this->_items[$i] = $current->changeQuantity($quantity);
                $this->saveItems();
                return;
            }
        }
        throw new \DomainException('Item is not found.');
    }

    public function remove($id): void
    {
        $this->loadItems();
        foreach ($this->_items as $i => $current) {
            if ($current->getId() == $id) {
                unset($this->_items[$i]);
                $this->saveItems();
                return;
            }
        }
        throw new \DomainException('Item is not found.');
    }

    public function clear(): void
    {
        $this->_items = [];
        $this->saveItems();
    }

    /*public function getCost(): Cost
    {
        $this->loadItems();
        return $this->calculator->getCost($this->items);
    }*/

    public function getCost(): int
    {
        $this->loadItems();
        $cost = 0;
        foreach ($this->_items as $item) {
            $cost += $item->getCost();
        }
        return $cost;
    }

    public function getCalculatedCost(): int
    {
        $this->loadItems();
        $cost = 0;
        foreach ($this->_items as $item) {
            $cost += $item->getCalculatedCost();
        }
        return ShopDiscountHelper::discountedCost($cost);
    }

    public function getWeight(): int
    {
        $this->loadItems();
        return array_sum(array_map(function (ShopCartItem $item) {
            return $item->getWeight();
        }, $this->_items));
    }

    private function loadItems(): void
    {
        if ($this->_items === null) {
            $this->_items = $this->_storage->load();
        }
    }

    private function saveItems(): void
    {
        $this->_storage->save($this->_items);
    }
}
