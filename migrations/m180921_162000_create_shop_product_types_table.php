<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_product_types`.
 */
class m180921_162000_create_shop_product_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_product_types}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-shop_product_types-slug}}', '{{%shop_product_types}}', 'slug', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_product_types}}');
    }
}
