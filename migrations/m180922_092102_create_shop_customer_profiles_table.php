<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_customer_profiles`.
 */
class m180922_092102_create_shop_customer_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_customer_profiles}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'company' => 'LONGTEXT NULL',
            'name' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-shop_customer_profiles-user_id}}', '{{%shop_customer_profiles}}', 'user_id');
        $this->addForeignKey('{{%fk-shop_customer_profiles-user_id}}', '{{%shop_customer_profiles}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_customer_profiles-user_id}}', '{{%shop_customer_profiles}}');
        $this->dropIndex('{{%idx-shop_customer_profiles-user_id}}', '{{%shop_customer_profiles}}');

        $this->dropTable('{{%shop_customer_profiles}}');
    }
}
