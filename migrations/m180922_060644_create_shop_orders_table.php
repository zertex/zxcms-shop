<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_orders`.
 */
class m180922_060644_create_shop_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_orders}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'delivery_method_id' => $this->integer(),
            'delivery_method_name' => $this->string()->notNull(),
            'delivery_cost' => $this->integer()->notNull(),
            'payment_method' => $this->string(),
            'cost' => 'float NOT NULL',
            'discount' => $this->integer()->Null(),
            'discount_ext' => $this->integer()->Null(),
            'note' => $this->text(),
            'current_status' => $this->integer()->notNull(),
            'cancel_reason' => $this->text(),
            'statuses_json' => 'LONGTEXT NOT NULL',
            'customer_phone' => $this->string(),
            'customer_name' => $this->string(),
            'delivery_index' => $this->string(),
            'delivery_address' => $this->text(),
            'customer_company' => 'LONGTEXT NULL',
        ], $tableOptions);

        $this->createIndex('{{%idx-shop_orders-user_id}}', '{{%shop_orders}}', 'user_id');
        $this->createIndex('{{%idx-shop_orders-delivery_method_id}}', '{{%shop_orders}}', 'delivery_method_id');

        $this->addForeignKey('{{%fk-shop_orders-user_id}}', '{{%shop_orders}}', 'user_id', '{{%users}}', 'id', 'CASCADE');
        $this->addForeignKey('{{%fk-shop_orders-delivery_method_id}}', '{{%shop_orders}}', 'delivery_method_id', '{{%shop_delivery_methods}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_orders-delivery_method_id}}', '{{%shop_orders}}');
        $this->dropForeignKey('{{%fk-shop_orders-user_id}}', '{{%shop_orders}}');
        $this->dropIndex('{{%idx-shop_orders-delivery_method_id}}', '{{%shop_orders}}');
        $this->dropIndex('{{%idx-shop_orders-user_id}}', '{{%shop_orders}}');

        $this->dropTable('shop_orders');
    }
}
