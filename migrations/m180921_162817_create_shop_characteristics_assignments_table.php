<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_characteristics_assignments`.
 */
class m180921_162817_create_shop_characteristics_assignments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_characteristics_assignments}}', [
            'characteristic_id' => $this->integer()->notNull(),
            'product_type_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-shop_characteristics_assignments}}', '{{%shop_characteristics_assignments}}', ['characteristic_id', 'product_type_id']);

        $this->createIndex('{{%idx-shop_characteristics_assignments-characteristic_id}}', '{{%shop_characteristics_assignments}}', 'characteristic_id');
        $this->createIndex('{{%idx-shop_characteristics_assignments-product_type_id}}', '{{%shop_characteristics_assignments}}', 'product_type_id');

        $this->addForeignKey('{{%fk-shop_characteristics_assignments-characteristic_id}}', '{{%shop_characteristics_assignments}}', 'characteristic_id', '{{%shop_characteristics}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-shop_characteristics_assignments-product_type_id}}', '{{%shop_characteristics_assignments}}', 'product_type_id', '{{%shop_product_types}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_characteristics_assignments-product_type_id}}', '{{%shop_characteristics_assignments}}');
        $this->dropForeignKey('{{%fk-shop_characteristics_assignments-characteristic_id}}', '{{%shop_characteristics_assignments}}');
        $this->dropIndex('{{%idx-shop_characteristics_assignments-product_type_id}}', '{{%shop_characteristics_assignments}}');
        $this->dropIndex('{{%idx-shop_characteristics_assignments-characteristic_id}}', '{{%shop_characteristics_assignments}}');

        $this->dropTable('{{%shop_characteristics_assignments}}');
    }
}
