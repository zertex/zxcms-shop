<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_discounts`.
 */
class m180921_103958_create_shop_discounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_discounts}}', [
            'id' => $this->primaryKey(),
            'percent' => $this->integer()->notNull(),
            'date_from' => $this->integer(),
            'date_to' => $this->integer(),
            'status' => $this->boolean()->notNull(),
            'sort' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull()->defaultValue(-1),
            'type' => $this->integer()->notNull()->defaultValue(1),
            'method' => $this->integer()->notNull()->defaultValue(1),
            'code' => $this->string()->null(),
            'value' => $this->string()->null(),
            'recipient' => $this->integer()->notNull(),
            'value_recipient' => $this->string()->null(),
        ], $tableOptions);

        $this->createIndex('idx-shop_discounts-code', '{{%shop_discounts}}', 'code', true);
        $this->createIndex('idx-shop_discounts-from-to-active-count', '{{%shop_discounts}}', ['date_from', 'date_to', 'status', 'count']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-shop_discounts-from-to-active-count', '{{%shop_discounts}}');
        $this->dropIndex('idx-shop_discounts-code', '{{%shop_discounts}}');

        $this->dropTable('{{%shop_discounts}}');
    }
}
