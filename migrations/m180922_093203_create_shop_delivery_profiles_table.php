<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_delivery_profiles`.
 */
class m180922_093203_create_shop_delivery_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_delivery_profiles}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'method_id' => $this->integer()->notNull(),
            'post_index' => $this->string()->notNull(),
            'post_address' => $this->text()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-shop_delivery_profiles-user_id}}', '{{%shop_delivery_profiles}}', 'user_id');
        $this->addForeignKey('{{%fk-shop_delivery_profiles-user_id}}', '{{%shop_delivery_profiles}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_delivery_profiles-user_id}}', '{{%shop_delivery_profiles}}');
        $this->dropIndex('{{%idx-shop_delivery_profiles-user_id}}', '{{%shop_delivery_profiles}}');

        $this->dropTable('{{%shop_delivery_profiles}}');
    }
}
