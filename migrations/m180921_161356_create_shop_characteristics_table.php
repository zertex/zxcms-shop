<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_characteristics`.
 */
class m180921_161356_create_shop_characteristics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_characteristics}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(16)->notNull(),
            'required' => $this->boolean()->notNull(),
            'sort' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
            'widget' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('{{%idx-shop_characteristics-widget}}', '{{%shop_characteristics}}', 'widget');
        $this->createIndex('{{%idx-shop_characteristics-group_id}}', '{{%shop_characteristics}}', 'group_id');
        $this->addForeignKey('{{%fk-shop_characteristic-group_id}}', '{{%shop_characteristics}}', 'group_id', '{{%shop_characteristics_groups}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_characteristic-group_id}}', '{{%shop_characteristics}}');
        $this->dropIndex('{{%idx-shop_characteristics-group_id}}', '{{%shop_characteristics}}');
        $this->dropIndex('{{%idx-shop_characteristics-widget}}', '{{%shop_characteristics}}');

        $this->dropTable('{{%shop_characteristics}}');
    }
}
