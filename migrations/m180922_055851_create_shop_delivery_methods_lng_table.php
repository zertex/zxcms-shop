<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_delivery_methods_lng`.
 */
class m180922_055851_create_shop_delivery_methods_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_delivery_methods_lng}}', [
            'id' => $this->primaryKey(),
            'main_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx_delivery_methods_lng-language}}', '{{%shop_delivery_methods_lng}}', 'language');
        $this->createIndex('{{%idx_delivery_methods_lng-main_id}}', '{{%shop_delivery_methods_lng}}', 'main_id');
        $this->addForeignKey('{{%fk_delivery_methods_lng_main_id_id}}', '{{%shop_delivery_methods_lng}}', 'main_id', '{{%shop_delivery_methods}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk_delivery_methods_lng_main_id_id}}', '{{%shop_delivery_methods_lng}}');
        $this->dropIndex('{{%idx_delivery_methods_lng-main_id}}', '{{%shop_delivery_methods_lng}}');
        $this->dropIndex('{{%idx_delivery_methods_lng-language}}', '{{%shop_delivery_methods_lng}}');

        $this->dropTable('{{%shop_delivery_methods_lng}}');
    }
}
