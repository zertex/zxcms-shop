<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_characteristics_lng`.
 */
class m180921_155042_create_shop_characteristics_groups_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_characteristics_groups_lng}}', [
            'id' => $this->primaryKey(),
            'shop_char_group_id' => $this->integer()->notNull(),
            'language'               => $this->string(6)->notNull(),
            'name'                   => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_shop_characteristics_gr_lng_language', '{{%shop_characteristics_groups_lng}}', 'language');
        $this->createIndex('idx_shop_characteristics_gr_lng_shop_char_group_id', '{{%shop_characteristics_groups_lng}}', 'shop_char_group_id');
        $this->addForeignKey('frg_shop_characteristics_gr_lng_shop_char_group_id_id', '{{%shop_characteristics_groups_lng}}', 'shop_char_group_id', '{{%shop_characteristics_groups}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_shop_characteristics_gr_lng_shop_char_group_id_id', '{{%shop_characteristics_groups_lng}}');
        $this->dropIndex('idx_shop_characteristics_gr_lng_shop_char_group_id', '{{%shop_characteristics_groups_lng}}');
        $this->dropIndex('idx_shop_characteristics_gr_lng_language', '{{%shop_characteristics_groups_lng}}');

        $this->dropTable('{{%shop_characteristics_groups_lng}}');
    }
}
