<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_modifications_lng`.
 */
class m180921_215428_create_shop_modifications_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_modifications_lng}}', [
            'id' => $this->primaryKey(),
            'main_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_mods_lng_language', '{{%shop_modifications_lng}}', 'language');
        $this->createIndex('idx_mods_lng_main_id', '{{%shop_modifications_lng}}', 'main_id');
        $this->addForeignKey('frg_mods_lng_main_id_id', '{{%shop_modifications_lng}}', 'main_id', '{{%shop_modifications}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_mods_lng_main_id_id', '{{%shop_modifications_lng}}');
        $this->dropIndex('idx_mods_lng_main_id', '{{%shop_modifications_lng}}');
        $this->dropIndex('idx_mods_lng_language', '{{%shop_modifications_lng}}');

        $this->dropTable('{{%shop_modifications_lng}}');
    }
}
