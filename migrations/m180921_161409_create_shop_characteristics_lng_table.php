<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_characteristics_lng`.
 */
class m180921_161409_create_shop_characteristics_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_characteristics_lng}}', [
            'id' => $this->primaryKey(),
            'main_id' => $this->integer()->notNull(),
            'language' => $this->string(6)->notNull(),
            'name' => $this->string()->notNull(),
            'default' => $this->string(),
            'variants_json' => 'LONGTEXT NOT NULL',
        ], $tableOptions);

        $this->createIndex('idx_shop_characteristics_lng_language', '{{%shop_characteristics_lng}}', 'language');
        $this->createIndex('idx_shop_characteristics_lng_main_id', '{{%shop_characteristics_lng}}', 'main_id');
        $this->addForeignKey('frg_shop_characteristics_lng_main_id', '{{%shop_characteristics_lng}}', 'main_id', '{{%shop_characteristics}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_shop_characteristics_lng_main_id', '{{%shop_characteristics_lng}}');
        $this->dropIndex('idx_shop_characteristics_lng_main_id', '{{%shop_characteristics_lng}}');
        $this->dropIndex('idx_shop_characteristics_lng_language', '{{%shop_characteristics_lng}}');

        $this->dropTable('{{%shop_characteristics_lng}}');
    }
}
