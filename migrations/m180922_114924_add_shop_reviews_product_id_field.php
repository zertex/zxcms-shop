<?php

use yii\db\Migration;

/**
 * Class m180922_114924_add_shop_reviews_product_id_field
 */
class m180922_114924_add_shop_reviews_product_id_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%shop_reviews}}', 'product_id', $this->integer()->notNull());
        $this->createIndex('{{%idx-shop_reviews-product_id}}', '{{%shop_reviews}}', 'product_id');
        $this->addForeignKey('{{%fk-shop_reviews-product_id}}', '{{%shop_reviews}}', 'product_id', '{{%shop_products}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_reviews-product_id}}', '{{%shop_reviews}}');
        $this->dropIndex('{{%idx-shop_reviews-product_id}}', '{{%shop_reviews}}');
        $this->dropColumn('{{%shop_reviews}}', 'product_id');
    }
}
