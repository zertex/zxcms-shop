<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_discounts_lng`.
 */
class m180921_105353_create_shop_discounts_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_discounts_lng}}', [
            'id' => $this->primaryKey(),
            'shop_discount_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name'             => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_shop_discounts_lng_language', '{{%shop_discounts_lng}}', 'language');
        $this->createIndex('idx_shop_discounts_lng_shop_discount_id', '{{%shop_discounts_lng}}', 'shop_discount_id');
        $this->addForeignKey('frg_shop_discounts_lng_shop_discounts_shop_discount_id_id', '{{%shop_discounts_lng}}', 'shop_discount_id', '{{%shop_discounts}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_shop_discounts_lng_shop_discounts_shop_discount_id_id', '{{%shop_discounts_lng}}');
        $this->dropIndex('idx_shop_discounts_lng_shop_discount_id', '{{%shop_discounts_lng}}');
        $this->dropIndex('idx_shop_discounts_lng_language', '{{%shop_discounts_lng}}');

        $this->dropTable('{{%shop_discounts_lng}}');
    }
}
