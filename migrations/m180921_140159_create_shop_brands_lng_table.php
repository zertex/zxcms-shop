<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_brands_lng`.
 */
class m180921_140159_create_shop_brands_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_brands_lng}}', [
            'id' => $this->primaryKey(),
            'shop_brand_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name'             => $this->string()->notNull(),
            'meta_title'       => $this->string(255),
            'meta_description' => $this->text(),
            'meta_keywords'    => $this->string(255),
        ], $tableOptions);

        $this->createIndex('idx_shop_brands_lng_language', '{{%shop_brands_lng}}', 'language');
        $this->createIndex('idx_shop_brands_lng_shop_brand_id', '{{%shop_brands_lng}}', 'shop_brand_id');
        $this->addForeignKey('frg_shop_brands_lng_shop_brands_shop_brand_id_id', '{{%shop_brands_lng}}', 'shop_brand_id', '{{%shop_brands}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_shop_brands_lng_shop_brands_shop_brand_id_id', '{{%shop_brands_lng}}');
        $this->dropIndex('idx_shop_brands_lng_shop_brand_id', '{{%shop_brands_lng}}');
        $this->dropIndex('idx_shop_brands_lng_language', '{{%shop_brands_lng}}');

        $this->dropTable('{{%shop_brands_lng}}');
    }
}
