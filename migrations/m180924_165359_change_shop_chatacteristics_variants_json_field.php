<?php

use yii\db\Migration;

/**
 * Class m180924_165359_change_shop_chatacteristics_variants_json_field
 */
class m180924_165359_change_shop_chatacteristics_variants_json_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('shop_characteristics_lng', 'variants_json', 'LONGTEXT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('shop_characteristics_lng', 'variants_json', 'LONGTEXT NOT NULL');
    }
}
