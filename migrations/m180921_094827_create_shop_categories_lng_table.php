<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_categories_lng`.
 */
class m180921_094827_create_shop_categories_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_categories_lng}}', [
            'id' => $this->primaryKey(),
            'shop_category_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name'             => $this->string()->notNull(),
            'title'            => $this->string(),
            'description'      => $this->text(),
            'meta_title'       => $this->string(255),
            'meta_description' => $this->text(),
            'meta_keywords'    => $this->string(255),
        ], $tableOptions);

        $this->createIndex('idx_shop_categories_lng_language', '{{%shop_categories_lng}}', 'language');
        $this->createIndex('idx_shop_categories_lng_shop_category_id', '{{%shop_categories_lng}}', 'shop_category_id');
        $this->addForeignKey('frg_shop_categories_lng_shop_categories_shop_category_id_id', '{{%shop_categories_lng}}', 'shop_category_id', '{{%shop_categories}}', 'id', 'CASCADE', 'CASCADE');

        $this->insert('{{%shop_categories}}', [
            'id' => 1,
            'slug' => 'root',
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
        ]);

        foreach (Yii::$app->params['translatedLanguages'] as $language => $languageName) {
            $this->insert('{{%shop_categories_lng}}', [
                'shop_category_id' => 1,
                'language'         => $language,
                'name'             => '',
                'title'            => null,
                'description'      => null,
                'meta_title'       => null,
                'meta_description' => null,
                'meta_keywords'    => null,
            ]);
        };
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_shop_categories_lng_shop_categories_shop_category_id_id', '{{%shop_categories_lng}}');
        $this->dropIndex('idx_shop_categories_lng_shop_category_id', '{{%shop_categories_lng}}');
        $this->dropIndex('idx_shop_categories_lng_language', '{{%shop_categories_lng}}');

        $this->dropTable('{{%shop_categories_lng}}');
    }
}
