<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_wishlist_items`.
 */
class m180921_222159_create_shop_wishlist_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_wishlist_items}}', [
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-shop_wishlist_items}}', '{{%shop_wishlist_items}}', ['user_id', 'product_id']);

        $this->createIndex('{{%idx-shop_wishlist_items-user_id}}', '{{%shop_wishlist_items}}', 'user_id');
        $this->createIndex('{{%idx-shop_wishlist_items-product_id}}', '{{%shop_wishlist_items}}', 'product_id');
        $this->addForeignKey('{{%fk-shop_wishlist_items-user_id}}', '{{%shop_wishlist_items}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-shop_wishlist_items-product_id}}', '{{%shop_wishlist_items}}', 'product_id', '{{%shop_products}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-shop_wishlist_items-product_id}}', '{{%shop_wishlist_items}}');
        $this->dropForeignKey('{{%fk-shop_wishlist_items-user_id}}', '{{%shop_wishlist_items}}');
        $this->dropIndex('{{%idx-shop_wishlist_items-product_id}}', '{{%shop_wishlist_items}}');
        $this->dropIndex('{{%idx-shop_wishlist_items-user_id}}', '{{%shop_wishlist_items}}');

        $this->dropTable('{{%shop_wishlist_items}}');
    }
}
