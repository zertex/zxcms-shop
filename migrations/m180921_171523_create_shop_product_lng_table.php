<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_product_lng`.
 */
class m180921_171523_create_shop_product_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%shop_products_lng}}', [
            'id' => $this->primaryKey(),
            'main_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'meta_title'       => $this->string(255),
            'meta_description' => $this->text(),
            'meta_keywords'    => $this->string(255),
        ], $tableOptions);

        $this->createIndex('idx_shop_products_lng_language', '{{%shop_products_lng}}', 'language');
        $this->createIndex('idx_shop_products_lng_main_id', '{{%shop_products_lng}}', 'main_id');
        $this->addForeignKey('frg_shop_products_lng_main_id_id', '{{%shop_products_lng}}', 'main_id', '{{%shop_products}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_shop_products_lng_main_id_id', '{{%shop_products_lng}}');
        $this->dropIndex('idx_shop_products_lng_main_id', '{{%shop_products_lng}}');
        $this->dropIndex('idx_shop_products_lng_language', '{{%shop_products_lng}}');

        $this->dropTable('{{%shop_products_lng}}');
    }
}
