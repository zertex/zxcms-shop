<?php

use yii\db\Migration;

/**
 * Class m180922_094804_add_shop_modifications_quantity_field
 */
class m180922_094804_add_shop_modifications_quantity_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%shop_modifications}}', 'quantity', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%shop_modifications}}', 'quantity');
    }
}
