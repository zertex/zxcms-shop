<?php

use yii\db\Migration;

/**
 * Class m180922_074748_init_shop_settings
 */
class m180922_074748_init_shop_settings extends Migration
{
    private $_data = [
        'payment_robokassa' => 0,
        'payment_invoice' => 0,
        'delivery_in_invoice' => 0,
        'invoice_tax_percent' => 0,
    ];

    private $_section = 'shop';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $form = new \core\forms\SettingsForm();
        $form->section = $this->_section;
        $form->active = 1;

        foreach ($this->_data as $key => $value) {
            $form->key = $key;
            $form->value = $value;

            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $form->{'value_' . $language} = $value;
            }

            $setting = \core\entities\Settings::create($form, 'string', $this->_section, $key, 1);
            $setting->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach ($this->_data as $key => $value) {
            $setting = \core\entities\Settings::find()
                ->andWhere(['section' => $this->_section])
                ->andWhere(['key' => $key])
                ->one();
            if ($setting) {
                $setting->delete();
            }
        }
    }
}
