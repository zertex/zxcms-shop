<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopBrandForm */

$this->title = Yii::t('shop', 'Create Brand');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
