<?php

use common\modules\shop\entities\ShopBrand;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\shop\forms\search\ShopBrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Brands');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Brand'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 't_name',
                        'label' => Yii::t('shop', 'Brand Name'),
                        'format' => 'raw',
                        'value' => function (ShopBrand $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                    ],
                    /*[
                        'attribute' => 'name',
                        'label' => Yii::t('shop', 'Brand Name'),
                        'value' => function (ShopBrand $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],*/
                    [
                        'attribute' => 'slug',
                        'label' => Yii::t('shop', 'SEO Link'),
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
