<?php

/* @var $this yii\web\View */
/* @var $brand \common\modules\shop\entities\ShopBrand */
/* @var $model \common\modules\shop\forms\ShopBrandForm */

$this->title = Yii::t('shop', 'Update Brand: {name}', ['name' => $brand->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $brand->translation->name, 'url' => ['view', 'id' => $brand->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="brand-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
