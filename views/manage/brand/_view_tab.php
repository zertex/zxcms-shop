<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\ShopBrand;

/**
 * @var $this \yii\web\View
 * @var $brand ShopBrand
 * @var $language string
 */

?>

<?= DetailView::widget([
    'model'      => $brand,
    'attributes' => [
        [
            'label' => Yii::t('shop', 'Name'),
            'value' => function (ShopBrand $entity) use ($language) {
                return $entity->findTranslation($language)->name;
            }
        ],
    ],
]) ?>

<div class="box">
    <div class="box-header with-border"><?= Yii::t('shop', 'SEO') ?></div>
    <div class="box-body">

        <?= DetailView::widget([
            'model'      => $brand,
            'attributes' => [
                [
                    'label' => Yii::t('shop', 'META Title'),
                    'value' => function (ShopBrand $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_title;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Description'),
                    'value' => function (ShopBrand $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_description;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Keywords'),
                    'value' => function (ShopBrand $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_keywords;
                    }
                ],
            ],
        ]) ?>
    </div>
</div>