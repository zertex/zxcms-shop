<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopCharacteristicGroupForm */

$this->title = Yii::t('shop', 'Create Characteristic');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Characteristics Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="characteristic-group-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
