<?php

use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\forms\search\ShopCharacteristicGroupSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ShopCharacteristicGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('shop', 'Characteristics Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="characteristic-group-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Group'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    [
                        'attribute' => 't_name',
                        'label'     => Yii::t('shop', 'Group Name'),
                        'value'     => function (ShopCharacteristicGroup $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format'    => 'raw',
                    ],
                    [
                        'class'   => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
                'tableOptions' => ['class' => 'table table-striped table-hover'],
            ]); ?>
        </div>
    </div>
</div>
