<?php

use common\modules\shop\entities\ShopCharacteristicGroup;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $characteristicGroup ShopCharacteristicGroup */

$this->title = $characteristicGroup->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Characteristics Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="characteristic-group-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Characteristics Groups'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $characteristicGroup->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $characteristicGroup->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this characteristic group?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $characteristicGroup,
                'attributes' => [
                    [
                        'attribute' => 'id',
                        'label' => Yii::t('shop', 'ID'),
                    ],
                    [
                        'attribute' => 'sort',
                        'label' => Yii::t('shop', 'Sort'),
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_view_tab', [
                'group' => $characteristicGroup,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]) ?>
    </div>
</div>
