<?php

/* @var $this yii\web\View */
/* @var $characteristicGroup \common\modules\shop\entities\ShopCharacteristicGroup */
/* @var $model \common\modules\shop\forms\ShopCharacteristicGroupForm */

$this->title = Yii::t('shop', 'Update Characteristic Group: {name}', ['name' => $characteristicGroup->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Characteristics Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $characteristicGroup->translation->name, 'url' => ['view', 'id' => $characteristicGroup->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="characteristic-group-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
