<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\ShopCharacteristicGroup;

/**
 * @var $this \yii\web\View
 * @var $group ShopCharacteristicGroup
 * @var $language string
 */

echo DetailView::widget([
    'model'      => $group,
    'attributes' => [
        [
            'label' => Yii::t('shop', 'Name'),
            'value' => function (ShopCharacteristicGroup $entity) use ($language) {
                return $entity->findTranslation($language)->name;
            }
        ],
    ],
]);
