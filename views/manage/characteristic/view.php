<?php

use common\modules\shop\helpers\ShopCharacteristicHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $characteristic \common\modules\shop\entities\ShopCharacteristic */

$this->title = $characteristic->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Characteristics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Characteristics'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $characteristic->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $characteristic->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this characteristic?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $characteristic,
                'attributes' => [
                    [
                        'attribute' => 'id',
                        'label' => Yii::t('shop', 'ID'),
                    ],
                    /*[
                        'attribute' => 'name',
                        'label' => Yii::t('shop', 'Characteristic Name'),
                    ],*/
                    [
                        'attribute' => 'type',
                        'label' => Yii::t('shop', 'Characteristic Type'),
                        'value' => ShopCharacteristicHelper::typeName($characteristic->type),
                    ],
                    [
                        'attribute' => 'sort',
                        'label' => Yii::t('shop', 'Sort'),
                    ],
                    [
                        'attribute' => 'required',
                        'format' => 'boolean',
                        'label' => Yii::t('shop', 'Required'),
                    ],
                    /*[
                        'attribute' => 'default',
                        'label' => Yii::t('shop', 'Default'),
                    ],
                    [
                        'attribute' => 'variants',
                        'label' => Yii::t('shop', 'Text Variants'),
                        'value' => implode(PHP_EOL, $characteristic->variants),
                        'format' => 'ntext',
                    ],*/
                    [
                        'attribute' => 'widget',
                        'label' => Yii::t('shop', 'Widget'),
                        'value' => ShopCharacteristicHelper::widgetName($characteristic->widget),
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_view_tab', [
                'characteristic' => $characteristic,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]) ?>
    </div>
</div>
