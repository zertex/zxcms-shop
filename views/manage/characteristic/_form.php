<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopCharacteristicForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="characteristic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_form_tab', [
                'form'     => $form,
                'model'    => $model,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items
        ]) ?>
    </div>

    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'required', [
                        'template' => '{input}',
                        'options' => [
                            'tag' => null, // Don't wrap with "form-group" div
                        ],
                    ])->checkbox(['label' => false])->label(false) ?>
                    <?= Yii::t('shop', 'Required') ?>

                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'widget')->dropDownList($model->widgetsList()) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'group_id')->dropDownList($model->groupsList()) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type')->dropDownList($model->typesList()) ?>
                </div>
            </div>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
