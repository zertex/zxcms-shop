<?php
/**
 * Created by Error202
 * Date: 28.08.2018
 */

/**
 * @var $this \yii\web\View
 * @var $form \yii\widgets\ActiveForm
 * @var $model \common\modules\shop\forms\ShopCharacteristicForm
 * @var $language string
 */

$postfix = $language == Yii::$app->params['defaultLanguage'] ? '' : '_' . $language;
?>

<div class="box box-default">
    <div class="box-body">
        <?= $form->field($model, 'name' . $postfix)->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'default' . $postfix)->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'variants_json' . $postfix)->textarea(['rows' => 6]) ?>
    </div>
</div>
