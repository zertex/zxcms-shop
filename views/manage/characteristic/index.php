<?php

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\helpers\ShopCharacteristicHelper;
use common\modules\shop\forms\search\ShopCharacteristicSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ShopCharacteristicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('shop', 'Characteristics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="characteristics-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Characteristic'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    [
                        'attribute' => 't_name',
                        'label'     => Yii::t('shop', 'Characteristic Name'),
                        'value'     => function (ShopCharacteristic $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'group_id',
                        'label'     => Yii::t('shop', 'Characteristic Group'),
                        'filter'    => $searchModel->groupsList(),
                        'value'     => function (ShopCharacteristic $model) {
                            return $model->characteristicGroup->translation->name;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label'     => Yii::t('shop', 'Characteristic Type'),
                        'filter'    => $searchModel->typesList(),
                        'value'     => function (ShopCharacteristic $model) {
                            return ShopCharacteristicHelper::typeName($model->type);
                        },
                        'options'   => ['style' => 'width: 200px;'],
                    ],
                    [
                        'attribute' => 'widget',
                        'label'     => Yii::t('shop', 'Widget'),
                        'filter'    => $searchModel->widgetsList(),
                        'value'     => function (ShopCharacteristic $model) {
                            return ShopCharacteristicHelper::widgetName($model->widget ?: 0);
                        },
                        'options'   => ['style' => 'width: 150px;'],
                    ],
                    [
                        'attribute' => 'required',
                        'label'     => Yii::t('shop', 'Required'),
                        'filter'    => $searchModel->requiredList(),
                        'format'    => 'boolean',
                        'options'   => ['style' => 'width: 150px;'],
                    ],
                    [
                        'class'   => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
                'tableOptions' => ['class' => 'table table-striped table-hover'],
            ]); ?>
        </div>
    </div>
</div>
