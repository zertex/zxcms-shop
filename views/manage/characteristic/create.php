<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopCharacteristicForm */

$this->title = Yii::t('shop', 'Create Characteristic');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Characteristics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="characteristic-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
