<?php

/* @var $this yii\web\View */
/* @var $characteristic \common\modules\shop\entities\ShopCharacteristic */
/* @var $model \common\modules\shop\forms\ShopCharacteristicForm */

$this->title = Yii::t('shop', 'Update Characteristic: {name}', ['name' => $characteristic->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Characteristics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $characteristic->translation->name, 'url' => ['view', 'id' => $characteristic->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="characteristic-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
