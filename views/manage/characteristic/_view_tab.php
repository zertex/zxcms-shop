<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\ShopCharacteristic;

/**
 * @var $this \yii\web\View
 * @var $characteristic ShopCharacteristic
 * @var $language string
 */

echo DetailView::widget([
    'model'      => $characteristic,
    'attributes' => [
        /*[
            'label' => Yii::t('shop', 'Name'),
            'value' => function (ShopCharacteristic $entity) use ($language) {
                return $entity->findTranslation($language)->name;
            }
        ],*/
        [
            'attribute' => 'name',
            'label' => Yii::t('shop', 'Characteristic Name'),
            'value' => function (ShopCharacteristic $entity) use ($language) {
                return $entity->findTranslation($language)->name;
            }
        ],
        [
            'attribute' => 'default',
            'label' => Yii::t('shop', 'Default'),
            'value' => function (ShopCharacteristic $entity) use ($language) {
                return $entity->findTranslation($language)->default;
            }
        ],
        [
            'attribute' => 'name',
            'format' => 'ntext',
            'label' => Yii::t('shop', 'Text Variants'),
            'value' => function (ShopCharacteristic $entity) use ($language) {
                return $entity->findTranslation($language)->variants_json;
            }
        ],
    ],
]);
