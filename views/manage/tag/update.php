<?php

/* @var $this yii\web\View */
/* @var $tag \common\modules\shop\entities\ShopTag */
/* @var $model \common\modules\shop\forms\ShopTagForm */

$this->title = Yii::t('shop', 'Update Tag: {name}', ['name' => $tag->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $tag->name, 'url' => ['view', 'id' => $tag->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
