<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $tag \common\modules\shop\entities\ShopTag */

$this->title = $tag->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Tags'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $tag->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $tag->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this tag?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $tag,
                'attributes' => [
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('shop', 'Tag Name'),
                    ],
                    [
                        'attribute' => 'slug',
                        'label' => Yii::t('shop', 'SEO Link'),
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
