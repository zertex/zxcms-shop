<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopTagForm */

$this->title = Yii::t('shop', 'Create Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
