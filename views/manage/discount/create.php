<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopDiscountForm */

$this->title = Yii::t('shop', 'Create Discount');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->date_from = date('d.m.Y', time());
?>
<div class="discount-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
