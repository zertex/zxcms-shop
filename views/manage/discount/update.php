<?php

/* @var $this yii\web\View */
/* @var $discount \common\modules\shop\entities\ShopDiscount */
/* @var $model \common\modules\shop\forms\ShopDiscountForm */

$this->title = Yii::t('shop', 'Update Discount: {name}', ['name' => $discount->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $discount->name, 'url' => ['view', 'id' => $discount->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="discount-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
