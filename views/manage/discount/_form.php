<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\shop\forms\ShopDiscountForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\modules\shop\helpers\ShopDiscountHelper;
use common\modules\shop\helpers\PriceHelper;

/* @var $this yii\web\View */
/* @var $model ShopDiscountForm */
/* @var $form yii\widgets\ActiveForm */

$currencyTag = PriceHelper::currencyTag();

$urlUser = Url::to(['/shop/admin/discount/search-user']);
$urlUsersGroup = Url::to(['/shop/admin/discount/search-users-group']);
$urlCategory = Url::to(['/shop/admin/discount/search-category']);
$urlProduct = Url::to(['/shop/admin/discount/search-product']);
$urlProductType = Url::to(['/shop/admin/discount/search-product-type']);
$urlBrand = Url::to(['/shop/admin/discount/search-brand']);

$titleUser = Yii::t('shop', 'ID / User name / E-mail');
$titleUsersGroup = Yii::t('shop', 'ID / Group name');
$titleCategory = Yii::t('shop', 'ID / Category name');
$titleProduct = Yii::t('shop', 'ID / Product name / Code');
$titleProductType = Yii::t('shop', 'ID / Type name');
$titleBrand = Yii::t('shop', 'ID / Brand name');

$jsHead = <<<JSHEAD
    valueUrl='';
    valueRecipientUrl='';
JSHEAD;
$this->registerJs($jsHead, $this::POS_HEAD);

$valueData = $model->value;
$js = <<<JS
function str_rand(count) {
    var result       = '';
    var words        = '123456789qwertyuipasdfghjkzxcvbnmQWERTYUPASDFGHJKLZXCVBNM';
    var max_position = words.length;
        for( i = 0; i < count; ++i ) {
            position = Math.floor ( Math.random() * max_position );
            result = result + words.substring(position, position + 1);
        }
    return result;
}

function disableUseCount()
{
    var countField = $("#discountform-count");
    if (countField.val() === '')
    {
        countField.val(-1);
    }
    countField.prop("readonly", true);
}

function enableUseCount()
{
    var countField = $("#discountform-count");
    if (countField.val() === '-1')
    {
        countField.val(1);
    }
    countField.prop("readonly", false);
}

function disableToDate()
{
    var field = $("#discountform-to_date");
    field.val('');
    field.prop("disabled", true);
}

function enableToDate()
{
    var field = $("#discountform-to_date");
    field.val('');
    field.prop("disabled", false);
}

function showValueRecipientUser(val)
{
    $(".field-discountform-value_recipient").show();
    $(".field-discountform-value_recipient label").html('{$titleUser}');
    if (!val)
    {
        $("#discountform-value_recipient").empty().trigger('change');
    }
    valueRecipientUrl='{$urlUser}';
}
function showValueRecipientUsersGroup(val)
{
    $(".field-discountform-value_recipient").show();
    $(".field-discountform-value_recipient label").html('{$titleUsersGroup}');
    if (!val)
    {
        $("#discountform-value_recipient").empty().trigger('change');
    }
    valueRecipientUrl='{$urlUsersGroup}';
}
function showValueCategory(val)
{
    $(".field-discountform-value").show();
    $(".field-discountform-value label").html('{$titleCategory}');
    if (!val)
    {
        $("#discountform-value").empty().trigger('change');
    }
    valueUrl='{$urlCategory}';
}
function showValueProduct(val)
{
    $(".field-discountform-value").show();
    $(".field-discountform-value label").html('{$titleProduct}');
    if (!val)
    {
        $("#discountform-value").empty().trigger('change');
    }
    valueUrl='{$urlProduct}';
}
function showValueProductType(val)
{
    $(".field-discountform-value").show();
    $(".field-discountform-value label").html('{$titleProductType}');
    if (!val)
    {
        $("#discountform-value").empty().trigger('change');
    }
    valueUrl='{$urlProductType}';
}
function showValueBrand(val)
{
    $(".field-discountform-value").show();
    $(".field-discountform-value label").html('{$titleBrand}');
    if (!val)
    {
        $("#discountform-value").empty().trigger('change');
    }
    valueUrl='{$urlBrand}';
}

function hideValue()
{
    $("#discountform-value").empty().trigger('change');
    $(".field-discountform-value").hide();
}

function hideValueRecipient()
{
    $("#discountform-value_recipient").empty().trigger('change');
    $(".field-discountform-value_recipient").hide();
}

function updateTypeValue(type) {
    var val = $("#discountform-value").val();
    if (type === '2') {showValueCategory(val);} // category
    else if (type === '3') {showValueProduct(val);} // product
    else if (type === '4') {showValueProductType(val);} // product type
    else if (type === '5') {showValueBrand(val);} // brand
    else {hideValue();}
}

function updateRecipientValue(recipient) {
    var val = $("#discountform-value_recipient").val();
    if (recipient === '3') {showValueRecipientUser(val);} // user
    else if (recipient === '2') {showValueRecipientUsersGroup(val);} // users group
    else {hideValueRecipient();}
}

function updateRecipientCount(recipient)
{
    if (recipient === '1' || recipient === '2') { // all users & group of users
        var forCount = $("#for_count");
        forCount.prop('checked', true).trigger("change");
        disableUseCount();
        forCount.prop('disabled', true);
    } 
    else
    {
        $("#for_count").prop('disabled', false);
    }
}

$("#discountform-method").on('change', function(){
    var method = this.value;
    var tag = $("#percent_tag");
    method === '0' ? tag.html('{$currencyTag}') : tag.html('%');
});

function changeCountState()
{
    $("#for_count").prop('checked') ? disableUseCount() : enableUseCount();
}

function changeToDateState()
{
    $("#for_to_date").prop('checked') ? disableToDate() : enableToDate();
}

$("#for_count").on('click', function(){
    changeCountState();
});

$("#for_to_date").on('click', function(){
    changeToDateState();
});

$("#discount-code-button").on('click', function(){
   $("#discountform-code").val(str_rand(10)); 
});

$("#discountform-type").on('change', function(){
    var type = this.value;
    $("#discountform-value").val('');
    updateTypeValue(type);
});

$("#discountform-recipient").on('change', function(){
    var recipient = this.value;
    $("#discountform-value_recipient").val('');
    updateRecipientValue(recipient);
    updateRecipientCount(recipient);
});

updateRecipientValue($("#discountform-recipient").val());
updateTypeValue($("#discountform-type").val());
changeCountState();
changeToDateState();
updateRecipientCount($("#discountform-recipient").val());
//$("#discountform-value").select2();
JS;
$this->registerJs($js, $this::POS_READY);
?>

<div class="discount-form">

    <?php $form = ActiveForm::begin([
        'id' => 'discount-form',
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(array_merge(['/shop/admin/discount/validate-form'], array_filter($model->id ? ['id' => $model->id] : []))),
    ]); ?>

    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-6">

                    <!-- Discount Params -->
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= Yii::t('shop', 'Discount Params') ?></div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-9">
                                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'sort')->textInput(['maxlength' => true, 'value' => $model->getNextSort()]) ?>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'code', [
                                        'template' => '{label}<div class="input-group"><div class="input-group-btn"><button type="button" class="btn btn-primary" id="discount-code-button"><i class="fa fa-refresh" aria-hidden="true"></i></button></div>{input}</div>',
                                    ])->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'count')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'for_count')->checkbox(['id' => 'for_count']) ?>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Calculate Method -->
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= Yii::t('shop', 'Calculate Method') ?></div>
                        <div class="panel-body">

                            <?= $form->field($model, 'method')->dropDownList(ShopDiscountForm::methodList()) ?>
                            <?= $form->field($model, 'percent', [
                                'template' => '{label}<div class="input-group">{input}<span class="input-group-addon" id="percent_tag">%</span></div>',
                            ])->textInput(['maxlength' => true]) ?>

                        </div>
                    </div>

                    <!-- Active Period -->
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= Yii::t('shop', 'Action Period') ?></div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <?=  $form->field($model, 'date_from')->widget(DatePicker::class, [
                                        'options' => [],
                                        'pluginOptions' => [
                                            'format' => 'dd.mm.yyyy',
                                            'autoclose'=>true,
                                            'todayHighlight' => true,
                                        ],
                                    ]); ?>
                                </div>
                                <div class="col-md-6">
                                    <?=  $form->field($model, 'date_to')->widget(DatePicker::class, [
                                        'options' => [],
                                        'pluginOptions' => [
                                            'format' => 'dd.mm.yyyy',
                                            'autoclose'=>true,
                                            'todayHighlight' => true,
                                        ],
                                    ]); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'for_to_date')->checkbox(['id' => 'for_to_date']) ?>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="col-md-6">

                    <!-- Discount Type -->
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= Yii::t('shop', 'Discount Type') ?></div>
                        <div class="panel-body">

                            <?= $form->field($model, 'type')->dropDownList(ShopDiscountForm::typeList()) ?>

                            <?= $form->field($model, 'value')->widget(Select2::class, [
                                'initValueText' => $model->value ? strip_tags(ShopDiscountHelper::getDiscountValue($model->type, $model->value)) : '',
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 1,
                                    'ajax' => [
                                        'url' => new JsExpression('function() {return valueUrl;}'),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                        //'cache' => true,
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(repo){ return repo.text; }'),
                                    'templateSelection' => new JsExpression('function(repo){ return repo.text; }'),
                                ],
                            ]); ?>

                        </div>
                    </div>

                    <!-- Discount Recipient -->
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= Yii::t('shop', 'Discount Recipient') ?></div>
                        <div class="panel-body">

                            <?= $form->field($model, 'recipient')->dropDownList(ShopDiscountForm::recipientList()) ?>

                            <?= $form->field($model, 'value_recipient')->widget(Select2::class, [
                                'initValueText' => $model->value_recipient ? strip_tags(ShopDiscountHelper::getDiscountRecipientValue($model->recipient, $model->value_recipient)) : '',
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 1,
                                    'ajax' => [
                                        'url' => new JsExpression('function() {return valueRecipientUrl;}'),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                        //'cache' => true,
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(repo){ return repo.text; }'),
                                    'templateSelection' => new JsExpression('function(repo){ return repo.text; }'),
                                ],
                            ]); ?>

                        </div>
                    </div>

                </div>
            </div>


            <?= $form->field($model, 'status')->checkbox() ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
