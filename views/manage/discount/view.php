<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\shop\entities\ShopDiscount;
use common\modules\shop\forms\ShopDiscountForm;
use common\modules\shop\helpers\PriceHelper;
use common\modules\shop\helpers\ShopDiscountHelper;

/* @var $this yii\web\View */
/* @var $discount ShopDiscount */

$this->title = $discount->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$activeDate = ShopDiscount::find()->dateActive()->andWhere(['id' => $discount->id])->count();
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Discounts'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php if ($discount->isActive()) : ?>
            <?= Html::a(Yii::t('shop', 'Deactivate'), ['draft', 'id' => $discount->id], ['class' => 'btn btn-primary', 'data-method' => 'post']) ?>
        <?php else : ?>
            <?= Html::a(Yii::t('shop', 'Activate'), ['activate', 'id' => $discount->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $discount->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $discount->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this discount?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-header with-border"><?= Yii::t('shop', 'Common') ?></div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $discount,
                'attributes' => [
                    [
                        'label' => Yii::t('shop', 'Status'),
                        'format' => 'raw',
                        'value' => function (ShopDiscount $discount) use ($activeDate) {
                            if ($discount->status == ShopDiscount::STATUS_DISABLED) {
                                return '<span class="text-danger">' . Yii::t('shop', 'Disabled Discount') . '</span>';
                            } elseif (!$activeDate) {
                                return '<span class="text-danger">' . Yii::t('shop', 'Time Expired') . '</span>';
                            } else {
                                return '<span class="text-success">' . Yii::t('shop', 'Enabled Discount') . '</span>';
                            }
                        }
                    ],
                    [
                        'attribute' => 'count',
                        'label' => Yii::t('shop', 'Use count'),
                    ],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('shop', 'Discount Name'),
                        'value' => function (ShopDiscount $discount) {
                            return $discount->translation->name;
                        }
                    ],
                    [
                        'attribute' => 'code',
                        'label' => Yii::t('shop', 'Discount code'),
                    ],
                    [
                        'attribute' => 'method',
                        'label' => Yii::t('shop', 'Calculate Method'),
                        'value' => function ($discount) {
                            return ShopDiscountForm::methodName($discount->method);
                        }
                    ],
                    [
                        'attribute' => 'percent',
                        'label' => Yii::t('shop', 'Discount'),
                        'value' => function ($discount) {
                            return $discount->percent . ' ' . ($discount->method == ShopDiscount::METHOD_PERCENT ? '%' : PriceHelper::currencyTag());
                        }
                    ],
                    [
                        'label' => Yii::t('shop', 'Action Period'),
                        'format' => 'raw',
                        'value' => function ($discount) {
                            return Yii::t('shop', 'From') . ' <strong>' . $discount->from_date . '</strong> ' . Yii::t('shop', 'To') . ' <strong>' . ($discount->to_date ?: Yii::t('shop', 'No limit')) . '</strong>';
                        }
                    ],
                    [
                        'attribute' => 'type',
                        'label' => Yii::t('shop', 'Discount Type'),
                        'value' => function ($discount) {
                            return ShopDiscountForm::typeName($discount->type);
                        }
                    ],
                    [
                        'attribute' => 'value',
                        'label' => Yii::t('shop', 'Value'),
                        'format' => 'raw',
                        'value' => function ($discount) {
                            return ShopDiscountHelper::getDiscountValue($discount->type, $discount->value);
                        }
                    ],
                    [
                        'attribute' => 'recipient',
                        'label' => Yii::t('shop', 'Discount Recipient'),
                        'value' => function ($discount) {
                            return ShopDiscountForm::recipientName($discount->recipient);
                        }
                    ],
                    [
                        'attribute' => 'value_recipient',
                        'label' => Yii::t('shop', 'Value'),
                        'format' => 'raw',
                        'value' => function ($discount) {
                            return ShopDiscountHelper::getDiscountRecipientValue($discount->recipient, $discount->value_recipient);
                        }
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
