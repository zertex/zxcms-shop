<?php

use common\modules\shop\entities\ShopDiscount;
use common\modules\shop\forms\search\ShopDiscountSearch;
use common\modules\shop\forms\ShopDiscountForm;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\shop\helpers\PriceHelper;

/* @var $this yii\web\View */
/* @var $searchModel ShopDiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Discounts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Discount'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('shop', 'Discount Name'),
                        'value' => function (ShopDiscount $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'type',
                        'filter' => ShopDiscountForm::typeList(),
                        'label' => Yii::t('shop', 'Discount Type'),
                        'value' => function (ShopDiscount $model) {
                            return ShopDiscountForm::typeName($model->type);
                        },
                        'options' => ['style' => 'width: 250px;'],
                    ],
                    [
                        'attribute' => 'recipient',
                        'filter' => ShopDiscountForm::recipientList(),
                        'label' => Yii::t('shop', 'Discount Recipient'),
                        'value' => function (ShopDiscount $model) {
                            return ShopDiscountForm::recipientName($model->recipient);
                        },
                        'options' => ['style' => 'width: 250px;'],
                    ],
                    [
                        'attribute' => 'percent',
                        'label' => Yii::t('shop', 'Discount'),
                        'value' => function (ShopDiscount $model) {
                            return $model->percent . ' ' . ($model->method == ShopDiscount::METHOD_PERCENT ? '%' : PriceHelper::currencyTag());
                        },
                        'options' => ['style' => 'width: 100px;'],
                    ],
                    [
                        'label' => Yii::t('shop', 'Status'),
                        'format' => 'raw',
                        'value' => function (ShopDiscount $discount) {
                            $datetime1 = new DateTime(date('Y-m-d', $discount->date_from));
                            $datetime2 = new DateTime(date('Y-m-d'));
                            $interval = $datetime1->diff($datetime2);
                            $sign = $interval->format('%R');
                            $from = $sign == '+';
                            if ($discount->date_to) {
                                $datetime1 = new DateTime(date('Y-m-d', $discount->date_to));
                                $interval = $datetime1->diff($datetime2);
                                $sign = $interval->format('%R');
                                $days = $interval->format('%a');
                                $to = $sign == '-' || ($sign == '+' && $days == '0');
                            } else {
                                $to = true;
                            }
                            $activeDate = $to && $from;
                            if ($discount->status == ShopDiscount::STATUS_DISABLED) {
                                return '<span class="text-danger">' . Yii::t('shop', 'Disabled Discount') . '</span>';
                            } elseif (!$activeDate) {
                                return '<span class="text-danger">' . Yii::t('shop', 'Time Expired') . '</span>';
                            } else {
                                return '<span class="text-success">' . Yii::t('shop', 'Enabled Discount') . '</span>';
                            }
                        },
                        'options' => ['style' => 'width: 350px;'],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
