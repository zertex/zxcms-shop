<?php

use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\helpers\PriceHelper;
use common\modules\shop\helpers\ShopProductHelper;
use common\modules\shop\forms\search\ShopProductSearch;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ShopProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('shop', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'rowOptions'   => function (ShopProduct $model) {
                    return $model->quantity <= 0 ? ['style' => 'background: #fdc'] : [];
                },
                'columns'      => [
                    [
                        'value'          => function (ShopProduct $model) {
                            return $model->mainPhoto ? Html::img($model->mainPhoto->getThumbFileUrl('file', 'admin')) : null;
                        },
                        'format'         => 'raw',
                        'contentOptions' => ['style' => 'width: 100px'],
                    ],
                    [
                        'attribute' => 'id',
                        'label'     => 'ID',
                        'options'   => ['style' => 'width: 100px;'],
                    ],
                    [
                        'attribute' => 'code',
                        'label'     => Yii::t('shop', 'Code'),
                        'options'   => ['style' => 'width: 150px;'],
                    ],
                    [
                        'attribute' => 't_name',
                        'label'     => Yii::t('shop', 'Product Name'),
                        'value'     => function (ShopProduct $model) {
                            $label = $model->label && $model->label > 0 ? '<br><i class="fa fa-flag ribbon-color_' . $model->label . '" aria-hidden="true"></i> ' . ShopProductHelper::labelName($model->label) : '';
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]) . $label;
                        },
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'category_id',
                        'label'     => Yii::t('shop', 'Category'),
                        'filter'    => $searchModel->categoriesList(),
                        'value'     => 'category.translation.name',
                    ],
                    [
                        'attribute' => 'price_new',
                        'label'     => Yii::t('shop', 'Price New'),
                        'value'     => function (ShopProduct $model) {
                            return PriceHelper::format($model->price_new);
                        },
                        'options'   => ['style' => 'width: 150px;'],
                    ],
                    [
                        'attribute'      => 'quantity',
                        'label'          => Yii::t('shop', 'Quantity'),
                        'contentOptions' => ['style' => 'width: 100px'],
                    ],
                    [
                        'attribute'      => 'status',
                        'label'          => Yii::t('shop', 'Status'),
                        'filter'         => $searchModel->statusList(),
                        'value'          => function (ShopProduct $model) {
                            return ShopProductHelper::statusLabel($model->status);
                        },
                        'format'         => 'raw',
                        'contentOptions' => ['style' => 'width: 120px'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
