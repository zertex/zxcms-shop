<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $product \common\modules\shop\entities\product\ShopProduct
 * @var $model \common\modules\shop\forms\product\ShopProductEditForm
 * @var $characteristics array
 */
/* @var $this \yii\web\View */

$this->title = Yii::t('shop', 'Change Characteristics');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($product->translation->name), 'url' => ['view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-change-characteristics">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-body">

            <!-- < ?php foreach ($model->values as $i => $value): ?> -->
            <?php foreach ($characteristics as $i => $value) : ?>
                <?php if ($value->group) : ?>
                    <h4 class="text-left" style="padding:10px; background: #d9edf7"><?= $value->groupLabel ?></h4>
                <?php endif; ?>
                <?php if ($variants = $value->variantsList()) : ?>
                    <?= $form->field($value, '[' . $i . ']value')->dropDownList($variants, ['prompt' => '']) ?>
                <?php else : ?>
                    <?= $form->field($value, '[' . $i . ']value')->textInput() ?>
                <?php endif ?>
            <?php endforeach; ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
