<?php

use kartik\file\FileInput;
use common\modules\shop\entities\product\ShopModification;
use common\modules\shop\helpers\PriceHelper;
use common\modules\shop\helpers\ShopProductHelper;
use common\modules\shop\helpers\ShopWeightHelper;
use yii\bootstrap\ActiveForm;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\detail\DetailView as KartikDetailView;
use common\modules\shop\entities\product\ShopProduct;

/* @var $this yii\web\View */
/* @var $product \common\modules\shop\entities\product\ShopProduct */
/* @var $photosForm \common\modules\shop\forms\product\ShopPhotosForm */
/* @var $modificationsProvider yii\data\ActiveDataProvider */
/* @var $characteristics array */

$this->title                   = $product->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="product-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Products'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php if ($product->isActive()) : ?>
            <?= Html::a(
                Yii::t('shop', 'Deactivate'),
                ['draft', 'id' => $product->id],
                ['class' => 'btn btn-primary', 'data-method' => 'post']
            ) ?>
        <?php else : ?>
            <?= Html::a(
                Yii::t('shop', 'Activate'),
                ['activate', 'id' => $product->id],
                ['class' => 'btn btn-success', 'data-method' => 'post']
            ) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $product->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $product->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this product?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">

            <div class="box">
                <div class="box-header with-border"><?= Yii::t('shop', 'Common') ?></div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model'      => $product,
                        'attributes' => [
                            [
                                'attribute' => 'id',
                                'label'     => 'ID',
                            ],
                            [
                                'attribute' => 'status',
                                'label'     => Yii::t('shop', 'Status'),
                                'value'     => ShopProductHelper::statusLabel($product->status),
                                'format'    => 'raw',
                            ],
                            [
                                'attribute' => 'brand_id',
                                'label'     => Yii::t('shop', 'Brand Name'),
                                'value'     => ArrayHelper::getValue($product, 'brand.translation.name'),
                            ],
                            [
                                'attribute' => 'code',
                                'label'     => Yii::t('shop', 'Code'),
                            ],
                            [
                                'attribute' => 'category_id',
                                'label'     => Yii::t('shop', 'Category'),
                                'value'     => ArrayHelper::getValue($product, 'category.translation.name'),
                            ],
                            [
                                'label' => Yii::t('shop', 'Other Categories'),
                                'value' => implode(', ', ArrayHelper::getColumn($product->categories, 'name')),
                            ],
                            [
                                'attribute' => 'slug',
                                'label'     => Yii::t('shop', 'SEO Link'),
                            ],
                            [
                                'attribute' => 'type_id',
                                'label'     => Yii::t('shop', 'Product Type'),
                                'value'     => ArrayHelper::getValue($product, 'type.translation.name'),
                            ],
                            [
                                'label' => Yii::t('shop', 'Tags'),
                                'value' => implode(', ', ArrayHelper::getColumn($product->tags, 'name')),
                            ],
                            [
                                'attribute' => 'quantity',
                                'label'     => Yii::t('shop', 'Quantity'),
                            ],
                            [
                                'attribute' => 'weight',
                                'label'     => Yii::t('shop', 'Weight'),
                                'value'     => ShopWeightHelper::format($product->weight),
                            ],
                            [
                                'attribute' => 'price_new',
                                'label'     => Yii::t('shop', 'Price New'),
                                'value'     => PriceHelper::format($product->price_new),
                            ],
                            [
                                'attribute' => 'price_old',
                                'label'     => Yii::t('shop', 'Price Old'),
                                'value'     => PriceHelper::format($product->price_old),
                            ],
                        ],
                    ]) ?>
                    <br/>
                    <p>
                        <?= Html::a(
                            Yii::t('shop', 'Change Price'),
                            ['price', 'id' => $product->id],
                            ['class' => 'btn btn-primary']
                        ) ?>
                        <?php if ($product->canChangeQuantity()) : ?>
                            <?= Html::a(
                                Yii::t('shop', 'Change Quantity'),
                                ['quantity', 'id' => $product->id],
                                ['class' => 'btn btn-primary']
                            ) ?>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">

            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('shop', 'Characteristics') ?></div>
                <div class="box-body">

                    <?= KartikDetailView::widget([
                        'model'      => $product,
                        'hAlign'     => 'left',
                        'attributes' => $characteristics,
                    ]) ?>
                    <p>
                        <?= Html::a(
                            Yii::t('shop', 'Change Characteristics'),
                            ['/shop/manage/product/change-characteristics', 'id' => $product->id],
                            ['class' => 'btn btn-primary']
                        ) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_view_tab', [
                'product' => $product,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]) ?>
    </div>

    <div class="box" id="modifications">
        <div class="box-header with-border"><?= Yii::t('shop', 'Modifications') ?></div>
        <div class="box-body">
            <p>
                <?= Html::a(
                    Yii::t('shop', 'Create Modification'),
                    ['/shop/manage/modification/create', 'product_id' => $product->id],
                    ['class' => 'btn btn-success']
                ) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $modificationsProvider,
                'columns'      => [
                    [
                        'attribute' => 'code',
                        'label'     => Yii::t('shop', 'Code'),
                    ],
                    [
                        'attribute' => 'name',
                        'label'     => Yii::t('shop', 'Modification Name'),
                        'value'     => 'translation.name',
                    ],
                    [
                        'attribute' => 'price',
                        'label'     => Yii::t('shop', 'Price'),
                        'value'     => function (ShopModification $model) {
                            return PriceHelper::format($model->price);
                        },
                    ],
                    [
                        'attribute' => 'quantity',
                        'label'     => Yii::t('shop', 'Quantity'),
                    ],
                    [
                        'class'      => ActionColumn::class,
                        'controller' => '/shop/manage/modification',
                        'template'   => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <div class="box" id="photos">
        <div class="box-header with-border"><?= Yii::t('shop', 'Photos') ?></div>
        <div class="box-body">

            <div class="row">
                <?php foreach ($product->photos as $photo) : ?>
                    <div class="col-md-2 col-xs-3" style="text-align: center">
                        <div class="btn-group">
                            <?= Html::a(
                                '<span class="glyphicon glyphicon-arrow-left"></span>',
                                ['move-photo-up', 'id' => $product->id, 'photo_id' => $photo->id],
                                [
                                    'class'       => 'btn btn-default',
                                    'data-method' => 'post',
                                ]
                            ); ?>
                            <?= Html::a(
                                '<span class="glyphicon glyphicon-remove"></span>',
                                ['delete-photo', 'id' => $product->id, 'photo_id' => $photo->id],
                                [
                                    'class'        => 'btn btn-default',
                                    'data-method'  => 'post',
                                    'data-confirm' => 'Remove photo?',
                                ]
                            ); ?>
                            <?= Html::a(
                                '<span class="glyphicon glyphicon-arrow-right"></span>',
                                ['move-photo-down', 'id' => $product->id, 'photo_id' => $photo->id],
                                [
                                    'class'       => 'btn btn-default',
                                    'data-method' => 'post',
                                ]
                            ); ?>
                        </div>
                        <div>
                            <?= Html::a(
                                Html::img($photo->getThumbFileUrl('file', 'thumb')),
                                $photo->getUploadedFileUrl('file'),
                                ['class' => 'thumbnail', 'target' => '_blank']
                            ) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($photosForm, 'files[]')->label(false)->widget(FileInput::class, [
                'options'       => [
                    'accept'   => 'image/*',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showUpload' => false,
                ],
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('shop', 'Upload'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
