<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\helpers\ShopProductHelper;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $product ShopProduct
 * @var $language string
 */

?>

<?= DetailView::widget([
    'model'      => $product,
    'attributes' => [
        [
            'attribute' => 'name',
            'label'     => Yii::t('shop', 'Product Name'),
            'value'     => function (ShopProduct $model) use ($language) {
                $label = $model->label && $model->label > 0 ? '<br><i class="fa fa-flag ribbon-color_' . $model->label . '" aria-hidden="true"></i> ' . ShopProductHelper::labelName($model->label) : '';
                return Html::a(Html::encode($model->findTranslation($language)->name), ['view', 'id' => $model->id]) . $label;
            },
            'format'    => 'raw',
        ],
    ],
]) ?>

<div class="box">
    <div class="box-header with-border"><?= Yii::t('shop', 'SEO') ?></div>
    <div class="box-body">

        <?= DetailView::widget([
            'model'      => $product,
            'attributes' => [
                [
                    'label' => Yii::t('shop', 'META Title'),
                    'value' => function (ShopProduct $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_title;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Description'),
                    'value' => function (ShopProduct $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_description;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Keywords'),
                    'value' => function (ShopProduct $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_keywords;
                    }
                ],
            ],
        ]) ?>
    </div>
</div>

<div class="box">
    <div class="box-header with-border"><?= Yii::t('shop', 'Product Description') ?></div>
    <div class="box-body">
        <?= Yii::$app->formatter->asHtml($product->findTranslation($language)->description, [
            'Attr.AllowedRel'      => ['nofollow'],
            'HTML.SafeObject'      => true,
            'Output.FlashCompat'   => true,
            'HTML.SafeIframe'      => true,
            'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
        ]) ?>
    </div>
</div>