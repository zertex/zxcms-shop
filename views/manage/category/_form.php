<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\shop\forms\ShopCategoryForm;

/* @var $this yii\web\View */
/* @var $model ShopCategoryForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('shop', 'Common') ?></div>
        <div class="box-body">
            <?= $form->field($model, 'parentId')->dropDownList($model->parentCategoriesList()) ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_form_tab', [
                'form'     => $form,
                'model'    => $model,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
