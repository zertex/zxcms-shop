<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopCategoryForm */

$this->title = Yii::t('shop', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
