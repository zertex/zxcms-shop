<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\ShopCategory;

/**
 * @var $this \yii\web\View
 * @var $category ShopCategory
 * @var $language string
 */

?>

<?= DetailView::widget([
    'model'      => $category,
    'attributes' => [
        [
            'label' => Yii::t('shop', 'Title'),
            'value' => function (ShopCategory $entity) use ($language) {
                return $entity->findTranslation($language)->title;
            }
        ],
    ],
]) ?>

<div class="box">
    <div class="box-header with-border"><?= Yii::t('shop', 'SEO') ?></div>
    <div class="box-body">

        <?= DetailView::widget([
            'model'      => $category,
            'attributes' => [
                [
                    'label' => Yii::t('shop', 'META Title'),
                    'value' => function (ShopCategory $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_title;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Description'),
                    'value' => function (ShopCategory $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_description;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Keywords'),
                    'value' => function (ShopCategory $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_keywords;
                    }
                ],
            ],
        ]) ?>
    </div>
</div>

<div class="box">
    <div class="box-header with-border"><?= Yii::t('shop', 'Category Description') ?></div>
    <div class="box-body">
        <?= Yii::$app->formatter->asHtml($category->findTranslation($language)->description, [
            'Attr.AllowedRel'      => ['nofollow'],
            'HTML.SafeObject'      => true,
            'Output.FlashCompat'   => true,
            'HTML.SafeIframe'      => true,
            'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
        ]) ?>
    </div>
</div>