<?php

/* @var $this yii\web\View */
/* @var $category \common\modules\shop\entities\ShopCategory */
/* @var $model \common\modules\shop\forms\ShopCategoryForm */

$this->title = Yii::t('shop', 'Update Category: {name}', ['name' => $category->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $category->translation->name, 'url' => ['view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
