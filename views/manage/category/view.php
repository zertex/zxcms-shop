<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $category \common\modules\shop\entities\ShopCategory */

$this->title = $category->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="category-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Categories'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $category->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $category->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this category?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-header with-border"><?= Yii::t('shop', 'Common') ?></div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $category,
                'attributes' => [
                    [
                        'attribute' => 'id',
                        'label' => 'ID',
                    ],
                    [
                        'attribute' => 'slug',
                        'label' => Yii::t('shop', 'SEO Link'),
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_view_tab', [
                'category' => $category,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]) ?>
    </div>

</div>
