<?php

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\forms\search\ShopCategorySearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ShopCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => Yii::t('shop', 'ID'),
                        'options' => ['style' => 'width: 100px;'],
                    ],
                    [
                        'attribute' => 't_name',
                        'label' => Yii::t('shop', 'Category Name'),
                        'value' => function (ShopCategory $model) {
                            $indent = ($model->depth > 1 ? str_repeat('&nbsp;&nbsp;', $model->depth - 1) . ' ' : '');
                            return $indent . Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'value' => function (ShopCategory $model) {
                            return
                                Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                                Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align: center'],
                        'options' => ['style' => 'width: 60px;'],
                    ],
                    [
                        'attribute' => 'slug',
                        'label' => Yii::t('shop', 'SEO Link'),
                    ],
                    [
                        'attribute' => 't_title',
                        'value' => function (ShopCategory $category) {
                            return $category->translation->title;
                        },
                        'label' => Yii::t('shop', 'Category Title'),
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
                'tableOptions' => ['class' => 'table table-striped table-hover'],
            ]); ?>
        </div>
    </div>
</div>
