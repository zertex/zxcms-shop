<?php

use common\modules\shop\entities\product\ShopReview;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\shop\helpers\ShopReviewHelper;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\shop\forms\search\ShopReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Reviews');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-reviews-index">

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'text',
                        'label' => Yii::t('shop', 'Review'),
                        'format' => 'ntext',
                    ],
                    [
                        'attribute' => 'vote',
                        'label' => Yii::t('shop', 'Rating'),
                        'format' => 'raw',
                        'filter' => $searchModel->voteList(),
                        'value' => function (ShopReview $review) {
                            $starWidget = \kartik\rating\StarRating::widget([
                                'name' => 'rating_' . $review->id,
                                'value' => $review->vote,
                                'pluginOptions' => [
                                    'displayOnly' => true,
                                    'size' => 'xs',
                                ]
                            ]);

                            return '<small>' . $starWidget . '</small>';
                        },
                        'options' => ['style' => 'width: 180px;'],
                    ],
                    [
                        'attribute' => 'product_id',
                        'label' => Yii::t('shop', 'Product'),
                        'value' => function (ShopReview $model) {
                            return Html::a($model->product->translation->name, ['/shop/manage/product/view', 'id' => $model->product->id]);
                        },
                        'format' => 'raw',
                        'options' => ['style' => 'width: 30%;'],
                    ],
                    [
                        'attribute' => 'user_id',
                        'label' => Yii::t('shop', 'User'),
                        'value' => function (ShopReview $model) {
                            return Html::a($model->user->username, ['/users/manage/manage/view', 'id' => $model->user->id]);
                        },
                        'format' => 'raw',
                        'options' => ['style' => 'width: 10%;'],
                    ],
                    [
                        'attribute' => 'active',
                        'label' => Yii::t('shop', 'Status'),
                        'filter' => $searchModel->statusList(),
                        'value' => function (ShopReview $model) {
                            return ShopReviewHelper::statusLabel($model->active);
                        },
                        'format' => 'raw',
                        'options' => ['style' => 'width: 150px;'],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template'=>'{status} {view} {update} {delete}',
                        'buttons'=>[
                            'status'=>function ($url, $model) {
                                return \yii\helpers\Html::a(
                                    '<span class="glyphicon glyphicon-refresh"></span>',
                                    [
                                        '/shop/manage/review/status',
                                        'id' => $model->id
                                    ],
                                    [
                                        'title' => Yii::t('shop', 'Change Status'),
                                        'data-method' => 'post',
                                    ]
                                );
                            }
                        ],
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
