<?php

/* @var $this yii\web\View */
/* @var $review \common\modules\shop\entities\product\ShopReview */
/* @var $model \common\modules\shop\forms\ShopReviewForm */

$this->title = Yii::t('shop', 'Update Review');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $review->id, 'url' => ['view', 'id' => $review->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="review-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
