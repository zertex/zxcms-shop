<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopReviewForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-body">
            <!-- < ?= $form->field($model, 'vote')->textInput(['maxlength' => true]) ?> -->

            <?= $form->field($model, 'vote')->widget(\kartik\rating\StarRating::class, [
            'pluginOptions' => [
            'size'=>'sm',
            'min' => 0,
            'max' => 5,
            'step' => 1,
            'showClear' => false,
            'showCaption' => false,

            ]
            ]) ?>

            <?= $form->field($model, 'text')->textarea() ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
