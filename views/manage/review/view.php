<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\shop\entities\product\ShopReview;

/* @var $this yii\web\View */
/* @var $review ShopReview */

$this->title = Yii::t('shop', 'Review') . ': ' . $review->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Reviews'), ['index'], ['class' => 'btn btn-default']) ?>

        <?php if ($review->isActive()) : ?>
            <?= Html::a(Yii::t('shop', 'Deactivate'), ['/shop/manage/review/status', 'id' => $review->id], ['class' => 'btn btn-primary']) ?>
        <?php else : ?>
            <?= Html::a(Yii::t('shop', 'Publish'), ['/shop/manage/review/status', 'id' => $review->id], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>

        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $review->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $review->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this review?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $review,
                'attributes' => [
                    [
                        'attribute' => 'vote',
                        'label' => Yii::t('shop', 'Rating'),
                        'format' => 'raw',
                        'value' => function (ShopReview $review) {
                            $starWidget = \kartik\rating\StarRating::widget([
                                'name' => 'rating_' . $review->id,
                                'value' => $review->vote,
                                'pluginOptions' => [
                                    'displayOnly' => true,
                                    'size' => 'xs',
                                ]
                            ]);

                            return '<small>' . $starWidget . '</small>' . date('d-m-Y h:i', $review->created_at);
                        },
                    ],
                    [
                        'attribute' => 'text',
                        'format' => 'ntext',
                        'label' => Yii::t('shop', 'Review'),
                    ],
                    [
                        'attribute' => 'product_id',
                        'label' => Yii::t('shop', 'Product'),
                        'value' => function (ShopReview $model) {
                            return Html::a($model->product->translation->name, ['/shop/manage/product/view', 'id' => $model->product->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'user_id',
                        'label' => Yii::t('shop', 'User'),
                        'value' => function (ShopReview $model) {
                            return Html::a($model->user->username, ['/users/manage/manage/view', 'id' => $model->user->id]);
                        },
                        'format' => 'raw',
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
