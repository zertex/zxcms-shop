<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopDeliveryMethodForm */

$this->title = Yii::t('shop', 'Create Delivery Method');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Delivery Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="method-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
