<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\ShopDeliveryMethod;

/**
 * @var $this \yii\web\View
 * @var $method ShopDeliveryMethod
 * @var $language string
 */

echo DetailView::widget([
    'model'      => $method,
    'attributes' => [
        [
            'label' => Yii::t('shop', 'Name'),
            'value' => function (ShopDeliveryMethod $entity) use ($language) {
                return $entity->findTranslation($language)->name;
            }
        ],
    ],
]);
