<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\shop\helpers\ShopWeightHelper;
use common\modules\shop\helpers\PriceHelper;
use common\modules\shop\entities\ShopDeliveryMethod;

/* @var $this yii\web\View */
/* @var $method ShopDeliveryMethod */

$this->title = $method->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Delivery Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Delivery Methods'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $method->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $method->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this method?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_view_tab', [
                'method' => $method,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]) ?>
    </div>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $method,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'cost',
                        'label' => Yii::t('shop', 'Cost'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return PriceHelper::format($model->cost);
                        },
                    ],
                    [
                        'attribute' => 'min_weight',
                        'label' => Yii::t('shop', 'Min Weight'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return ShopWeightHelper::format($model->min_weight);
                        },
                    ],
                    [
                        'attribute' => 'max_weight',
                        'label' => Yii::t('shop', 'Max Weight'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return ShopWeightHelper::format($model->max_weight);
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
