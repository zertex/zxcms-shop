<?php

/* @var $this yii\web\View */
/* @var $method \common\modules\shop\entities\ShopDeliveryMethod */
/* @var $model \common\modules\shop\forms\ShopDeliveryMethodForm */

$this->title = Yii::t('shop', 'Update Delivery Method: {name}', ['name' => $method->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Delivery Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $method->translation->name, 'url' => ['view', 'id' => $method->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="method-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
