<?php

use common\modules\shop\entities\ShopDeliveryMethod;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\shop\helpers\ShopWeightHelper;
use common\modules\shop\helpers\PriceHelper;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\shop\forms\search\ShopDeliveryMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Delivery Methods');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Create Method'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 't_name',
                        'label' => Yii::t('shop', 'Method Name'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'cost',
                        'label' => Yii::t('shop', 'Cost'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return PriceHelper::format($model->cost);
                        },
                    ],
                    [
                        'attribute' => 'min_weight',
                        'label' => Yii::t('shop', 'Min Weight'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return ShopWeightHelper::format($model->min_weight);
                        },
                    ],
                    [
                        'attribute' => 'max_weight',
                        'label' => Yii::t('shop', 'Max Weight'),
                        'value' => function (ShopDeliveryMethod $model) {
                            return ShopWeightHelper::format($model->max_weight);
                        },
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
