<?php

/* @var $this yii\web\View */
/* @var $product \common\modules\shop\entities\product\ShopProduct */
/* @var $modification \common\modules\shop\entities\product\ShopModification */
/* @var $model \common\modules\shop\forms\product\ShopModificationForm */

$this->title = Yii::t('shop', 'Update Modification: {name}', ['name' => $modification->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Products'), 'url' => ['/shop/product/index']];
$this->params['breadcrumbs'][] = ['label' => $product->translation->name, 'url' => ['/shop/product/view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = $modification->translation->name;
?>
<div class="modification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
