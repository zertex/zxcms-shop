<?php

/* @var $this yii\web\View */
/* @var $product \common\modules\shop\entities\product\ShopProduct */
/* @var $model \common\modules\shop\forms\product\ShopModificationForm */

$this->title = Yii::t('shop', 'Create Modification');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Products'), 'url' => ['/shop/product/index']];
$this->params['breadcrumbs'][] = ['label' => $product->translation->name, 'url' => ['/shop/product/view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modification-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
