<?php

use yii\helpers\Html;

/* @var $user \core\entities\user\User */
?>

<div>
    <div style="float: left">
        <img style="width:70px;" src="<?= Yii::$app->avatar->show(Yii::$app->user->identity->user->username) ?>"
    </div>
    <div style="float: left; padding: 0 0 0 10px;">
        <strong><?= $user->username ?></strong><br>
        ID: <?= $user->id ?><br>
        E-mail: <?= Html::a($user->email, 'mailto:' . $user->email) ?><br>
    </div>
</div>
