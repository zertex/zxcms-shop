<?php

/* @var $this yii\web\View */
/* @var $order \common\modules\shop\entities\order\ShopOrder */
/* @var $model \common\modules\shop\forms\order\manage\ShopOrderEditForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('shop', 'Update Order: {num}', ['num' => $order->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $order->id, 'url' => ['view', 'id' => $order->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="order-update">

    <?php $form = ActiveForm::begin() ?>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('shop', 'Customer') ?></div>
        <div class="box-body">
            <?= $form->field($model->customer, 'phone')->textInput() ?>
            <?= $form->field($model->customer, 'name')->textInput() ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('shop', 'Delivery') ?></div>
        <div class="box-body">
            <?= $form->field($model->delivery, 'method')->dropDownList($model->delivery->deliveryMethodsList(), ['prompt' => Yii::t('shop', '--- Select ---')]) ?>
            <?= $form->field($model->delivery, 'index')->textInput() ?>
            <?= $form->field($model->delivery, 'address')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('shop', 'Note') ?></div>
        <div class="box-body">
            <?= $form->field($model, 'note')->textarea(['rows' => 3])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
