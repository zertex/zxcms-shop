<?php

use common\modules\shop\entities\order\ShopOrder;
use common\modules\shop\helpers\ShopOrderHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\shop\forms\search\ShopOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a(Yii::t('shop', 'Export'), ['export'], ['class' => 'btn btn-primary', 'data-method' => 'post', 'data-confirm' => 'Export?']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => Yii::t('shop', 'Order ID'),
                        'value' => function (ShopOrder $model) {
                            return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'label' => Yii::t('shop', 'Date'),
                    ],
                    [
                        'attribute' => 'current_status',
                        'label' => Yii::t('shop', 'Status'),
                        'filter' => $searchModel->statusList(),
                        'value' => function (ShopOrder $model) {
                            return ShopOrderHelper::statusLabel($model->current_status);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                    ],
                ],
                'tableOptions' => ['class' => 'table table-striped table-hover'],
            ]); ?>
        </div>
    </div>
</div>
