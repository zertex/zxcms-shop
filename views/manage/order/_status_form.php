<?php
use yii\widgets\ActiveForm;
use common\modules\shop\helpers\ShopOrderHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $order \common\modules\shop\entities\order\ShopOrder */
/* @var $statusForm \common\modules\shop\forms\order\ShopStatusChangeForm */

$statusForm->status_id = $order->current_status;
$statusForm->order_id = $order->id;
?>

<?php $form = ActiveForm::begin() ?>

<div class="input-group">
    <?= $form->field($statusForm, 'status_id', [
        'options' => [
            'tag' => null, // Don't wrap with "form-group" div
        ],
    ])->dropDownList(ShopOrderHelper::statusList())->label(false) ?>
    <span class="input-group-addon">
        <?= $form->field($statusForm, 'mail_user', [
            'template' => '{input}',
            'options' => [
                'tag' => null, // Don't wrap with "form-group" div
            ],
        ])->checkbox(['label' => false])->label(false) ?>
        <?= Yii::t('shop', 'Inform the buyer') ?>
    </span>
    <span class="input-group-btn">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-primary']) ?>
    </span>
</div>
    <?= $form->field($statusForm, 'note')->textarea()->label(Yii::t('shop', 'Status Note')) ?>
    <?= $form->field($statusForm, 'order_id')->hiddenInput()->label(false) ?>
<?php ActiveForm::end(); ?>
