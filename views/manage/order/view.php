<?php

use common\modules\shop\helpers\ShopOrderHelper;
use common\modules\shop\forms\order\ShopStatusChangeForm;
use common\modules\shop\helpers\PriceHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use core\entities\user\User;

/* @var $this yii\web\View */
/* @var $order \common\modules\shop\entities\order\ShopOrder */

$this->title = Yii::t('shop', 'Order {num}', ['num' => $order->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Orders'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $order->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $order->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this order?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $order,
                'attributes' => [
                    [
                        'attribute' => 'id',
                        'label' => Yii::t('shop', 'Order ID'),
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'label' => Yii::t('shop', 'Created At'),
                    ],
                    [
                        'label' => Yii::t('shop', 'Status'),
                        'format' => 'raw',
                        'value' => function ($order) {
                            /* @var $order \common\modules\shop\entities\order\ShopOrder */
                            $statusForm = new ShopStatusChangeForm();
                            if ($statusForm->load(Yii::$app->request->post()) && $statusForm->validate()) {
                                try {
                                    $order->addStatusById($statusForm);
                                    Yii::$app->session->setFlash('success', Yii::t('shop', 'Order status is changed.'));
                                } catch (DomainException $e) {
                                    Yii::$app->session->setFlash('danger', $e->getMessage());
                                }
                            }

                            return $this->render('_status_form', [
                                'statusForm' => $statusForm,
                                'order' => $order,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'user_id',
                        'label' => Yii::t('shop', 'User'),
                        'format' => 'raw',
                        'value' => function ($order) {
                            $user = User::findOne($order->user_id);
                            return $this->render('_user', [
                                'user' => $user,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'delivery_method_name',
                        'label' => Yii::t('shop', 'Delivery Method'),
                        'value' => function ($order) {
                            /* @var $order \common\modules\shop\entities\order\ShopOrder */
                            return $order->delivery_method_name . ' (' . PriceHelper::format($order->delivery_cost) . ')';
                        },
                    ],
                    [
                        'attribute' => 'deliveryData.index',
                        'label' => Yii::t('shop', 'Delivery Post Index'),
                    ],
                    [
                        'attribute' => 'deliveryData.address',
                        'label' => Yii::t('shop', 'Delivery Address'),
                    ],
                    [
                        'attribute' => 'cost',
                        'label' => Yii::t('shop', 'Cost'),
                        'value' => function ($order) {
                            return PriceHelper::format($order->cost);
                        },
                    ],
                    [
                        'attribute' => 'note',
                        'format' => 'ntext',
                        'label' => Yii::t('shop', 'Note'),
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                    <tr>
                        <th class="text-left"><?= Yii::t('shop', 'Product Name') ?></th>
                        <th class="text-left"><?= Yii::t('shop', 'Modification') ?></th>
                        <th class="text-left"><?= Yii::t('shop', 'Quantity') ?></th>
                        <th class="text-right"><?= Yii::t('shop', 'Price') ?></th>
                        <th class="text-right"><?= Yii::t('shop', 'Total') ?></th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($order->items as $item) : ?>
                        <tr <?= $item->isHidden() ? 'style="background: #cccccc"' : '' ?>>
                            <td class="text-left">
                                <?= Html::encode($item->product_code) ?> <?= $item->isHidden() ? Html::tag('span', Yii::t('shop', 'Removed'), ['class' => 'label label-danger']) : '' ?><br />
                                <?= Html::encode($item->product_name) ?>
                            </td>
                            <td class="text-left">
                                <?= Html::encode($item->modification_code) ?><br />
                                <?= Html::encode($item->modification_name) ?>
                            </td>
                            <td class="text-left">
                                <?= $item->quantity ?>
                            </td>
                            <td class="text-right"><?= PriceHelper::format($item->price) ?></td>
                            <td class="text-right"><?= PriceHelper::format($item->getCost()) ?></td>

                            <?php if ($item->isHidden()) : ?>
                                <td class="text-center">
                                    <?= Html::a('<i class="fa fa-undo" aria-hidden="true"></i>', ['activate-item', 'id' => $item->id, 'order_id' => $order->id], ['data-method' => 'post', 'title' => Yii::t('shop', 'Return Product')]) ?>
                                </td>
                            <?php else : ?>
                                <td class="text-center">
                                    <?= Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', ['hide-item', 'id' => $item->id, 'order_id' => $order->id], ['data-method' => 'post', 'title' => Yii::t('shop', 'Remove Product')]) ?>
                                </td>
                            <?php endif; ?>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                    <tr>
                        <th class="text-left"><?= Yii::t('shop', 'Date') ?></th>
                        <th class="text-left"><?= Yii::t('shop', 'Status') ?></th>
                        <th class="text-left"><?= Yii::t('shop', 'Manager') ?></th>
                        <th class="text-left"><?= Yii::t('shop', 'Status Note') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($order->statuses as $status) : ?>
                        <tr>
                            <td class="text-left">
                                <?= Yii::$app->formatter->asDatetime($status->created_at) ?>
                            </td>
                            <td class="text-left">
                                <?= ShopOrderHelper::statusLabel($status->value) ?>
                            </td>
                            <td class="text-left">
                                <?= (isset($status->manager_id) && $user = User::findOne($status->manager_id)) ? $user->username : '---'; ?>
                            </td>
                            <td class="text-left">
                                <?= isset($status->note) ? Html::encode($status->note) : ''  ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
