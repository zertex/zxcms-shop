<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\shop\entities\ShopProductType;

/**
 * @var $this \yii\web\View
 * @var $type ShopProductType
 * @var $language string
 */

?>

<?= DetailView::widget([
    'model'      => $type,
    'attributes' => [
        [
            'label' => Yii::t('shop', 'Name'),
            'value' => function (ShopProductType $entity) use ($language) {
                return $entity->findTranslation($language)->name;
            }
        ],
    ],
]) ?>

<div class="box">
    <div class="box-header with-border"><?= Yii::t('shop', 'SEO') ?></div>
    <div class="box-body">

        <?= DetailView::widget([
            'model'      => $type,
            'attributes' => [
                [
                    'label' => Yii::t('shop', 'META Title'),
                    'value' => function (ShopProductType $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_title;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Description'),
                    'value' => function (ShopProductType $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_description;
                    }
                ],
                [
                    'label' => Yii::t('blog', 'META Keywords'),
                    'value' => function (ShopProductType $entity) use ($language) {
                        return $entity->findTranslation($language)->meta_keywords;
                    }
                ],
            ],
        ]) ?>
    </div>
</div>