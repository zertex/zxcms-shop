<?php
/**
 * Created by Error202
 * Date: 28.08.2018
 */

/**
 * @var $this \yii\web\View
 * @var $form \yii\widgets\ActiveForm
 * @var $model \common\modules\shop\forms\ShopProductTypeForm
 * @var $language string
 */

$postfix = $language == Yii::$app->params['defaultLanguage'] ? '' : '_' . $language;
?>

<div class="box box-default">
    <div class="box-body">
        <?= $form->field($model, 'name' . $postfix)->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="box box-default">
    <div class="box-header with-border"><?= Yii::t('shop', 'SEO') ?></div>
    <div class="box-body">
        <?= $form->field($model, 'meta_title' . $postfix)->textInput() ?>
        <?= $form->field($model, 'meta_description' . $postfix)->textarea(['rows' => 2]) ?>
        <?= $form->field($model, 'meta_keywords' . $postfix)->textInput() ?>
    </div>
</div>
