<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopProductTypeForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('shop', 'Common') ?></div>
        <div class="box-body">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <!--
    <div class="box box-default">
        <div class="box-header with-border">< ?= Yii::t('shop', 'Characteristic Group') ?></div>
        <div class="box-body" style="-webkit-column-count:3; -moz-column-count:3; column-count:3;">
            < ?= $form->field($model, 'groups_json')->checkboxList($model->groupsList())->label(false); ?>
        </div>
    </div>
    -->

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_form_tab', [
                'form'     => $form,
                'model'    => $model,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
