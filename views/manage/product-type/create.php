<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\forms\ShopProductTypeForm */

$this->title = Yii::t('shop', 'Create Product Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Product Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
