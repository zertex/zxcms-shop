<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $type \common\modules\shop\entities\ShopProductType */
/* @var $model \common\modules\shop\forms\ShopProductTypeForm */
/* @var $data array */

$this->title = $type->translation->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Product Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="product-type-view">

    <p>
        <?= Html::a(Yii::t('shop', 'Product Types'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('shop', 'Edit'), ['update', 'id' => $type->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $type->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('shop', 'Are you sure you want to delete this product type?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">

            <div class="box">
                <div class="box-header with-border"><?= Yii::t('shop', 'Common') ?></div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $type,
                        'attributes' => [
                            [
                                'attribute' => 'slug',
                                'label' => Yii::t('shop', 'SEO Link'),
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_view_tab', [
                        'type' => $type,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

            <div class="nav-tabs-custom">
                <?= \yii\bootstrap\Tabs::widget([
                    'items' => $items,
                ]) ?>
            </div>


        </div>
        <div class="col-md-6">

            <div class="box">
                <div class="box-header with-border"><?= Yii::t('shop', 'Characteristics') ?></div>
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>
                        <?=
                            $form->field($model, 'existing')->widget(Select2::class, [
                                'data' => $data,
                                'options' => ['placeholder' => Yii::t('shop', 'Select a characteristics...'), 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                        <p>
                            <?= Html::submitButton(Yii::t('shop', 'Save'), ['class' => 'btn btn-success']) ?>
                        </p>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>

    </div>



</div>
