<?php

/* @var $this yii\web\View */
/* @var $type \common\modules\shop\entities\ShopProductType */
/* @var $model \common\modules\shop\forms\ShopProductTypeForm */

$this->title = Yii::t('shop', 'Update Product Type: {name}', ['name' => $type->translation->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Product Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $type->translation->name, 'url' => ['view', 'id' => $type->id]];
$this->params['breadcrumbs'][] = Yii::t('shop', 'Editing');
?>
<div class="product-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
