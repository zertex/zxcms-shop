<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<div class="product_listing_controls">
    <ul class="product_listing_toggle">
        <li id="toggle_grid" class="active"><i class="fa fa-th"></i></li>
        <li id="toggle_list"><i class="fa fa-th-list"></i></li>
        <li><a href="<?= Url::to(['/shop/compare/index']) ?>" id="compare-total" class="btn btn-link" target="_blank"><?= Yii::t('shop_public','Product Compare') ?> <?= !empty(Yii::$app->session->get('compare', [])) ? '<span id="sj-compare-indicator" class="compare_count_indicator">( ' . count(Yii::$app->session->get('compare', [])) . ' )</span>' : '<span id="sj-compare-indicator" class="compare_count_indicator"></span>' ?></a></li>
    </ul>

    <div class="sort_by">
        <label><?= Yii::t('shop_public', 'Sort by') ?></label>

        <select id="sort_by_select" class="form-control" onchange="location = this.value;">
            <?php
            $values = [
                '' => Yii::t('shop_public', 'Default'),
                //'name' => Yii::t('shop_public', 'Name (A - Z)'),
                //'-name' => Yii::t('shop_public', 'Name (Z - A)'),
                'price' => Yii::t('shop_public', 'Price (Low &gt; High)'),
                '-price' => Yii::t('shop_public', 'Price (High &gt; Low)'),
                '-rating' => Yii::t('shop_public', 'Rating (Highest)'),
                'rating' => Yii::t('shop_public', 'Rating (Lowest)'),
            ];
            $current = Yii::$app->request->get('sort');
            ?>
            <?php foreach ($values as $value => $label): ?>
                <option value="<?= Html::encode(Url::current(['sort' => $value ?: null])) ?>" <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $label ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="show_products">
        <label><?= Yii::t('shop_public','Show') ?></label>

        <select id="show_products_select" class="form-control" onchange="location = this.value;">
            <?php
            $values = [15, 25, 50, 75, 100];
            $current = $dataProvider->getPagination()->getPageSize();
            ?>
            <?php foreach ($values as $value): ?>
                <option value="<?= Html::encode(Url::current(['per-page' => $value])) ?>" <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $value ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>



<div class="product_listing_main row">
    <?php  $i = 1; foreach ($dataProvider->getModels() as $product):
        $itemClass = $i <= 3 ? $i : $i % 3;
        ?>
        <?= $this->render('_product', [
            'product' => $product,
            'itemClass' => 'item3_' . $itemClass,
        ]) ?>
    <?php $i++; endforeach; ?>
</div>

<div class="row">
    <div class="col-sm-6 text-left">
        <?= LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ]) ?>
    </div>
    <div class="col-sm-6 text-right"><?= Yii::t('shop_public', 'Showing {count} of {total}', ['count' => $dataProvider->getCount(), 'total' => $dataProvider->getTotalCount()]) ?></div>
</div>