<?php

use common\modules\shop\helpers\PriceHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\shop\helpers\ShopWeightHelper;
use common\modules\shop\entities\ShopCategory;
use kartik\widgets\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $product \common\modules\shop\entities\product\ShopProduct */
/* @var $cartForm \common\modules\shop\forms\ShopAddToCartForm */
/* @var $reviewForm \common\modules\shop\forms\ShopReviewForm */
/* @var $reviewProvider \common\modules\shop\entities\product\ShopReview[] */
/* @var $characteristics array */

$this->title = $product->translation->name;

$this->registerMetaTag(['name' => 'description', 'content' => $product->translation->meta_description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $product->translation->meta_keywords]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Catalog'), 'url' => ['index']];
foreach ($product->category->parents as $parent) {
    if (!$parent->isRoot()) {
        $this->params['breadcrumbs'][] = [
            'label' => $parent->translation->name,
            'url'   => ['category', 'id' => $parent->id]
        ];
    }
}
$this->params['breadcrumbs'][] = [
    'label' => $product->category->translation->name,
    'url'   => ['category', 'id' => $product->category->id]
];
$this->params['breadcrumbs'][] = $product->translation->name;

$this->params['active_category'] = $product->category;

//\app\assets\ElevateZoomAsset::register($this);
//\app\assets\BxSliderAsset::register($this);
/*
$js = <<<JS
$(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) 
{
    location.hash = e.target.hash;
});

if (location.hash.length) 
{
    $('[data-toggle="tab"], [data-toggle="pill"]').filter('[href="' + location.hash + '"]').tab('show');
}
JS;
$this->registerJs($js, $this::POS_READY);
*/

$codeText     = Yii::t('shop_public', 'Code');
$quantityText = Yii::t('shop_public', 'Quantity');

$formatJs = <<<FJS
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    
var items = repo.text.split('||');    
    
var markup =
'<div class="row">' + 
    '<div class="col-sm-8">' +
        '<strong>' + items[1] + '</strong><br>' +
        '{$codeText}: ' + items[0] + '<br>' +
        '{$quantityText}: ' + items[3] + '<br>' +
    '</div>' +
    '<div class="col-sm-4">' + items[2] + '</div>' +
'</div>';
if (repo.description) {
      markup += '<h5>' + repo.description + '</h5>';
    }
    return '<div style="overflow:hidden;">' + markup + '</div>';
};

var formatRepoSelection = function (repo) {
    //return repo.full_name || repo.text;
    var items = repo.text.split('||');
    if (!items[1])
    {
        return '';
    }
    return items[1] + ' (' + items[2] + ')';
}
FJS;
$this->registerJs($formatJs, $this::POS_HEAD);

$inCompare = in_array($product->id, Yii::$app->session->get('compare', []));
$inWish    = Yii::$app->user->isGuest ? false : Yii::$app->user->identity->user->inWishlist($product->id);
?>


    <div itemscope="" itemtype="http://schema.org/Product" class="product-scope">

        <?php
        $mainOriginImage  = isset($product->photos[0]) ? $product->photos[0]->getThumbFileUrl('file',
            'catalog_origin') : '';
        $mainProductImage = isset($product->photos[0]) ? $product->photos[0]->getThumbFileUrl('file',
            'catalog_product_main') : '';
        ?>

        <meta itemprop="url" content="<?= Url::canonical() ?>">
        <meta itemprop="image" content="<?= $mainOriginImage ?>">

        <div class="product_wrap">
            <div class="row">
                <div class="col-sm-5 col-md-4 product_images product_left">


                    <?php if (isset($product->photos[0])) : ?>
                        <div class="elevatezoom_big_wrapper">
                            <div style="height:360px;width:360px;" class="zoomWrapper">
                                <img id="elevatezoom_big" src="<?= $mainProductImage ?>"
                                     alt="<?= Html::encode($product->name) ?>" data-zoom-image="<?= $mainOriginImage ?>"
                                     style="position: absolute; width: 360px; height: 360px;">
                            </div>
                        </div>
                    <?php endif; ?>


                    <div class="bx-wrapper" style="max-width: 404px;">
                        <div class="bx-viewport"
                             style="width: 100%; overflow: hidden; position: relative; height: 85px;">
                            <div id="elevatezoom_gallery"
                                 style="width: 1075%; position: relative; transition-duration: 0s; transform: translate3d(-570px, 0px, 0px);">


                                <?php foreach ($product->photos as $i => $photo) : ?>
                                    <?php if ($i == 0) : ?>
                                        <a data-image="<?= $photo->getThumbFileUrl('file', 'catalog_product_main') ?>"
                                           data-zoom-image="<?= $photo->getThumbFileUrl('file', 'catalog_origin') ?>"
                                           href="#"
                                           style="float: left; list-style: none; position: relative; width: 111px;"
                                           class="bx-clone">
                                            <img src="<?= $photo->getThumbFileUrl('file', 'catalog_product_main') ?>"
                                                 alt="<?= Html::encode($product->name) ?>"/>
                                        </a>
                                    <?php else : ?>
                                        <a data-image="<?= $photo->getThumbFileUrl('file',
                                            'catalog_product_additional') ?>"
                                           data-zoom-image="<?= $photo->getThumbFileUrl('file', 'catalog_origin') ?>"
                                           href="#"
                                           style="float: left; list-style: none; position: relative; width: 111px;"
                                           class="bx-clone">
                                            <img src="<?= $photo->getThumbFileUrl('file',
                                                'catalog_product_additional') ?>" alt=""/>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-sm-7 col-md-8">


                    <!--<form action="/cart/add" method="post" enctype="multipart/form-data" id="product-actions">-->
                    <div class="product_info__wrapper">
                        <div class="product_info__left">
                            <div itemprop="name" class="product_name"><?= Html::encode($product->name) ?></div>

                            <div class="options clearfix">
                                <div class="variants-wrapper hidden">
                                    <div class="selector-wrapper">
                                        <select class="single-option-selector" data-option="option1"
                                                id="product-select-option-0">
                                            <option value="Default Title">Default Title</option>
                                        </select>
                                    </div>
                                    <select id="product-select" name="id" style="display: none;">
                                        <option value="4792929668">Default Title - $80.00</option>
                                    </select>
                                </div>
                            </div>

                            <div class="product_details">
                                <p class="product_details__item product_weight"><label for=""><?= Yii::t('shop_public',
                                            'Weight') ?>:</label>
                                    <span id="product_weight"><?= ShopWeightHelper::format($product->weight) ?></span>
                                </p>

                                <?php //todo Barcode, ISDN... ?>
                                <!--
                                <p class="product_details__item product_sku"><label for="">SKU:</label> <span id="product_sku">327038</span></p>
                                <p class="product_details__item product_barcode"><label for="">Barcode:</label> <span id="product_barcode">555-6322-1</span></p>
                                -->
                            </div>

                            <div class="product_details">

                                <p class="product_details__item product_collections">
                                    <label for=""><?= Yii::t('shop_public', 'Categories') ?>:</label>
                                    <?= implode(', ', array_map(
                                        function (ShopCategory $category) {
                                            return Html::a(
                                                Html::encode($category->translation->name),
                                                ['/shop/catalog/category', 'id' => $category->id]
                                            );
                                        },
                                        array_merge([$product->category], $product->categories)
                                    )) ?>
                                </p>

                                <p class="product_details__item product_type">
                                    <label for=""><?= Yii::t('shop_public', 'Product Type') ?>:</label> <a
                                        href="<?= Html::encode(Url::to([
                                            'product-type',
                                            'id' => $product->type->id
                                        ])) ?>"
                                        title="<?= Html::encode($product->type->name) ?>"><?= Html::encode($product->type->name) ?></a>
                                </p>

                                <p class="product_details__item product_vendor">
                                    <label for=""><?= Yii::t('shop_public', 'Vendor') ?>:</label> <a
                                        href="<?= Html::encode(Url::to([
                                            'brand',
                                            'id' => $product->brand->id
                                        ])) ?>"><?= Html::encode($product->brand->name) ?></a>
                                </p>

                                <p class="product_details__item product_tags">
                                    <label for=""><?= Yii::t('shop_public', 'Tags') ?>:</label>
                                    <?php foreach ($product->tags as $tag) : ?>
                                        <span><a href="<?= Html::encode(Url::to([
                                                '/shop/catalog/tag',
                                                'id' => $tag->id
                                            ])) ?>"><?= Html::encode($tag->name) ?></a></span>
                                    <?php endforeach; ?>
                                </p>

                            </div>
                        </div>

                        <div class="product_info__right">

                            <div>
                                <?= Yii::t('shop_public', 'Product Code: {code}', ['code' => $product->code]) ?>
                            </div>

                            <span id="sj-compare-<?= $product->id ?>">

                            <p class="btn-group">
                                    <?php if (!Yii::$app->user->isGuest) : ?>
                                        <?= Html::a('
                                            <span class="fa-stack fa-lg">
                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                                            </span>',
                                            $inWish ? [
                                                '/shop/wishlist/delete',
                                                'id' => $product->id
                                            ] : ['/shop/wishlist/add', 'id' => $product->id],
                                            [
                                                'class'             => $inWish ? 'link-red' . ' sjax' : 'link-blue' . ' sjax',
                                                'title'             => $inWish ? Yii::t('shop_public',
                                                    'Remove from Wish') : Yii::t('shop_public', 'Add to Wish'),
                                                'data'              => [
                                                    'method' => 'post',
                                                ],
                                                'data-toggle'       => 'tooltip',
                                                'data-sjax-id'      => 'sj-compare-' . $product->id,
                                                'data-sjax-method'  => 'post',
                                                'data-sjax-type'    => 'success',
                                                'data-sjax-message' => $inWish ? Yii::t('shop_public',
                                                    'Product removed from wish list.') : Yii::t('shop_public',
                                                    'Product added to wish list.'),
                                            ]
                                        )
                                        ?>
                                    <?php endif; ?>



                                    <?= Html::a('
                                            <span class="fa-stack fa-lg">
                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                                            </span>',
                                        $inCompare ? [
                                            '/shop/compare/delete',
                                            'id' => $product->id
                                        ] : ['/shop/compare/add', 'id' => $product->id],
                                        [
                                            'class'             => $inCompare ? 'link-red' . ' sjax' : 'link-blue' . ' sjax',
                                            'title'             => $inCompare ? Yii::t('shop_public',
                                                'Remove from compare') : Yii::t('shop_public', 'Add to compare'),
                                            'data'              => [
                                                'method' => 'post',
                                            ],
                                            'data-toggle'       => 'tooltip',
                                            'data-sjax-id'      => 'sj-compare-' . $product->id,
                                            'data-sjax-method'  => 'post',
                                            'data-sjax-type'    => 'success',
                                            'data-sjax-ids'     => '["sj-compare-indicator"]',
                                            'data-sjax-message' => $inCompare ? Yii::t('shop_public',
                                                'Product successfully removed from comparison.') : Yii::t('shop_public',
                                                'Product successfully added for comparison.'),
                                        ]
                                    )
                                    ?>
                            </p>
                            </span>

                            <div id="product_price">
                                <p class="price product-price">
                                    <span class="money">
                                        <?= PriceHelper::format($product->getCalculatedNewPrice()) ?>
                                    </span>

                                    <?php if ($product->getCalculatedOldPrice()) : ?>

                                        <span class="money compare-at-price money_sale" data-currency-usd="$88.00">
                                        <?= PriceHelper::format($product->getCalculatedOldPrice()) ?>
                                    </span><span
                                            class="money_sale_percent">-<?= round(100 - ($product->getCalculatedNewPrice() / $product->getCalculatedOldPrice()) * 100) ?>
                                            %</span>

                                    <?php endif; ?>
                                </p>
                            </div>

                            <p class="product_details__item" id="product_quantity">
                                <?php if (Yii::$app->settings->data['shop']['quantity_use'] == 0 || Yii::$app->params['settings']['shop']['quantity_show'] == 0) : ?>
                                    <?php if ($product->isAvailable()) : ?>
                                        <label for=""><span class="notify_success"><?= Yii::t('shop_public', 'In stock') ?></span></label>
                                    <?php else : ?>
                                        <label for=""><span class="notify_danger"><?= Yii::t('shop_public', 'Out of stock') ?>:</span></label>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <label for=""><?= Yii::t('shop_public', 'Availability') ?>:</label> <span
                                        class="notify_success"><b><?= $product->quantity ?></b></span>
                                <?php endif; ?>
                            </p>
                            <div id="purchase">

                                <?php if ($product->isAvailable()) : ?>

                                    <?php $form = ActiveForm::begin([
                                        'action' => ['/shop/cart/add', 'id' => $product->id],
                                        'id'     => 'add-cart-form',
                                    ]) ?>

                                    <?php if ($modifications = $cartForm->modificationsListSelect2()) : ?>
                                        <!-- < ?= $form->field($cartForm, 'modification', ['options' => ['tag' => null]])->dropDownList($modifications, ['prompt' => Yii::t('shop_public', '--- Select ---')]) ?> -->

                                        <?=
                                        $form->field($cartForm, 'modification',
                                            ['options' => ['tag' => null]])->widget(Select2::class, [
                                            'data'          => $modifications,
                                            'options'       => [
                                                'placeholder' => Yii::t('shop_public', '--- Select ---')
                                            ],
                                            'pluginOptions' => [
                                                //'allowClear' => true
                                                'minimumResultsForSearch' => -1,
                                                'escapeMarkup'            => new JsExpression('function (markup) { return markup; }'),
                                                'templateResult'          => new JsExpression('formatRepo'),
                                                'templateSelection'       => new JsExpression('formatRepoSelection'),
                                            ],
                                        ]);
                                        ?>

                                    <?php endif; ?>

                                    <label for="quantity"><?= Yii::t('shop_public', 'Quantity') ?>:</label>

                                    <div class="quantity_box">
                                        <?= $form->field($cartForm, 'quantity', [
                                            'template' => '{input}',
                                            'options'  => ['tag' => null]
                                        ])->textInput(['class' => 'quantity_input'])->label(false) ?>
                                        <span class="quantity_modifier quantity_down"><i class="fa fa-minus"></i></span>
                                        <span class="quantity_modifier quantity_up"><i class="fa fa-plus"></i></span>
                                    </div>

                                    <?= Html::submitButton(Yii::t('shop_public', 'Add to Cart'),
                                        ['id' => 'add-to-cart', 'class' => 'btn btn-cart']) ?>

                                    <?= $form->errorSummary($cartForm); ?>

                                    <?php ActiveForm::end() ?>

                                <?php else : ?>

                                    <div class="alert alert-danger">
                                        <?= Yii::t('shop_public', 'The product is not available for purchasing right now. Add it to your wishlist.') ?>
                                    </div>

                                <?php endif; ?>
                            </div>

                            <div class="rating">
                                <p>
                                    <?= \kartik\rating\StarRating::widget([
                                        'name'          => 'rating_35',
                                        'value'         => $product->rating,
                                        'pluginOptions' => [
                                            'displayOnly' => true,
                                            'size'        => 'xs',
                                        ]
                                    ]); ?>
                                    <br>
                                    <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?= Yii::t('shop_public',
                                            '{count} reviews', ['count' => 0]) ?></a> / <a href=""
                                                                                           onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?= Yii::t('shop_public',
                                            'Write a review') ?></a>
                                </p>
                            </div>

                            <div class="addthis_sharing_toolbox"
                                 data-url="https://theme203-sport.myshopify.com/products/extra-thick-exercise-yoga-mat-with-carry-strap"
                                 data-title="Extra Thick Exercise Yoga Mat with Carry Strap | sports store"
                                 style="clear: both;">
                                <div id="atstbx"
                                     class="at-share-tbx-element addthis-smartlayers addthis-animated at4-show"
                                     aria-labelledby="at-611a466d-abf8-4e6a-a1fb-97c543764301" role="region">
                                    <span id="at-611a466d-abf8-4e6a-a1fb-97c543764301"
                                          class="at4-visually-hidden"><?= Yii::t('shop_public', 'Share this') ?></span>
                                    <div class="at-share-btn-elements">
                                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                                        <script src="//yastatic.net/share2/share.js"></script>
                                        <div class="ya-share2"
                                             data-services="collections,vkontakte,facebook,odnoklassniki,gplus"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--</form>-->

                </div>
            </div><!-- /.row -->

            <div class="tab-description">
                <!-- TABS START -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-description" data-toggle="tab"><?= Yii::t('shop_public',
                                'Description') ?></a></li>
                    <li><a href="#tab-specification" data-toggle="tab"><?= Yii::t('shop_public', 'Specification') ?></a>
                    </li>
                    <li><a href="#tab-review" data-toggle="tab"><?= Yii::t('shop_public', 'Reviews') ?></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-description"><p>
                            <?= Yii::$app->formatter->asHtml($product->description, [
                                'Attr.AllowedRel'      => array('nofollow'),
                                'HTML.SafeObject'      => true,
                                'Output.FlashCompat'   => true,
                                'HTML.SafeIframe'      => true,
                                'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
                            ]) ?>
                    </div>
                    <div class="tab-pane" id="tab-specification">

                        <?= \kartik\detail\DetailView::widget([
                            'model'      => $product,
                            'hAlign'     => 'left',
                            'attributes' => $characteristics,
                            'bordered'   => false,
                            'striped'    => false,
                            'options'    => [
                                'class' => 'char_detail'
                            ],
                        ]) ?>


                    </div>
                    <div class="tab-pane" id="tab-review">

                        <div class="panel">
                            <div class="panel-body">

                                <div id="review">
                                    <?php \yii\widgets\Pjax::begin([
                                        'clientOptions' => [
                                            'method'  => 'POST',
                                            'timeout' => 7000
                                        ]
                                    ]); ?>
                                    <?= \yii\grid\GridView::widget([
                                        'dataProvider' => $reviewProvider,
                                        'summary'      => '',
                                        'showHeader'   => false,
                                        'emptyText'    => Yii::t('shop_public', 'There are no reviews. Be the first.'),
                                        'columns'      => [
                                            [
                                                'attribute' => 'vote',
                                                'format'    => 'raw',
                                                'value'     => function (
                                                    \common\modules\shop\entities\product\ShopReview $review
                                                ) {
                                                    $starWidget = \kartik\rating\StarRating::widget([
                                                        'name'          => 'rating_' . $review->id,
                                                        'value'         => $review->vote,
                                                        'pluginOptions' => [
                                                            'displayOnly' => true,
                                                            'size'        => 'xs',
                                                        ]
                                                    ]);

                                                    return '<small>' . $starWidget . '</small>' . date('d-m-Y h:i',
                                                            $review->created_at);
                                                },
                                                'options'   => ['style' => 'width: 150px;'],
                                            ],
                                            [
                                                'attribute' => 'text',
                                                'format'    => 'raw',
                                                'value'     => function (
                                                    \common\modules\shop\entities\product\ShopReview $review
                                                ) {
                                                    return '<strong>' . $review->user->username . '</strong><br>' . nl2br(Html::encode($review->text));
                                                }
                                            ]
                                        ],
                                        'tableOptions' => ['class' => 'table'],
                                    ]) ?>
                                    <?php \yii\widgets\Pjax::end(); ?>
                                </div>

                                <h2><?= Yii::t('shop_public', 'Write a review') ?></h2>

                                <?php if (Yii::$app->user->isGuest): ?>

                                    <div class="panel-panel-info">
                                        <div class="panel-body">
                                            <?= Yii::t('shop_public', 'Please {login} for writing a review.', [
                                                'login' => Html::a(Yii::t('shop_public', 'Log In'),
                                                    ['/users/auth/login'])
                                            ]) ?>
                                        </div>
                                    </div>

                                <?php else: ?>

                                    <?php $form = ActiveForm::begin(['id' => 'form-review']) ?>

                                    <?= $form->field($reviewForm,
                                        'vote')->widget(\kartik\rating\StarRating::classname(), [
                                        'pluginOptions' => [
                                            'size'        => 'sm',
                                            'min'         => 0,
                                            'max'         => 5,
                                            'step'        => 1,
                                            'showClear'   => false,
                                            'showCaption' => false,

                                        ]
                                    ]) ?>
                                    <?= $form->field($reviewForm, 'text')->textarea(['rows' => 5]) ?>

                                    <div class="form-group">
                                        <?= Html::submitButton(Yii::t('shop_public', 'Send'), [
                                            'class' => 'btn btn-primary btn-lg btn-block',
                                            'style' => 'width:150px'
                                        ]) ?>
                                    </div>

                                    <?php ActiveForm::end() ?>

                                <?php endif; ?>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- TABS END -->
            </div>
        </div><!-- /.product_wrap -->
    </div>


<?php
$js = <<<ELEVATE
jQuery(function($) {
    $(window).load(function(){
        // PRODUCT IMAGE ZOOM
        $("#elevatezoom_big").elevateZoom({
            gallery : "elevatezoom_gallery",
            zoomActivation: "hover",
            zoomType : "window",
            scrollZoom : true,
            zoomWindowFadeIn : 500,
            zoomLensFadeIn: 500,
            imageCrossfade: true,
            zoomWindowWidth : 370,
            zoomWindowHeight : 370,
            zoomWindowOffetx : 15,
            zoomWindowOffety : 0,
            borderSize : 1,
            borderColour : "#e7e7e7"
        });


        // BIG IMAGE FANCYBOX
        $("#elevatezoom_big").bind("click", function() {
            $.fancybox( $('#elevatezoom_big').data('elevateZoom').getGalleryList() );
            return false;
        });
    });


    // THUMBS SLIDER
    $('#elevatezoom_gallery').bxSlider({
        infiniteLoop: true,
        minSlides: 1,
        maxSlides: 4,
        moveSlides: 1,
        slideWidth: 101,
        pager: false,
        prevText: '',
        nextText: ''
    });
});
ELEVATE;
$this->registerJs($js, $this::POS_READY);
?>