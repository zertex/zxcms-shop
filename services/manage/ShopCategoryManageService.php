<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\forms\ShopCategoryForm;
use common\modules\shop\repositories\ShopCategoryRepository;
use common\modules\shop\repositories\ShopProductRepository;
use Yii;

class ShopCategoryManageService
{
    private $_categories;
    private $_products;

    public function __construct(ShopCategoryRepository $categories, ShopProductRepository $products)
    {
        $this->_categories = $categories;
        $this->_products = $products;
    }

    public function create(ShopCategoryForm $form): ShopCategory
    {
        $parent = $this->_categories->get($form->parentId);
        $category = ShopCategory::create(
            $form,
            $form->slug
        );
        $category->appendTo($parent);
        $this->_categories->save($category);
        return $category;
    }

    public function edit($id, ShopCategoryForm $form): void
    {
        $category = $this->_categories->get($id);
        $this->assertIsNotRoot($category);
        $category->edit(
            $form,
            $form->slug
        );
        if ($form->parentId !== $category->parent->id) {
            $parent = $this->_categories->get($form->parentId);
            $category->appendTo($parent);
        }
        $this->_categories->save($category);
    }

    public function moveUp($id): void
    {
        $category = $this->_categories->get($id);
        $this->assertIsNotRoot($category);
        if ($prev = $category->prev) {
            $category->insertBefore($prev);
        }
        $this->_categories->save($category);
    }

    public function moveDown($id): void
    {
        $category = $this->_categories->get($id);
        $this->assertIsNotRoot($category);
        if ($next = $category->next) {
            $category->insertAfter($next);
        }
        $this->_categories->save($category);
    }

    public function remove($id): void
    {
        $category = $this->_categories->get($id);
        $this->assertIsNotRoot($category);
        if ($this->_products->existsByMainCategory($category->id)) {
            throw new \DomainException(Yii::t('shop', 'Unable to remove category with products.'));
        }
        $this->_categories->remove($category);
    }

    private function assertIsNotRoot(ShopCategory $category): void
    {
        if ($category->isRoot()) {
            throw new \DomainException('Unable to manage the root category.');
        }
    }
}
