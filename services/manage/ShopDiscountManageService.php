<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopDiscount;
use common\modules\shop\forms\ShopDiscountForm;
use common\modules\shop\repositories\ShopDiscountRepository;

class ShopDiscountManageService
{
    private $_discounts;

    public function __construct(ShopDiscountRepository $discounts)
    {
        $this->_discounts = $discounts;
    }

    public function create(ShopDiscountForm $form): ShopDiscount
    {
        $discount = ShopDiscount::create(
            $form,
            $form->percent,
            $form->date_from,
            $form->date_to,
            $form->sort,
            !$form->for_count ? $form->count : -1,
            $form->type,
            $form->recipient,
            $form->method,
            $form->code,
            $form->value,
            $form->value_recipient,
            $form->status
        );
        $this->_discounts->save($discount);
        return $discount;
    }

    public function edit($id, ShopDiscountForm $form): void
    {
        $discount = $this->_discounts->get($id);
        $discount->edit(
            $form,
            $form->percent,
            $form->date_from,
            $form->date_to,
            $form->sort,
            !$form->for_count ? $form->count : -1,
            $form->type,
            $form->recipient,
            $form->method,
            $form->code,
            $form->value,
            $form->value_recipient,
            $form->status
        );
        $this->_discounts->save($discount);
    }

    public function remove($id): void
    {
        $discount = $this->_discounts->get($id);
        $this->_discounts->remove($discount);
    }

    public function activate($id): void
    {
        $discount = $this->_discounts->get($id);
        $discount->activate();
        $this->_discounts->save($discount);
    }

    public function draft($id): void
    {
        $discount = $this->_discounts->get($id);
        $discount->draft();
        $this->_discounts->save($discount);
    }

    public function downCounter($id): void
    {
        $discount = $this->_discounts->get($id);
        $discount->downCounter();

        if ($discount->count == 0) {
            $this->_discounts->remove($discount);
        } else {
            $this->_discounts->save($discount);
        }
    }
}
