<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\forms\ShopCharacteristicGroupForm;
use common\modules\shop\repositories\ShopCharacteristicGroupRepository;

class ShopCharacteristicGroupManageService
{
    private $_characteristics;

    public function __construct(ShopCharacteristicGroupRepository $characteristics)
    {
        $this->_characteristics = $characteristics;
    }

    public function create(ShopCharacteristicGroupForm $form): ShopCharacteristicGroup
    {
        $characteristic = ShopCharacteristicGroup::create(
            $form,
            $form->sort
        );
        $this->_characteristics->save($characteristic);
        return $characteristic;
    }

    public function edit($id, ShopCharacteristicGroupForm $form): void
    {
        $characteristic = $this->_characteristics->get($id);
        $characteristic->edit(
            $form,
            $form->sort
        );
        $this->_characteristics->save($characteristic);
    }

    public function remove($id): void
    {
        $characteristic = $this->_characteristics->get($id);
        if ($this->_characteristics->existsByGroup($characteristic->id)) {
            throw new \DomainException(\Yii::t('shop', 'Unable to remove group with characteristics.'));
        }
        $this->_characteristics->remove($characteristic);
    }
}
