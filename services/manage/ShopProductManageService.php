<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\entities\ShopCharacteristicAssignment;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\entities\ShopTag;
use common\modules\shop\events\ShopProductActivateEvent;
use common\modules\shop\events\ShopProductDraftEvent;
use common\modules\shop\forms\product\ShopQuantityForm;
use common\modules\shop\forms\product\ShopModificationForm;
use common\modules\shop\forms\product\ShopPhotosForm;
use common\modules\shop\forms\product\ShopPriceForm;
use common\modules\shop\forms\product\ShopProductCreateForm;
use common\modules\shop\forms\product\ShopProductEditForm;
use common\modules\shop\forms\product\ShopValueForm;
use common\modules\shop\repositories\ShopBrandRepository;
use common\modules\shop\repositories\ShopCategoryRepository;
use common\modules\shop\repositories\ShopProductRepository;
use common\modules\shop\repositories\ShopProductTypeRepository;
use common\modules\shop\repositories\ShopTagRepository;
use core\services\TransactionManager;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

class ShopProductManageService
{
    private $_products;
    private $_brands;
    private $_types;
    private $_categories;
    private $_tags;
    private $_transaction;

    public function __construct(
        ShopProductRepository $products,
        ShopBrandRepository $brands,
        ShopCategoryRepository $categories,
        ShopTagRepository $tags,
        TransactionManager $transaction,
        ShopProductTypeRepository $types
    ) {
        $this->_products = $products;
        $this->_brands = $brands;
        $this->_categories = $categories;
        $this->_tags = $tags;
        $this->_transaction = $transaction;
        $this->_types = $types;
    }

    public function create(ShopProductCreateForm $form): ShopProduct
    {
        $brand = $this->_brands->get($form->brandId);
        $type = $this->_types->get($form->typeId);
        $category = $this->_categories->get($form->categories->main);

        $product = ShopProduct::create(
            $form,
            $brand->id,
            $category->id,
            $form->code,
            $form->slug,
            $type->id,
            $form->weight,
            $form->label,
            $form->quantity->quantity
        );

        $product->setPrice($form->price->new, $form->price->old);

        foreach ($form->categories->others as $otherId) {
            $category = $this->_categories->get($otherId);
            $product->assignCategory($category->id);
        }

        foreach ($form->photos->files as $file) {
            $product->addPhoto($file);
        }

        foreach ($form->tags->existing as $tagId) {
            $tag = $this->_tags->get($tagId);
            $product->assignTag($tag->id);
        }

        $this->_transaction->wrap(function () use ($product, $form) {
            foreach ($form->tags->newNames as $tagName) {
                if (!$tag = $this->_tags->findByName($tagName)) {
                    $tag = ShopTag::create($tagName, Inflector::slug($tagName, '_'));
                    $this->_tags->save($tag);
                }
                $product->assignTag($tag->id);
            }
            $this->_products->save($product);
        });

        return $product;
    }

    public function edit($id, ShopProductEditForm $form): void
    {
        $product = $this->_products->get($id);
        $brand = $this->_brands->get($form->brandId);
        $category = $this->_categories->get($form->categories->main);
        $type = $this->_types->get($form->typeId);

        $product->edit(
            $form,
            $brand->id,
            $form->code,
            $form->slug,
            $type->id,
            $form->weight,
            $form->label
        );

        $product->changeMainCategory($category->id);

        $this->_transaction->wrap(function () use ($product, $form) {

            $product->revokeCategories();
            $product->revokeTags();
            $this->_products->save($product);

            foreach ($form->categories->others as $otherId) {
                $category = $this->_categories->get($otherId);
                $product->assignCategory($category->id);
            }

            //foreach ($form->values as $value) {
            //    $product->setValue($value->id, $value->value);
            //}

            foreach ($form->tags->existing as $tagId) {
                $tag = $this->_tags->get($tagId);
                $product->assignTag($tag->id);
            }
            foreach ($form->tags->newNames as $tagName) {
                if (!$tag = $this->_tags->findByName($tagName)) {
                    $tag = ShopTag::create($tagName, Inflector::slug($tagName, '_'));
                    $this->_tags->save($tag);
                }
                $product->assignTag($tag->id);
            }
            $this->_products->save($product);
        });
    }

    public function editCharacteristics($id, array $values): void
    {
        $product = $this->_products->get($id);
        $this->_transaction->wrap(function () use ($product, $values) {
            foreach ($values as $value) {
                $product->setValue($value->id, $value->value);
            }
            $this->_products->save($product);
        });
    }

    public function changePrice($id, ShopPriceForm $form): void
    {
        $product = $this->_products->get($id);
        $product->setPrice($form->new, $form->old);
        $this->_products->save($product);
    }

    public function changeQuantity($id, ShopQuantityForm $form): void
    {
        $product = $this->_products->get($id);
        $product->changeQuantity($form->quantity);
        $this->_products->save($product);
    }

    public function activate($id): void
    {
        $product = $this->_products->get($id);
        $product->activate();
        $this->_products->save($product);
        $product->recordEvent(new ShopProductActivateEvent($product));
    }

    public function draft($id): void
    {
        $product = $this->_products->get($id);
        $product->draft();
        $this->_products->save($product);
        $product->recordEvent(new ShopProductDraftEvent($product));
    }

    public function addPhotos($id, ShopPhotosForm $form): void
    {
        $product = $this->_products->get($id);
        foreach ($form->files as $file) {
            $product->addPhoto($file);
        }
        $this->_products->save($product);
    }

    public function movePhotoUp($id, $photoId): void
    {
        $product = $this->_products->get($id);
        $product->movePhotoUp($photoId);
        $this->_products->save($product);
    }

    public function movePhotoDown($id, $photoId): void
    {
        $product = $this->_products->get($id);
        $product->movePhotoDown($photoId);
        $this->_products->save($product);
    }

    public function removePhoto($id, $photoId): void
    {
        $product = $this->_products->get($id);
        $product->removePhoto($photoId);
        $this->_products->save($product);
    }

    public function addRelatedProduct($id, $otherId): void
    {
        $product = $this->_products->get($id);
        $other = $this->_products->get($otherId);
        $product->assignRelatedProduct($other->id);
        $this->_products->save($product);
    }

    public function removeRelatedProduct($id, $otherId): void
    {
        $product = $this->_products->get($id);
        $other = $this->_products->get($otherId);
        $product->revokeRelatedProduct($other->id);
        $this->_products->save($product);
    }

    public function addModification($id, ShopModificationForm $form): void
    {
        $product = $this->_products->get($id);
        $product->addModification(
            $form,
            $form->code,
            $form->price,
            $form->quantity
        );
        $this->_products->save($product);
    }

    public function editModification($id, $modificationId, ShopModificationForm $form): void
    {
        $product = $this->_products->get($id);
        $product->editModification(
            $modificationId,
            $form,
            $form->code,
            $form->price,
            $form->quantity
        );
        $this->_products->save($product);
    }

    public function removeModification($id, $modificationId): void
    {
        $product = $this->_products->get($id);
        $product->removeModification($modificationId);
        $this->_products->save($product);
    }

    public function remove($id): void
    {
        $product = $this->_products->get($id);
        $this->_products->remove($product);
    }

    public function prepareCharacteristicGroupedForm($id)
    {
        // Выбираем ID прилинкованных характеристик
        $product = $this->_products->get($id);
        $charIds = array_map(function (ShopCharacteristicAssignment $charAssignment) use ($product) {
            return $charAssignment->characteristic_id;
        }, $product->type->characteristicAssignments);

        // Собираем значения характеристик товара
        $values = array_map(function (ShopCharacteristicAssignment $charAssignment) use ($product) {
            return new ShopValueForm($charAssignment->characteristic, $product->getValue($charAssignment->characteristic_id));
        }, $product->type->characteristicAssignments);

        // Группируем характеристики
        $groups = [];
        $groupTemp = '';
        foreach ($values as $value) {
            /* @var $value ShopValueForm */
            $group = ShopCharacteristic::findOne($value->getId())->characteristicGroup->translation->name;
            if (in_array($value->getId(), $charIds)) {
                if ($group != $groupTemp) {
                    $groupTemp = $group;
                    $value->group = true;
                    $value->groupLabel = $group;
                }
                $groups[] = $value;
            }
        }
        return $groups;
    }

    public function prepareCharacteristicGroupedList($id)
    {
        $product = $this->_products->get($id);
        $charIds = array_map(function (ShopCharacteristicAssignment $charAssignment) use ($product) {
            return $charAssignment->characteristic_id;
        }, $product->type->characteristicAssignments);

        // Идентификаторы характеристик, принадлежащих типу товара
        $typeChars = ArrayHelper::getColumn($product->type->characteristicAssignments, 'characteristic_id');

        // Группируем характеристики
        $groups = [];
        $groupTemp = '';
        foreach ($product->values as $value) {
            if (!in_array($value->characteristic_id, $typeChars)) {
                continue;
            }

            $group = $value->characteristic->characteristicGroup->translation->name;
            if ($group != $groupTemp) {
                $groups[] = [
                    'group' => true,
                    'label' => $group,
                    'rowOptions' => ['class'=>'info'],
                ];
                $groupTemp = $group;
            }
            if (in_array($value->characteristic_id, $charIds)) {
                $groups[] = [
                    'label' => $value->characteristic->translation->name,
                    'value' => $value->value,
                ];
            }
        }
        return $groups;
    }
}
