<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopTag;
use common\modules\shop\forms\ShopTagForm;
use common\modules\shop\repositories\ShopTagRepository;

class ShopTagManageService
{
    private $_tags;

    public function __construct(ShopTagRepository $tags)
    {
        $this->_tags = $tags;
    }

    public function create(ShopTagForm $form): ShopTag
    {
        $tag = ShopTag::create(
            $form->name,
            $form->slug
        );
        $this->_tags->save($tag);
        return $tag;
    }

    public function edit($id, ShopTagForm $form): void
    {
        $tag = $this->_tags->get($id);
        $tag->edit(
            $form->name,
            $form->slug
        );
        $this->_tags->save($tag);
    }

    public function remove($id): void
    {
        $tag = $this->_tags->get($id);
        $this->_tags->remove($tag);
    }
}
