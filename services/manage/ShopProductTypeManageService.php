<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopProductType;
use common\modules\shop\forms\ShopCharacteristicAssignmentForm;
use common\modules\shop\forms\ShopProductTypeForm;
use common\modules\shop\repositories\ShopCharacteristicRepository;
use common\modules\shop\repositories\ShopProductRepository;
use common\modules\shop\repositories\ShopProductTypeRepository;
use Yii;

class ShopProductTypeManageService
{
    private $_types;
    private $_products;
    private $_characteristics;

    public function __construct(ShopProductTypeRepository $types, ShopProductRepository $products, ShopCharacteristicRepository $characteristics)
    {
        $this->_types = $types;
        $this->_products = $products;
        $this->_characteristics = $characteristics;
    }

    public function create(ShopProductTypeForm $form): ShopProductType
    {
        $type = ShopProductType::create(
            $form,
            $form->slug
        );
        $this->_types->save($type);
        return $type;
    }

    public function edit($id, ShopProductTypeForm $form): void
    {
        $type = $this->_types->get($id);
        $type->edit(
            $form,
            $form->slug
        );
        $this->_types->save($type);
    }

    public function remove($id): void
    {
        $type = $this->_types->get($id);
        if ($this->_products->existsByType($type->id)) {
            throw new \DomainException(Yii::t('shop', 'Unable to remove type with products.'));
        }
        $this->_types->remove($type);
    }

    public function saveCharacteristics($id, ShopCharacteristicAssignmentForm $form): void
    {
        $type = $this->_types->get($id);

        // clear old assignments before saving new
        $type->characteristicAssignments = [];
        $this->_types->save($type);

        foreach ($form->existing as $characteristicId) {
            $characteristic = $this->_characteristics->get($characteristicId);
            $type->assignCharacteristics($characteristic->id);
        }
        $this->_types->save($type);
    }
}
