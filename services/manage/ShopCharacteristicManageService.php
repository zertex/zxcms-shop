<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\forms\ShopCharacteristicForm;
use common\modules\shop\repositories\ShopCharacteristicRepository;

class ShopCharacteristicManageService
{
    private $_characteristics;

    public function __construct(ShopCharacteristicRepository $characteristics)
    {
        $this->_characteristics = $characteristics;
    }

    public function create(ShopCharacteristicForm $form): ShopCharacteristic
    {
        $characteristic = ShopCharacteristic::create(
            $form,
            $form->type,
            $form->required,
            $form->sort,
            $form->group_id,
            $form->widget
        );
        $this->_characteristics->save($characteristic);
        return $characteristic;
    }

    public function edit($id, ShopCharacteristicForm $form): void
    {
        $characteristic = $this->_characteristics->get($id);
        $characteristic->edit(
            $form,
            $form->type,
            $form->required,
            $form->sort,
            $form->group_id,
            $form->widget
        );
        $this->_characteristics->save($characteristic);
    }

    public function remove($id): void
    {
        $characteristic = $this->_characteristics->get($id);
        $this->_characteristics->remove($characteristic);
    }
}
