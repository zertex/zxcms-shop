<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopDeliveryMethod;
use common\modules\shop\forms\ShopDeliveryMethodForm;
use common\modules\shop\repositories\ShopDeliveryMethodRepository;

class ShopDeliveryMethodManageService
{
    private $_methods;

    public function __construct(ShopDeliveryMethodRepository $methods)
    {
        $this->_methods = $methods;
    }

    public function create(ShopDeliveryMethodForm $form): ShopDeliveryMethod
    {
        $method = ShopDeliveryMethod::create(
            $form,
            $form->cost,
            $form->minWeight,
            $form->maxWeight,
            $form->sort
        );
        $this->_methods->save($method);
        return $method;
    }

    public function edit($id, ShopDeliveryMethodForm $form): void
    {
        $method = $this->_methods->get($id);
        $method->edit(
            $form,
            $form->cost,
            $form->minWeight,
            $form->maxWeight,
            $form->sort
        );
        $this->_methods->save($method);
    }

    public function remove($id): void
    {
        $method = $this->_methods->get($id);
        $this->_methods->remove($method);
    }
}
