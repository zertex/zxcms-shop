<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\events\ShopReviewStatusChangeEvent;
use common\modules\shop\forms\ShopReviewForm;
use common\modules\shop\repositories\ShopProductRepository;
use common\modules\shop\repositories\ShopReviewRepository;

class ShopReviewManageService
{
    private $_reviews;
    private $_products;

    public function __construct(ShopReviewRepository $reviews, ShopProductRepository $products)
    {
        $this->_reviews = $reviews;
        $this->_products = $products;
    }

    public function edit($id, ShopReviewForm $form): void
    {
        $review = $this->_reviews->get($id);
        $review->edit(
            $form->vote,
            $form->text
        );
        $this->_reviews->save($review);
    }

    public function remove($id): void
    {
        $review = $this->_reviews->get($id);
        $this->_reviews->remove($review);
    }

    public function statusChange($id): void
    {
        $review = $this->_reviews->get($id);
        if ($review->isActive()) {
            $review->draft();
        } else {
            $review->activate();
        }
        $this->_reviews->save($review);
        $review->recordEvent(new ShopReviewStatusChangeEvent($review->product));
    }
}
