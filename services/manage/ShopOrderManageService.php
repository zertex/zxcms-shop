<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\order\ShopCustomerData;
use common\modules\shop\entities\order\ShopDeliveryData;
use common\modules\shop\entities\order\ShopOrder;
use common\modules\shop\entities\order\ShopOrderItem;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\events\ShopOrderChangedEvent;
use common\modules\shop\events\ShopOrderDeleteEvent;
use common\modules\shop\forms\order\manage\ShopOrderEditForm;
use common\modules\shop\repositories\ShopDeliveryMethodRepository;
use common\modules\shop\repositories\ShopOrderRepository;
use common\modules\shop\cart\cost\calculator\DynamicCost;
use common\modules\shop\cart\cost\calculator\SimpleCost;
use core\services\TransactionManager;
use common\modules\shop\repositories\ShopProductRepository;
use Yii;

class ShopOrderManageService
{
    private $_orders;
    private $_deliveryMethods;
    private $_transaction;
    private $_products;

    public function __construct(
        ShopOrderRepository $orders,
        TransactionManager $transaction,
        ShopDeliveryMethodRepository $deliveryMethods,
        ShopProductRepository $products
    ) {
        $this->_orders = $orders;
        $this->_deliveryMethods = $deliveryMethods;
        $this->_transaction = $transaction;
        $this->_products = $products;
    }

    public function edit($id, ShopOrderEditForm $form): void
    {
        $order = $this->_orders->get($id);

        $order->edit(
            new ShopCustomerData(
                $form->customer->phone,
                $form->customer->name
            ),
            $form->note
        );

        $order->setDeliveryInfo(
            $this->_deliveryMethods->get($form->delivery->method),
            new ShopDeliveryData(
                $form->delivery->index,
                $form->delivery->address
            )
        );
        $this->_orders->save($order);
        $order->recordEvent(new ShopOrderChangedEvent($order));
    }

    public function remove($id): void
    {
        $order = $this->_orders->get($id);
        $this->_transaction->wrap(function () use ($order) {
            // restore quantity
            $this->restoreOrderQuantity($order);
            // remove order
            $this->_orders->remove($order);
            $order->recordEvent(new ShopOrderDeleteEvent($order));
        });
    }

    public function hideItem(int $order_id, int $id): void
    {
        if ($orderItem = ShopOrderItem::findOne($id)) {
            $this->restoreProductQuantity($orderItem);
            $orderItem->hide();
            $this->recalculate($order_id);
        }
    }

    public function activateItem(int $order_id, int $id): void
    {
        if ($orderItem = ShopOrderItem::findOne($id)) {
            $this->cutProductQuantity($orderItem);
            $orderItem->activate();
            $this->recalculate($order_id);
        }
    }

    public function cutProductQuantity(ShopOrderItem $item): void
    {
        if (Yii::$app->params['settings']['shop']['quantity_use'] == 0) {
            return;
        }

        if ($product = ShopProduct::findOne($item->product_id)) {
            // change for modification
            if ($product->modifications) {
                $modifications = $product->modifications;
                foreach ($modifications as $i => $modification) {
                    if ($modification->isIdEqualTo($item->modification_id)) {
                        if ($modification->quantity < $item->quantity) {
                            throw new \DomainException(\Yii::t('shop', 'Quantity is too big.'));
                        }
                        $modification->quantity = $modification->quantity - $item->quantity;
                        $product->updateModifications($modifications);
                    }
                }
            } else { // change if not modifications
                if ($product->quantity < $item->quantity) {
                    throw new \DomainException(\Yii::t('shop', 'Quantity is too big.'));
                }
                $product->changeQuantity($product->quantity - $item->quantity);
            }
        }
        $this->_products->save($product);
    }

    public function restoreProductQuantity(ShopOrderItem $item): void
    {
        if (Yii::$app->params['settings']['shop']['quantity_use'] == 0) {
            return;
        }

        if ($product = ShopProduct::findOne($item->product_id)) {
            // change for modification
            if ($product->modifications) {
                $modifications = $product->modifications;
                foreach ($modifications as $i => $modification) {
                    if ($modification->isIdEqualTo($item->modification_id)) {
                        $modification->quantity = $modification->quantity + $item->quantity;
                        $product->updateModifications($modifications);
                    }
                }
            } else { // change if not modifications
                $product->changeQuantity($product->quantity + $item->quantity);
            }
        }
        $this->_products->save($product);
    }

    public function restoreOrderQuantity(ShopOrder $order): void
    {
        if (Yii::$app->params['settings']['shop']['quantity_use'] == 0) {
            return;
        }

        foreach ($order->items as $item) {
            /* @var $item ShopOrderItem */
            $this->restoreProductQuantity($item);
        }
    }

    public function recalculate(int $id): void
    {
        $calculator = new DynamicCost(new SimpleCost());
        $order = $this->_orders->get($id);
        $order->cost = $calculator->getCost($order->activeItems)->getTotal();
        $this->_orders->save($order);
        $order->recordEvent(new ShopOrderChangedEvent($order));
    }
}
