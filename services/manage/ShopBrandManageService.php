<?php

namespace common\modules\shop\services\manage;

use common\modules\shop\entities\ShopBrand;
use common\modules\shop\forms\ShopBrandForm;
use common\modules\shop\repositories\ShopBrandRepository;
use common\modules\shop\repositories\ShopProductRepository;

class ShopBrandManageService
{
    private $_brands;
    private $_products;

    public function __construct(ShopBrandRepository $brands, ShopProductRepository $products)
    {
        $this->_brands = $brands;
        $this->_products = $products;
    }

    public function create(ShopBrandForm $form): ShopBrand
    {
        $brand = ShopBrand::create(
            $form,
            $form->slug
        );
        $this->_brands->save($brand);
        return $brand;
    }

    public function edit($id, ShopBrandForm $form): void
    {
        $brand = $this->_brands->get($id);
        $brand->edit(
            $form,
            $form->slug
        );
        $this->_brands->save($brand);
    }

    public function remove($id): void
    {
        $brand = $this->_brands->get($id);
        if ($this->_products->existsByBrand($brand->id)) {
            throw new \DomainException('Unable to remove brand with products.');
        }
        $this->_brands->remove($brand);
    }
}
