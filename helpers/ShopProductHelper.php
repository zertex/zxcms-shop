<?php

namespace common\modules\shop\helpers;

use common\modules\shop\entities\product\ShopProduct;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class ShopProductHelper
{
    public static function statusList(): array
    {
        return [
            ShopProduct::STATUS_DRAFT => Yii::t('shop', 'Draft'),
            ShopProduct::STATUS_ACTIVE => Yii::t('shop', 'Active'),
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case ShopProduct::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case ShopProduct::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

    public static function labelsList(): array
    {
        return [
            ShopProduct::LABEL_NEW => Yii::t('shop_public', 'New'),
            ShopProduct::LABEL_SALE => Yii::t('shop_public', 'Sale'),
            ShopProduct::LABEL_DISCOUNT => Yii::t('shop_public', 'Discount'),
            ShopProduct::LABEL_BESTSELLER => Yii::t('shop_public', 'Bestseller'),
            ShopProduct::LABEL_BEST_PRICE => Yii::t('shop_public', 'Best price'),
            ShopProduct::LABEL_RECOMMENDED => Yii::t('shop_public', 'Our choice'),
            ShopProduct::LABEL_POPULAR => Yii::t('shop_public', 'Popular'),
            ShopProduct::LABEL_PRODUCT_DAY => Yii::t('shop_public', 'Product Day'),
            ShopProduct::LABEL_PROMO => Yii::t('shop_public', 'Promo'),
            ShopProduct::LABEL_HOT_DEAL => Yii::t('shop_public', 'Hot deal'),
        ];
    }

    public static function labelName($label): string
    {
        return ArrayHelper::getValue(self::labelsList(), $label);
    }
}
