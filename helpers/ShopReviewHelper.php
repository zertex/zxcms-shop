<?php

namespace common\modules\shop\helpers;

use common\modules\shop\entities\product\ShopReview;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class ShopReviewHelper
{
    public static function statusList(): array
    {
        return [
            ShopReview::STATUS_DRAFT => Yii::t('shop', 'Draft'),
            ShopReview::STATUS_ACTIVE => Yii::t('shop', 'Active'),
        ];
    }

    public static function voteList(): array
    {
        return [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case ShopReview::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case ShopReview::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}
