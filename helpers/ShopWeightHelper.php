<?php

namespace common\modules\shop\helpers;

use Yii;

class ShopWeightHelper
{
    public static function format($weight): string
    {
        return $weight / 1000 . ' ' . Yii::t('shop_public', 'kg');
    }
}
