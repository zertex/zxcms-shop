<?php

namespace common\modules\shop\helpers;

use common\modules\shop\entities\order\ShopStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class ShopOrderHelper
{
    public static function statusList(): array
    {
        return [
            ShopStatus::NEW => Yii::t('shop_public', 'New'),
            ShopStatus::PAID => Yii::t('shop_public', 'Paid'),
            ShopStatus::SENT => Yii::t('shop_public', 'Sent'),
            ShopStatus::COMPLETED => Yii::t('shop_public', 'Completed'),
            ShopStatus::CANCELLED => Yii::t('shop_public', 'Cancelled'),
            ShopStatus::AWAITING_INVOICE => Yii::t('shop_public', 'Awaiting Invoice'),
            ShopStatus::CANCELLED_BY_CUSTOMER => Yii::t('shop_public', 'Cancelled by customer'),
            ShopStatus::PROCESSING => Yii::t('shop_public', 'Processing'),
            ShopStatus::AWAITING_CONFIRM => Yii::t('shop_public', 'Awaiting Confirmation'),
            ShopStatus::AWAITING_PAYMENT => Yii::t('shop_public', 'Awaiting Payment'),
            ShopStatus::AWAITING_ARRIVE => Yii::t('shop_public', 'Awaiting Arrive'),
            ShopStatus::AWAITING_SURCHARGE => Yii::t('shop_public', 'Awaiting Surcharge'),
            ShopStatus::COMPLETION => Yii::t('shop_public', 'Completion'),
            ShopStatus::PROCESSED => Yii::t('shop_public', 'Processed'),
            ShopStatus::CHANGED => Yii::t('shop_public', 'Changed'),
            ShopStatus::REFUNDED => Yii::t('shop_public', 'Refunded'),
            ShopStatus::REFUND => Yii::t('shop_public', 'Refund'),
            ShopStatus::PARTIALLY_REFUND => Yii::t('shop_public', 'Partially Refund'),
            ShopStatus::IN_DELIVERY_SERVICE => Yii::t('shop_public', 'In Delivery Service'),
            ShopStatus::DELIVERY_PROCESS => Yii::t('shop_public', 'Delivery Process'),
            ShopStatus::PICKUP_AVAILABLE => Yii::t('shop_public', 'Pickup Available'),
            ShopStatus::PARTIALLY_REFUNDED => Yii::t('shop_public', 'Partially Refunded'),
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case ShopStatus::NEW:
                $class = 'label label-success';
                break;

            case ShopStatus::COMPLETED:
                $class = 'label label-primary';
                break;
            case ShopStatus::PICKUP_AVAILABLE:
                $class = 'label label-primary';
                break;
            case ShopStatus::SENT:
                $class = 'label label-primary';
                break;

            case ShopStatus::CANCELLED:
                $class = 'label label-danger';
                break;
            case ShopStatus::PARTIALLY_REFUNDED:
                $class = 'label label-danger';
                break;
            case ShopStatus::REFUNDED:
                $class = 'label label-danger';
                break;
            case ShopStatus::CANCELLED_BY_CUSTOMER:
                $class = 'label label-danger';
                break;

            case ShopStatus::REFUND:
                $class = 'label label-warning';
                break;
            case ShopStatus::PARTIALLY_REFUND:
                $class = 'label label-warning';
                break;
            case ShopStatus::DELIVERY_PROCESS:
                $class = 'label label-warning';
                break;
            case ShopStatus::IN_DELIVERY_SERVICE:
                $class = 'label label-warning';
                break;
            case ShopStatus::AWAITING_ARRIVE:
                $class = 'label label-warning';
                break;
            case ShopStatus::AWAITING_PAYMENT:
                $class = 'label label-warning';
                break;
            case ShopStatus::AWAITING_SURCHARGE:
                $class = 'label label-warning';
                break;

            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}
