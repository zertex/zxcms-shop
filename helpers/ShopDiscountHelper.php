<?php
/**
 * Created by Error202
 * Date: 10.10.2017
 */

namespace common\modules\shop\helpers;

use common\modules\shop\entities\ShopBrand;
use common\modules\shop\entities\ShopCategory;
use common\modules\shop\entities\ShopDiscount;
use common\modules\shop\entities\order\ShopOrder;
use common\modules\shop\entities\order\ShopOrderItem;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\entities\ShopProductType;
use common\modules\shop\repositories\ShopDiscountRepository;
use core\entities\user\User;
use Yii;
use yii\base\BaseObject;
use yii\caching\Cache;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\modules\shop\services\manage\ShopDiscountManageService;

class ShopDiscountHelper extends BaseObject
{
    public static function discountedCost(int $cost):int
    {
        $newCost = $cost;
        /** @var ShopDiscount[] $discounts */
        $cache = Yii::$container->get(Cache::class);
        $discounts = $cache->getOrSet('discounts_active_date_count', function () {
            return ShopDiscount::find()->active()->dateActive()->countActive()->orderBy('sort')->all();
        }, null, new TagDependency(['tags' => ['discounts_active_date_count_tag']]));

        foreach ($discounts as $discount) {
            if (!in_array($discount->type, [ShopDiscount::TYPE_ORDER])) {
                continue;
            }
            $valid = ($discount->recipient == ShopDiscount::FOR_USER && !Yii::$app->user->isGuest && $discount->value_recipient == Yii::$app->user->id) ||
                ($discount->recipient == ShopDiscount::FOR_USERS_GROUP && !Yii::$app->user->isGuest && Yii::$app->user->can($discount->value_recipient)) ||
                ($discount->recipient == ShopDiscount::FOR_ALL_USERS);

            $discountItems = [
                $discount->type == ShopDiscount::TYPE_ORDER && $valid,
            ];
            foreach ($discountItems as $state) {
                $newCost -= $state ? ($discount->method == ShopDiscount::METHOD_PERCENT ? ($cost * $discount->percent / 100) : ($discount->percent >= $newCost ? $newCost-1 : $discount->percent)) : 0;
            }
        }
        return round($newCost);
    }

    public static function discountedPrice(int $price, ShopProduct $product = null): int
    {
        $newPrice = $price;
        /** @var ShopDiscount[] $discounts */
        $cache = Yii::$container->get(Cache::class);
        $discounts = $cache->getOrSet('discounts_active_date_count', function () {
            return ShopDiscount::find()->active()->dateActive()->countActive()->orderBy('sort')->all();
        }, null, new TagDependency(['tags' => ['discounts_active_date_count_tag']]));
        foreach ($discounts as $discount) {
            $valid = ($discount->recipient == ShopDiscount::FOR_USER && !Yii::$app->user->isGuest && $discount->value_recipient == Yii::$app->user->id) ||
                ($discount->recipient == ShopDiscount::FOR_USERS_GROUP && !Yii::$app->user->isGuest && Yii::$app->user->can($discount->value_recipient)) ||
                ($discount->recipient == ShopDiscount::FOR_ALL_USERS);

            // categories list for category discount
            $category_ids = [];
            if ($discount->type == ShopDiscount::TYPE_CATEGORY && $valid && $product) {
                $category = ShopCategory::findOne($discount->value);
                $category_ids = $cache->getOrSet('category_' . $category->id . '_descendants', function () use ($category) {
                    return ArrayHelper::merge([$category->id], $category->getDescendants()->select('id')->column());
                }, null, new TagDependency(['tags' => ['categories_descendants']]));
            }

            // exclude: order, promo_code
            $discountItems = [
                //$discount->type == Discount::TYPE_CATEGORY && $valid && $product && $product->category_id == $discount->value,
                $discount->type == ShopDiscount::TYPE_CATEGORY && $valid && $product && in_array($product->category_id, $category_ids),
                $discount->type == ShopDiscount::TYPE_PRODUCT && $valid && $product && $product->id == $discount->value,
                $discount->type == ShopDiscount::TYPE_PRODUCT_TYPE && $valid && $product && $product->type_id == $discount->value,
                $discount->type == ShopDiscount::TYPE_BRAND && $valid && $product && $product->brand_id == $discount->value,
                $discount->type == ShopDiscount::TYPE_ALL_PRODUCTS && $valid,
                //$discount->type == Discount::TYPE_ORDER && $valid,
            ];

            foreach ($discountItems as $state) {
                $newPrice -= $state ? ($discount->method == ShopDiscount::METHOD_PERCENT ? ($price * $discount->percent / 100) : ($discount->percent >= $newPrice ? $newPrice-1 : $discount->percent)) : 0;
            }
        }
        return round($newPrice);
    }

    public static function getDiscountRecipientValue($recipient, $value_recipient)
    {
        if ($recipient == ShopDiscount::FOR_USER) {
            if ($user = User::findOne($value_recipient)) {
                return Html::a($user->username, ['/users/manage/view', 'id'=>$value_recipient]);
            }
            return '- - -';
        } elseif ($recipient == ShopDiscount::FOR_USERS_GROUP) {
            return $value_recipient;
        } else {
            return '';
        }
    }

    public static function getDiscountValue($type, $value): string
    {
        if ($type == ShopDiscount::TYPE_CATEGORY) {
            if ($category = ShopCategory::findOne($value)) {
                return Html::a($category->translation->name, ['/shop/manage/category/view', 'id' => $value]);
            }
            return '- - -';
        } elseif ($type == ShopDiscount::TYPE_PRODUCT) {
            if ($product = ShopProduct::findOne($value)) {
                return Html::a($product->translation->name, ['/shop/manage/product/view', 'id' => $value]);
            }
            return '- - -';
        } elseif ($type == ShopDiscount::TYPE_PRODUCT_TYPE) {
            if ($productType = ShopProductType::findOne($value)) {
                return Html::a($productType->translation->name, ['/shop/manage/product-type/view', 'id' => $value]);
            }
            return '- - -';
        } elseif ($type == ShopDiscount::TYPE_BRAND) {
            if ($brand = ShopBrand::findOne($value)) {
                return Html::a($brand->translation->name, ['/shop/manage/brand/view', 'id' => $value]);
            }
            return '- - -';
        } else {
            return '';
        }
    }

    public static function updateDiscountsCounters(ShopOrder $order): void
    {
        $cache = Yii::$container->get(Cache::class);
        $discounts = $cache->getOrSet('discounts_active_date_count', function () {
            return ShopDiscount::find()->active()->dateActive()->countActive()->orderBy('sort')->all();
        }, null, new TagDependency(['tags' => ['discounts_active_date_count_tag']]));

        foreach ($discounts as $discount) {
            /* @var $discount ShopDiscount */
            if ($discount->count <= 0) {
                continue;
            }

            $needCountDown = false;

            foreach ($order->items as $item) {
                /* @var $item ShopOrderItem */
                if (($discount->recipient == ShopDiscount::FOR_USER && $discount->value_recipient == Yii::$app->user->id) ||
                    ($discount->recipient == ShopDiscount::FOR_USERS_GROUP && Yii::$app->user->can($discount->value_recipient)) ||
                    ($discount->recipient == ShopDiscount::FOR_ALL_USERS)) {
                    if ($discount->type == ShopDiscount::TYPE_ALL_PRODUCTS) {
                        $needCountDown = true;
                    } elseif ($discount->type == ShopDiscount::TYPE_CATEGORY) {
                        $category = ShopCategory::findOne($discount->value);
                        $category_ids = $cache->getOrSet('category_' . $category->id . '_descendants', function () use ($category) {
                            return ArrayHelper::merge([$category->id], $category->getDescendants()->select('id')->column());
                        }, null, new TagDependency(['tags' => ['categories_descendants']]));
                        if (in_array($item->product->category_id, $category_ids)) {
                            $needCountDown = true;
                        }
                    } elseif ($discount->type == ShopDiscount::TYPE_PRODUCT) {
                        if ($discount->value == $item->product_id) {
                            $needCountDown = true;
                        }
                    } elseif ($discount->type == ShopDiscount::TYPE_PRODUCT_TYPE) {
                        if ($discount->value == $item->product->type_id) {
                            $needCountDown = true;
                        }
                    } elseif ($discount->type == ShopDiscount::TYPE_BRAND) {
                        if ($discount->value == $item->product->brand_id) {
                            $needCountDown = true;
                        }
                    }
                }
            }
            if ($needCountDown) {
                // count down
                $service = new ShopDiscountManageService(new ShopDiscountRepository());
                $service->downCounter($discount->id);
            }
        }
    }

    public static function promoCheck($code): ?array
    {
        if ($discount = ShopDiscount::find()->active()->dateActive()->countActive()->andWhere(['code' => $code])->one()) {
            if (($discount->recipient == ShopDiscount::FOR_ALL_USERS) ||
                ($discount->recipient == ShopDiscount::FOR_USERS_GROUP && Yii::$app->user->can($discount->value_recipient)) ||
                ($discount->recipient == ShopDiscount::FOR_USER && !Yii::$app->user->isGuest && Yii::$app->user->id == $discount->value_recipient)) {
                return [
                    'id' => $discount->id,
                    'result' => 'active',
                    'amount' => $discount->percent,
                    'count' => $discount->count,
                    'method' => $discount->method,
                    'tag' => $discount->method == ShopDiscount::METHOD_PERCENT ? '%' : PriceHelper::currencyTag(),
                ];
            }
        }
        return null;
    }

    public static function updatePromoDiscountCounter($code): void
    {
        if ($promoData = self::promoCheck($code)) {
            $discount = ShopDiscount::findOne($promoData['id']);
            $service = new ShopDiscountManageService(new ShopDiscountRepository());
            $service->downCounter($discount->id);
        }
    }
}
