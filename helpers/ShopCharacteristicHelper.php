<?php

namespace common\modules\shop\helpers;

use common\modules\shop\entities\ShopCharacteristic;
use yii\helpers\ArrayHelper;
use Yii;

class ShopCharacteristicHelper
{
    public static function typeList(): array
    {
        return [
            ShopCharacteristic::TYPE_STRING => Yii::t('shop', 'String'),
            ShopCharacteristic::TYPE_INTEGER => Yii::t('shop', 'Integer number'),
            ShopCharacteristic::TYPE_FLOAT => Yii::t('shop', 'Float number'),
        ];
    }

    public static function typeName($type): string
    {
        return ArrayHelper::getValue(self::typeList(), $type);
    }

    public static function widgetList(): array
    {
        return [
            ShopCharacteristic::WIDGET_NONE => Yii::t('shop', 'No widget'),
            ShopCharacteristic::WIDGET_PERIOD => Yii::t('shop', 'Period'),
            ShopCharacteristic::WIDGET_CHECKBOX => Yii::t('shop', 'Checkbox'),
        ];
    }

    public static function widgetName($widget): string
    {
        return ArrayHelper::getValue(self::widgetList(), $widget);
    }
}
