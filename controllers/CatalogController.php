<?php

namespace common\modules\shop\controllers;

use common\modules\shop\forms\ShopAddToCartForm;
use common\modules\shop\forms\ShopReviewForm;
use common\modules\shop\forms\search\ShopSearchForm;
use common\modules\shop\repositories\read\ShopBrandReadRepository;
use common\modules\shop\repositories\read\ShopCategoryReadRepository;
use common\modules\shop\repositories\read\ShopProductReadRepository;
use common\modules\shop\repositories\read\ShopProductTypeReadRepository;
use common\modules\shop\repositories\read\ShopTagReadRepository;
use frontend\components\FrontendController;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use Yii;

class CatalogController extends FrontendController
{
    public $layout = 'catalog';

    private $_products;
    private $_categories;
    private $_brands;
    private $_types;
    private $_tags;

    public function __construct(
        $id,
        $module,
        ShopProductReadRepository $products,
        ShopCategoryReadRepository $categories,
        ShopBrandReadRepository $brands,
        ShopTagReadRepository $tags,
        ShopProductTypeReadRepository $types,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->_products = $products;
        $this->_categories = $categories;
        $this->_brands = $brands;
        $this->_types = $types;
        $this->_tags = $tags;
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        //$dataProvider = $this->products->getAll();
        $category = $this->_categories->getRoot();

        $form = new ShopSearchForm();
        $form->load(\Yii::$app->request->queryParams);
        $form->validate();

        $dataProvider = $this->_products->search($form);

        return $this->render('index', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCategory($id)
    {
        if (!$category = $this->_categories->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $form = new ShopSearchForm();
        $form->load(\Yii::$app->request->queryParams);
        $form->validate();

        //    $dataProvider = $this->products->getAllByCategory($category);
        // $dataProvider = $this->products->searchInCategory($form, $category);
        $dataProvider = $this->_products->search($form, $category);

        return $this->render('category', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionBrand($id)
    {
        if (!$brand = $this->_brands->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $form = new ShopSearchForm();
        $form->load(\Yii::$app->request->queryParams);
        $form->brand = [$id];
        $form->validate();

        $dataProvider = $this->_products->search($form);
        //$dataProvider = $this->products->getAllByBrand($brand);

        return $this->render('brand', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionProductType($id)
    {
        if (!$type = $this->_types->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $form = new ShopSearchForm();
        $form->load(\Yii::$app->request->queryParams);
        $form->type = [$id];
        $form->validate();

        //$dataProvider = $this->products->getAllByProductType($type);
        $dataProvider = $this->_products->search($form);

        return $this->render('type', [
            'type' => $type,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionTag($id)
    {
        if (!$tag = $this->_tags->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $dataProvider = $this->_products->getAllByTag($tag);

        return $this->render('tag', [
            'tag' => $tag,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSearch()
    {
        $form = new ShopSearchForm();
        $form->load(\Yii::$app->request->queryParams);
        $form->validate();

        $dataProvider = $this->_products->search($form);

        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'searchForm' => $form,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionProduct($id)
    {
        if (!$product = $this->_products->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->layout = '@app/views/layouts/blank';

        $cartForm = new ShopAddToCartForm($product);

        // Отзывы
        $reviewForm = new ShopReviewForm();

        if ($reviewForm->load(Yii::$app->request->post()) && $reviewForm->validate()) {
            $product->addReview(Yii::$app->user->id, $reviewForm->vote, $reviewForm->text);
        } else {
            if ($review = $product->getUserReview(Yii::$app->user->id)) {
                $reviewForm->text = $review->text;
                $reviewForm->vote = $review->vote;
            }
        }

        $characteristics = $this->_products->prepareCharacteristicGroupedList($id);

        $reviewProvider = new ArrayDataProvider([
            'allModels' => $product->activeReviews,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('product', [
            'product' => $product,
            'cartForm' => $cartForm,
            'reviewForm' => $reviewForm,
            'characteristics' => $characteristics,
            'reviewProvider' => $reviewProvider,
        ]);
    }
}
