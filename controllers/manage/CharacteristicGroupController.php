<?php

namespace common\modules\shop\controllers\manage;

use common\modules\shop\forms\ShopCharacteristicGroupForm;
use common\modules\shop\services\manage\ShopCharacteristicGroupManageService;
use Yii;
use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\forms\search\ShopCharacteristicGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class CharacteristicGroupController extends Controller
{
    private $_service;

    public function __construct($id, $module, ShopCharacteristicGroupManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create','view','index', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['ShopManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopCharacteristicGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'characteristicGroup' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ShopCharacteristicGroupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $characteristicGroup = $this->_service->create($form);
                return $this->redirect(['view', 'id' => $characteristicGroup->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $characteristicGroup = $this->findModel($id);

        $form = new ShopCharacteristicGroupForm($characteristicGroup);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->_service->edit($characteristicGroup->id, $form);
                return $this->redirect(['view', 'id' => $characteristicGroup->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'characteristicGroup' => $characteristicGroup,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->_service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return ShopCharacteristicGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): ShopCharacteristicGroup
    {
        if (($model = ShopCharacteristicGroup::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
