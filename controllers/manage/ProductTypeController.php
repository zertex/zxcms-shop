<?php

namespace common\modules\shop\controllers\manage;

use common\modules\shop\forms\search\ShopProductTypeSearch;
use common\modules\shop\forms\ShopCharacteristicAssignmentForm;
use common\modules\shop\forms\ShopProductTypeForm;
use common\modules\shop\services\manage\ShopProductTypeManageService;
use Yii;
use common\modules\shop\entities\ShopProductType;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ProductTypeController extends Controller
{
    private $_service;

    public function __construct($id, $module, ShopProductTypeManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['ShopManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopProductTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $productType = $this->findModel($id);
        $form = new ShopCharacteristicAssignmentForm();
        $data = $form->characteristicGroupedList();
        $form->existing = ArrayHelper::getColumn($productType->characteristicAssignments, 'characteristic_id');

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->_service->saveCharacteristics($id, $form);
            Yii::$app->session->setFlash('success', Yii::t('shop', 'Characteristics is saved.'));
        }

        return $this->render('view', [
            'type' => $this->findModel($id),
            'model' => $form,
            'data' => $data,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ShopProductTypeForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $brand = $this->_service->create($form);
                return $this->redirect(['view', 'id' => $brand->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $type = $this->findModel($id);

        $form = new ShopProductTypeForm($type);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->_service->edit($type->id, $form);
                return $this->redirect(['view', 'id' => $type->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'type' => $type,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->_service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return ShopProductType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): ShopProductType
    {
        if (($model = ShopProductType::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
