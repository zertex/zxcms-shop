<?php

namespace common\modules\shop\controllers\manage;

use common\modules\shop\entities\ShopBrand;
use common\modules\shop\entities\ShopCategory;
use common\modules\shop\entities\ShopDiscount;
use app\modules\shop\entities\product\Product;
use app\modules\shop\entities\ProductType;
use common\modules\shop\forms\search\ShopDiscountSearch;
use common\modules\shop\forms\ShopDiscountForm;
use common\modules\shop\services\manage\ShopDiscountManageService;
use core\entities\user\User;
use Yii;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

class DiscountController extends Controller
{
    private $_service;

    public function __construct($id, $module, ShopDiscountManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['ShopManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionValidateForm($id = null)
    {
        if ($id) {
            $model = new ShopDiscountForm($this->findModel($id));
        } else {
            $model = new ShopDiscountForm();
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ShopDiscountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'discount' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $form = new ShopDiscountForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $brand = $this->_service->create($form);

                return $this->redirect(['view', 'id' => $brand->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $form->for_to_date = true;
        $form->for_count   = true;

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $discount = $this->findModel($id);

        $form = new ShopDiscountForm($discount);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->_service->edit($discount->id, $form);

                return $this->redirect(['view', 'id' => $discount->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $form->date_from = $form->date_from ?: null;
        $form->date_to   = $form->date_to ?: null;

        return $this->render('update', [
            'model'    => $form,
            'discount' => $discount,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->_service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    public function actionActivate($id)
    {
        try {
            $this->_service->activate($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDraft($id)
    {
        try {
            $this->_service->draft($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionSearchUser($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $result = User::find()->select('id, username as text')->andFilterWhere([
                'or',
                ['=', 'id', $q],
                ['like', 'username', $q],
                ['like', 'email', $q],
            ])->limit(20)->asArray()->all();
            $out['results'] = $result;
        } elseif ($id > null) {
            $out['results'] = ['id' => $id, 'text' => User::findOne($id)->username];
        }
        return $out;
    }

    public function actionSearchUsersGroup($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('name as id, description as text')
                  ->from('{{%auth_items}}')
                  ->andFilterWhere([
                      'or',
                      ['like', 'name', $q],
                      ['like', 'description', $q],
                  ])
                  ->limit(20);

            $result         = $query->all();
            $out['results'] = $result;
        } elseif ($id > null) {
            $query = new Query();
            $query->select('name as id, description as text')
                  ->from('{{%auth_items}}')
                  ->where(['id' => $id])
                  ->limit(1);

            $result         = $query->one();
            $out['results'] = ['id' => $id, 'text' => $result->text];
        }
        return $out;
    }

    public function actionSearchCategory($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out                        = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = [];
            $categories = ShopCategory::find()
                         ->with('translation')
                         ->leftJoin('{{%shop_categories_lng}}', '`shop_categories_lng`.`shop_category_id` = `shop_categories`.`id`')
                         ->andWhere(['OR',
                             ['=', 'shop_categories_lng.shop_category_id', $q],
                             ['like', 'shop_categories_lng.title', $q],
                             ['like', 'shop_categories_lng.name', $q],
                         ])
                         ->limit(20)
                         ->all();

            foreach ($categories as $category) {
                $data[] = [
                    'id' => $category->id,
                    'text' => isset($category->translation) ? $category->translation->name : null,
                ];
            }
            $out['results'] = array_values($data);
        } elseif ($id > null) {
            $out['results'] = ['id' => $id, 'text' => ShopCategory::findOne($id)->translation->name];
        }

        return $out;
    }

    public function actionSearchProduct($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out                        = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $result         = Product::find()->select('id, name as text')->andFilterWhere([
                'or',
                ['=', 'id', $q],
                ['like', 'name', $q],
                ['like', 'code', $q],
            ])->limit(20)->asArray()->all();
            $out['results'] = $result;
        } elseif ($id > null) {
            $out['results'] = ['id' => $id, 'text' => Product::findOne($id)->name];
        }

        return $out;
    }

    public function actionSearchProductType($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out                        = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $result         = ProductType::find()->select('id, name as text')->andFilterWhere([
                'or',
                ['=', 'id', $q],
                ['like', 'name', $q],
            ])->limit(20)->asArray()->all();
            $out['results'] = $result;
        } elseif ($id > null) {
            $out['results'] = ['id' => $id, 'text' => ProductType::findOne($id)->name];
        }

        return $out;
    }

    public function actionSearchBrand($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = [];
            $brands = ShopBrand::find()
                  ->with('translation')
                  ->leftJoin('{{%shop_brands_lng}}', '`shop_brands_lng`.`shop_brand_id` = `shop_brands`.`id`')
                  ->andWhere(['OR',
                      ['=', 'shop_brands_lng.shop_brand_id', $q],
                      ['like', 'shop_brands_lng.name', $q],
                  ])
                  ->limit(20)
                  ->all();

            foreach ($brands as $brand) {
                $data[] = [
                    'id' => $brand->id,
                    'text' => isset($brand->translation) ? $brand->translation->name : null,
                ];
            }
            $out['results'] = array_values($data);
        } elseif ($id > null) {
            $out['results'] = ['id' => $id, 'text' => ShopBrand::findOne($id)->translation->name];
        }

        return $out;
    }

    /**
     * @param integer $id
     *
     * @return ShopDiscount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): ShopDiscount
    {
        if (($model = ShopDiscount::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
