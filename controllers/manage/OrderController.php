<?php

namespace common\modules\shop\controllers\manage;

use common\modules\shop\forms\order\manage\ShopOrderEditForm;
use common\modules\shop\services\manage\ShopOrderManageService;
use Yii;
use common\modules\shop\entities\order\ShopOrder;
use common\modules\shop\forms\search\ShopOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class OrderController extends Controller
{
    private $_service;

    public function __construct($id, $module, ShopOrderManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['ShopManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'export' => ['POST'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionExport()
    {
        //$query = ShopOrder::find()->orderBy(['id' => SORT_DESC]);

        //$objPHPExcel = new \PHPExcel();

        //$worksheet = $objPHPExcel->getActiveSheet();

        //foreach ($query->each() as $row => $order) {
            /** @var ShopOrder $order */

            //$worksheet->setCellValueByColumnAndRow(0, $row + 1, $order->id);
            //$worksheet->setCellValueByColumnAndRow(1, $row + 1, date('Y-m-d H:i:s', $order->created_at));
        //}

        //$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //$file = tempnam(sys_get_temp_dir(), 'export');
        //$objWriter->save($file);

        //return Yii::$app->response->sendFile($file, 'report.xlsx');
        return '';
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'order' => $this->findModel($id),
        ]);
    }

    public function actionHideItem($order_id, $id)
    {
        if (!Yii::$app->request->post()) {
            throw new \DomainException(Yii::t('shop', 'Only post method available.'));
        }

        if (!$order = $this->findModel($order_id)) {
            throw new \DomainException(Yii::t('shop', 'Order is not found.'));
        }

        try {
            $this->_service->hideItem($order_id, $id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        $this->redirect(['view', 'id' => $order->id]);
    }

    public function actionActivateItem($order_id, $id)
    {
        if (!Yii::$app->request->post()) {
            throw new \DomainException(Yii::t('shop', 'Only post method available.'));
        }

        if (!$order = $this->findModel($order_id)) {
            throw new \DomainException(Yii::t('shop', 'Order is not found.'));
        }

        try {
            $this->_service->activateItem($order_id, $id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        $this->redirect(['view', 'id' => $order->id]);
    }

    public function actionUpdate($id)
    {
        $order = $this->findModel($id);

        $form = new ShopOrderEditForm($order);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->_service->edit($order->id, $form);
                return $this->redirect(['view', 'id' => $order->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'order' => $order,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->_service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return ShopOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): ShopOrder
    {
        if (($model = ShopOrder::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
