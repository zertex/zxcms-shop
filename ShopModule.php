<?php

namespace common\modules\shop;

use common\modules\shop\widgets\MenuItemCreatorWidget;
use core\components\modules\ModuleInterface;
use yii\helpers\ArrayHelper;

/**
 * shop module definition class
 */
class ShopModule extends \yii\base\Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\shop\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        // add migration path
        $app->controllerMap['migrate']['migrationPath'][] = '@common/modules/shop/migrations';

        // add search rules
        //$app->params['search_rules'][] = "SELECT title, content, CONCAT('/blog/manage/post/view/', blog_post_id) AS url FROM {{blog_posts_lng}}";

        // add rules
        /*$app->getUrlManager()->addRules([
            'blog' => 'blog/post/index',
        ]);*/

        /*$app->getUrlManager()->addRules([
            ['class' => 'common\modules\blog\urls\BlogMainUrlRule'],
            ['class' => 'common\modules\blog\urls\BlogCategoryUrlRule'],
            ['class' => 'common\modules\blog\urls\BlogTagUrlRule'],
        ]);*/

        /*$app->getUrlManager()->addRules([
            'blog/manage/post/view/<id:\d+>' => 'blog/manage/post/view',
        ]);*/

        // add languages
        $app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
            'shop'        => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/shop/messages',
            ],
            'shop_public' => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/shop/messages',
            ],
        ]);

        // add menu items
        /*$app->params['adminMenu'][] = [
            'label' => \Yii::t('blog', 'Blog'), 'icon' => 'book', 'items' => [
                ['label' => \Yii::t('blog', 'Categories'), 'icon' => 'caret-right', 'url' => ['/blog/manage/category/index'], 'active' => \Yii::$app->controller->getUniqueId() == 'blog/manage/category'],
                ['label' => \Yii::t('blog', 'Posts'), 'icon' => 'caret-right', 'url' => ['/blog/manage/post/index'], 'active' => \Yii::$app->controller->getUniqueId() == 'blog/manage/post'],
            ], 'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('BlogManagement')
        ];*/
        //print_r(basename($app->getBasePath())); die;
        //print_r($app->basePath); die;

        if (basename($app->getBasePath()) === 'backend') {
            $app->params['adminMenu'][] = [
                'label'   => \Yii::t('shop', 'Shop'),
                'icon'    => 'shopping-cart',
                'items'   => [
                    [
                        'label' => \Yii::t('shop', 'Categories'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/category/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Products'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/product/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Brands'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/brand/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Product Types'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/product-type/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Tags'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/tag/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Characteristics'),
                        'icon'  => 'caret-right',
                        'items' => [
                            [
                                'label' => \Yii::t('shop', 'Characteristics Groups'),
                                'icon'  => 'caret-right',
                                'url'   => ['/shop/manage/characteristic-group/index'],
                            ],
                            [
                                'label' => \Yii::t('shop', 'Characteristics'),
                                'icon'  => 'caret-right',
                                'url'   => ['/shop/manage/characteristic/index'],
                            ],
                        ],
                    ],
                    [
                        'label' => \Yii::t('shop', 'Delivery Methods'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/delivery/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Orders'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/order/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Reviews'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/review/index']
                    ],
                    [
                        'label' => \Yii::t('shop', 'Discounts'),
                        'icon'  => 'caret-right',
                        'url'   => ['/shop/manage/discount/index']
                    ],
                ],
                'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('BlogManagement')
            ];
        }
    }

    public static function getMenuItemCreator($menu_id): array
    {
        $widgets   = [];
        $widgets[] = [
            'id'      => 'shop',
            'title'   => \Yii::t('shop', 'Shop'),
            'content' => MenuItemCreatorWidget::widget([
                'menu_id' => $menu_id,
            ]),
        ];

        return $widgets;
    }
}
