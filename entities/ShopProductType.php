<?php

namespace common\modules\shop\entities;

use core\behaviors\LanguageBehavior;
use core\events\EventTrait;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use core\behaviors\SluggableRelationBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * @property integer $id
 * @property string $slug
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 *
 * @property ShopCharacteristicAssignment[] $characteristicAssignments
 */
class ShopProductType extends ActiveRecord
{
    use EventTrait;

    public $_form;

    public static function create($form, $slug): self
    {
        $type = new static();
        $type->slug = $slug;
        $type->_form = $form;
        return $type;
    }

    public function edit($form, $slug): void
    {
        if ($slug != $this->slug) {
            TagDependency::invalidate(\Yii::$app->cache, 'product-types');
        }

        $this->slug = $slug;
        $this->_form = $form;
    }

    public function assignCharacteristics($id): void
    {
        $assignments = $this->characteristicAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForCharacteristic($id)) {
                //return;
                continue;
            }
        }
        $assignments[] = ShopCharacteristicAssignment::create($id);
        $this->characteristicAssignments = $assignments;
    }

    public function getSeoTitle(): string
    {
        return $this->translation->meta_title ?: $this->translation->name;
    }

    public function getCharacteristicAssignments(): ActiveQuery
    {
        return $this->hasMany(ShopCharacteristicAssignment::class, ['product_type_id' => 'id']);
    }

    ##########################

    public static function tableName(): string
    {
        return '{{%shop_product_types}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopProductTypeVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'main_id',
                'tableName'           => '{{%shop_product_types_lng}}',
                'attributes'          => [
                    'name',
                    'meta_title',
                    'meta_description',
                    'meta_keywords'
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
            [
                'class'     => SluggableRelationBehavior::class,
                'attribute' => 'name',
                'relation'  => 'translation',
            ],
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['characteristicAssignments'],
            ],
        ];
    }
}
