<?php

namespace common\modules\shop\entities;

use core\events\EventTrait;
use paulzi\nestedsets\NestedSetsBehavior;
use core\behaviors\LanguageBehavior;
use common\modules\shop\entities\queries\ShopCategoryQuery;
use core\behaviors\SluggableRelationBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\caching\TagDependency;
use Yii;

/**
 * @property integer $id
 * @property string $slug
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 *
 * @property ActiveQuery $descendants
 *
 * @property ShopCategory $parent
 * @property ShopCategory[] $parents
 * @property ShopCategory[] $children
 * @property ShopCategory $prev
 * @property ShopCategory $next
 * @mixin NestedSetsBehavior
 */
class ShopCategory extends ActiveRecord
{
    use EventTrait;

    public $_form;

    public static function create($form, $slug): self
    {
        $category        = new static();
        $category->slug  = $slug;
        $category->_form = $form;

        return $category;
    }

    public function edit($form, $slug): void
    {
        if ($slug != $this->slug) {
            TagDependency::invalidate(\Yii::$app->cache, 'categories');
        }

        $this->slug  = $slug;
        $this->_form = $form;
    }

    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('shop', 'ID'),
            'name'             => Yii::t('shop', 'Name'),
            'slug'             => Yii::t('shop', 'SEO link'),
            'title'            => Yii::t('shop', 'Title'),
            'description'      => Yii::t('shop', 'Description'),
            'meta_title'       => Yii::t('shop', 'META Title'),
            'meta_description' => Yii::t('shop', 'META Description'),
            'meta_keywords'    => Yii::t('shop', 'META Keywords'),
        ];
    }

    public function getSeoTitle(): string
    {
        return $this->translation->meta_title ?: $this->getHeadingTile();
    }

    public function getHeadingTile(): string
    {
        return $this->translation->title ?: $this->translation->name;
    }

    public static function tableName(): string
    {
        return '{{%shop_categories}}';
    }

    public function behaviors(): array
    {
        return [
            NestedSetsBehavior::class,
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopCategoryVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'shop_category_id',
                'tableName'           => '{{%shop_categories_lng}}',
                'attributes'          => [
                    'title',
                    'description',
                    'name',
                    'meta_title',
                    'meta_description',
                    'meta_keywords'
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
            [
                'class'     => SluggableRelationBehavior::class,
                'attribute' => 'name',
                'relation'  => 'translation',
            ],
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find(): ShopCategoryQuery
    {
        return new ShopCategoryQuery(static::class);
    }
}
