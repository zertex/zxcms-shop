<?php

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;
use yii\caching\TagDependency;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 */
class ShopTag extends ActiveRecord
{
    public static function create($name, $slug): self
    {
        $tag = new static();
        $tag->name = $name;
        $tag->slug = $slug;
        return $tag;
    }

    public function edit($name, $slug): void
    {
        $this->name = $name;

        if ($slug != $this->slug) {
            TagDependency::invalidate(\Yii::$app->cache, 'tags');
        }

        $this->slug = $slug;
    }

    public static function tableName(): string
    {
        return '{{%shop_tags}}';
    }
}
