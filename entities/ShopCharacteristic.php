<?php

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;
use yii\helpers\Json;
use core\behaviors\LanguageBehavior;

/**
 * @property integer $id
 * @property string $type
 * @property string $required
 * @property integer $sort
 * @property integer $group_id
 * @property integer $widget
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 *
 * @property ShopCharacteristicGroup $characteristicGroup
 *
 */
class ShopCharacteristic extends ActiveRecord
{
    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_FLOAT = 'float';

    const WIDGET_NONE = 0;
    const WIDGET_PERIOD = 1;
    const WIDGET_CHECKBOX = 2;


    public $variants;

    public $_form;

    public static function create($form, $type, $required, $sort, $groupId, $widget = 0): self
    {
        $object = new static();
        $object->type = $type;
        $object->required = $required;
        $object->sort = $sort;
        $object->group_id = $groupId;
        $object->widget = $widget;
        $object->_form = $form;
        return $object;
    }

    public function edit($form, $type, $required, $sort, $groupId, $widget = 0): void
    {
        $this->type = $type;
        $this->required = $required;
        $this->sort = $sort;
        $this->group_id = $groupId;
        $this->widget = $widget;
        $this->_form = $form;
    }

    public function isWidgetNone(): bool
    {
        return $this->widget === self::WIDGET_NONE;
    }

    public function isWidgetPeriod(): bool
    {
        return $this->widget === self::WIDGET_PERIOD;
    }

    public function isWidgetCheckbox(): bool
    {
        return $this->widget === self::WIDGET_CHECKBOX;
    }

    public function isString(): bool
    {
        return $this->type === self::TYPE_STRING;
    }

    public function isInteger(): bool
    {
        return $this->type === self::TYPE_INTEGER;
    }

    public function isFloat(): bool
    {
        return $this->type === self::TYPE_FLOAT;
    }

    public function isSelect(): bool
    {
        return count($this->variants) > 0;
    }

    public static function tableName(): string
    {
        return '{{%shop_characteristics}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopCharVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'main_id',
                'tableName'           => '{{%shop_characteristics_lng}}',
                'attributes'          => [
                    'name',
                    'default',
                    'variants_json',
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
        ];
    }

    /*public function afterFind(): void
    {
        $this->variants = array_filter(Json::decode($this->getAttribute('variants_json')));
        parent::afterFind();
    }*/

    // todo save variants
    /*public function beforeSave($insert): bool
    {
        $this->setAttribute('variants_json', Json::encode(array_filter($this->variants)));
        return parent::beforeSave($insert);
    }*/

    public function getCharacteristicGroup()
    {
        return $this->hasOne(ShopCharacteristicGroup::class, ['id' => 'group_id']);
    }
}
