<?php

namespace common\modules\shop\entities;

use common\modules\shop\entities\queries\ShopDiscountQuery;
use yii\db\ActiveRecord;
use core\behaviors\LanguageBehavior;

/**
 * @property integer $id
 * @property integer $percent
 * @property string $date_from
 * @property string $date_to
 * @property bool $status
 * @property integer $sort
 * @property integer $count
 * @property integer $type
 * @property integer $method
 * @property string $code
 * @property string $value
 * @property integer $recipient
 * @property string $value_recipient
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 *
 */
class ShopDiscount extends ActiveRecord
{
    const METHOD_PERCENT = 1;
    const METHOD_FIXED = 0;

    const TYPE_ALL_PRODUCTS = 1;
    const TYPE_CATEGORY = 2;
    const TYPE_PRODUCT = 3;
    const TYPE_PRODUCT_TYPE = 4;
    const TYPE_BRAND = 5;
    const TYPE_ORDER = 6;
    const TYPE_PROMO_CODE = 7;

    const FOR_ALL_USERS = 1;
    const FOR_USERS_GROUP = 2;
    const FOR_USER = 3;

    const STATUS_ENABLED = true;
    const STATUS_DISABLED = false;

    public $_form;

    public static function create($form, $percent, $fromDate, $toDate, $sort, $count, $type, $recipient, $method, $code, $value, $value_recipient, $active = ShopDiscount::STATUS_ENABLED): self
    {
        $discount = new static();
        $discount->percent = $percent;
        $discount->date_from = $fromDate;
        $discount->date_to = $toDate;
        $discount->sort = $sort;
        $discount->count = $count;
        $discount->type = $type;
        $discount->recipient = $recipient;
        $discount->method = $method;
        $discount->code = $code;
        $discount->value = $value;
        $discount->value_recipient = $value_recipient;
        $discount->status = $active;
        $discount->_form = $form;

        return $discount;
    }

    public function edit($form, $percent, $fromDate, $toDate, $sort, $count, $type, $recipient, $method, $code, $value, $value_recipient, $active): void
    {
        $this->percent = $percent;
        $this->date_from = $fromDate;
        $this->date_to = $toDate;
        $this->sort = $sort;
        $this->count = $count;
        $this->type = $type;
        $this->recipient = $recipient;
        $this->method = $method;
        $this->code = $code;
        $this->value = $value;
        $this->value_recipient = $value_recipient;
        $this->status = $active;
        $this->_form = $form;
    }

    public function activate(): void
    {
        $this->status = ShopDiscount::STATUS_ENABLED;
    }

    public function draft(): void
    {
        $this->status = ShopDiscount::STATUS_DISABLED;
    }

    /*public function isEnabled(): bool
    {
        return true;
    }*/

    public function isActive(): bool
    {
        return $this->status == ShopDiscount::STATUS_ENABLED;
    }

    public function downCounter(): void
    {
        $this->count = $this->count-1;
    }

    public static function tableName(): string
    {
        return '{{%shop_discounts}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'DiscountCategoryVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'shop_discount_id',
                'tableName'           => '{{%shop_discounts_lng}}',
                'attributes'          => ['name'],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
        ];
    }

    public static function find(): ShopDiscountQuery
    {
        return new ShopDiscountQuery(static::class);
    }
}
