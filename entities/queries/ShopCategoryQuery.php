<?php

namespace common\modules\shop\entities\queries;

use paulzi\nestedsets\NestedSetsQueryTrait;
use yii\db\ActiveQuery;

class ShopCategoryQuery extends ActiveQuery
{
    use NestedSetsQueryTrait;
}
