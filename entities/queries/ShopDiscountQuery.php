<?php

namespace common\modules\shop\entities\queries;

use common\modules\shop\entities\ShopDiscount;
use yii\db\ActiveQuery;

class ShopDiscountQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => ShopDiscount::STATUS_ENABLED]);
    }

    public function dateActive()
    {
        return $this
            ->andWhere('date_from <= ' . time())
            ->andWhere(['or',
                //["to_date >= DATE(NOW())"],
                ['>=', 'date_to', time()],
                ['date_to' => ''],
                ['date_to' => null]
            ]);
            //->andWhere('to_date >= DATE(NOW())');
    }

    public function countActive()
    {
        return $this->andWhere(['!=', 'count', 0]);
    }
}
