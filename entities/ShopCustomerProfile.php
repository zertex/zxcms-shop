<?php
/**
 * Created by Error202
 * Date: 06.10.2017
 */

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\Json;

/**
 * Class CustomerProfile
 * @package app\modules\shop\entities
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $company
 * @property string $name
 * @property string $phone
 */
class ShopCustomerProfile extends ActiveRecord
{
    public static function create($phone, $name, $company = null): self
    {
        $companyData = $company ? Json::decode($company, true) : [];

        $method = new static();
        $method->name = $name;
        $method->phone = $phone;
        $method->company = $company;
        $method->user_id = Yii::$app->user->id;
        $method->title = empty($companyData) ? $name . ' (' . $phone . ')' : $companyData['company'];
        return $method;
    }


    public static function tableName(): string
    {
        return '{{%shop_customer_profiles}}';
    }
}
