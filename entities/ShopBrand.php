<?php

namespace common\modules\shop\entities;

use core\events\EventTrait;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use core\behaviors\LanguageBehavior;
use core\behaviors\SluggableRelationBehavior;

/**
 * @property integer $id
 * @property string $slug
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 */
class ShopBrand extends ActiveRecord
{
    use EventTrait;

    public $_form;

    public static function create($form, $slug): self
    {
        $brand = new static();
        $brand->slug = $slug;
        $brand->_form = $form;
        return $brand;
    }

    public function edit($form, $slug): void
    {
        if ($slug != $this->slug) {
            TagDependency::invalidate(\Yii::$app->cache, 'brands');
        }
        $this->slug = $slug;
        $this->_form = $form;
    }

    public function getSeoTitle(): string
    {
        return $this->translation->meta_title ?: $this->translation->name;
    }

    ##########################

    public static function tableName(): string
    {
        return '{{%shop_brands}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'BrandCategoryVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'shop_brand_id',
                'tableName'           => '{{%shop_brands_lng}}',
                'attributes'          => [
                    'name',
                    'meta_title',
                    'meta_description',
                    'meta_keywords'
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
            [
                'class'     => SluggableRelationBehavior::class,
                'attribute' => 'name',
                'relation'  => 'translation',
            ],
        ];
    }
}
