<?php

namespace common\modules\shop\entities\product;

use common\modules\shop\entities\ShopProductType;
use common\modules\shop\events\ShopProductAppearedInStockEvent;
use common\modules\shop\events\ShopProductPriceDropEvent;
use common\modules\shop\events\ShopReviewStatusChangeEvent;
use common\modules\shop\forms\product\ShopModificationForm;
use common\modules\shop\helpers\ShopDiscountHelper;
use core\events\EventTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use common\modules\shop\entities\ShopBrand;
use common\modules\shop\entities\ShopCategory;
use common\modules\shop\entities\product\queries\ShopProductQuery;
use common\modules\shop\entities\ShopTag;
use common\modules\shop\entities\ShopWishlistItem;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;
use core\behaviors\LanguageBehavior;
use core\behaviors\SluggableRelationBehavior;

/**
 * @property integer $id
 * @property integer $created_at
 * @property string $code
 * @property integer $category_id
 * @property integer $brand_id
 * @property integer $price_old
 * @property integer $price_new
 * @property integer $rating
 * @property integer $main_photo_id
 * @property integer $status
 * @property integer $weight
 * @property integer $quantity
 * @property string $slug
 * @property integer $type_id
 * @property integer $label
 *
 * @property ShopBrand $brand
 * @property ShopCategory $category
 * @property ShopCategoryAssignment[] $categoryAssignments
 * @property ShopCategory[] $categories
 * @property ShopTagAssignment[] $tagAssignments
 * @property ShopTag[] $tags
 * @property ShopRelatedAssignment[] $relatedAssignments
 * @property ShopModification[] $modifications
 * @property ShopValue[] $values
 * @property Photo[] $photos
 * @property Photo $mainPhoto
 * @property ShopReview[] $reviews
 * @property ShopReview[] $activeReviews
 * @property ShopProductType $type
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 */
class ShopProduct extends ActiveRecord
{
    use EventTrait;

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const LABEL_NEW = 1;
    const LABEL_SALE = 2;
    const LABEL_DISCOUNT = 3;
    const LABEL_BESTSELLER = 4;
    const LABEL_BEST_PRICE = 5;
    const LABEL_RECOMMENDED = 6;
    const LABEL_POPULAR = 7;
    const LABEL_PRODUCT_DAY = 8;
    const LABEL_PROMO = 9;
    const LABEL_HOT_DEAL = 10;

    public $_form;

    public static function create($form, $brandId, $categoryId, $code, $slug, $typeId, $weight, $label, $quantity): self
    {
        $product = new static();
        $product->brand_id = $brandId;
        $product->category_id = $categoryId;
        $product->code = $code;
        $product->slug = $slug;
        $product->type_id = $typeId;
        $product->weight = $weight;
        $product->label = $label;
        $product->quantity = $quantity;
        $product->status = self::STATUS_DRAFT;
        $product->created_at = time();
        $product->_form = $form;
        return $product;
    }

    public function setPrice($new, $old): void
    {
        if ($this->price_new > $new && $this->price_new > 0) {
            $this->recordEvent(new ShopProductPriceDropEvent($this));
        }
        $this->price_new = $new;
        $this->price_old = $old;
    }

    public function changeQuantity($quantity): void
    {
        if ($this->modifications) {
            throw new \DomainException('Change modifications quantity.');
        }
        $this->setQuantity($quantity);
    }

    public function edit($form, $brandId, $code, $slug, $typeId, $weight, $label): void
    {
        $this->brand_id = $brandId;
        $this->type_id = $typeId;
        $this->code = $code;

        if ($slug != $this->slug) {
            TagDependency::invalidate(\Yii::$app->cache, 'products');
        }

        $this->slug = $slug;
        $this->weight = $weight;
        $this->label = $label;
        $this->_form = $form;
    }

    public function changeMainCategory($categoryId): void
    {
        $this->category_id = $categoryId;
    }

    public function activate(): void
    {
        if ($this->isActive()) {
            throw new \DomainException('Product is already active.');
        }
        $this->status = self::STATUS_ACTIVE;
    }

    public function draft(): void
    {
        if ($this->isDraft()) {
            throw new \DomainException('Product is already draft.');
        }
        $this->status = self::STATUS_DRAFT;
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }


    public function isDraft(): bool
    {
        return $this->status == self::STATUS_DRAFT;
    }

    public function isAvailable(): bool
    {
        return $this->quantity > 0;
    }

    public function canChangeQuantity(): bool
    {
        return !$this->modifications;
    }

    public function canBeCheckout($modificationId, $quantity): bool
    {
        if (Yii::$app->params['settings']['shop']['quantity_use'] == 0) {
            return true;
        }

        if ($modificationId) {
            return $quantity <= $this->getModification($modificationId)->quantity;
        }
        return $quantity <= $this->quantity;
    }

    public function checkout($modificationId, $quantity): void
    {
        if ($modificationId) {
            $modifications = $this->modifications;
            foreach ($modifications as $i => $modification) {
                if ($modification->isIdEqualTo($modificationId)) {
                    $modification->checkout($quantity);
                    $this->updateModifications($modifications);
                    return;
                }
            }
        }
        if ($quantity > $this->quantity && Yii::$app->params['settings']['shop']['quantity_use'] == 1) {
            throw new \DomainException('Only ' . $this->quantity . ' items are available.');
        }
        //$this->setQuantity($this->quantity - 1);
        // if used quantity
        if (Yii::$app->params['settings']['shop']['quantity_use'] == 1) {
            $this->setQuantity($this->quantity - $quantity);
        }
    }

    private function setQuantity($quantity): void
    {
        if ($this->quantity == 0 && $quantity > 0) {
            $this->recordEvent(new ShopProductAppearedInStockEvent($this));
        }
        $this->quantity = $quantity;
    }

    public function getSeoTitle(): string
    {
        return $this->translation->meta_title ?: $this->translation->name;
    }

    public function setValue($id, $value): void
    {
        $values = $this->values;
        foreach ($values as $val) {
            if ($val->isForCharacteristic($id)) {
                $val->change($value);
                $this->values = $values;
                return;
            }
        }
        $values[] = ShopValue::create($id, $value);
        $this->values = $values;
    }

    public function getValue($id): ShopValue
    {
        $values = $this->values;
        foreach ($values as $val) {
            if ($val->isForCharacteristic($id)) {
                return $val;
            }
        }
        return ShopValue::blank($id);
    }

    // Modification

    public function getModification($id): ShopModification
    {
        foreach ($this->modifications as $modification) {
            if ($modification->isIdEqualTo($id)) {
                return $modification;
            }
        }
        throw new \DomainException('Modification is not found.');
    }

    public function getModificationPrice($id): int
    {
        foreach ($this->modifications as $modification) {
            if ($modification->isIdEqualTo($id)) {
                return $modification->price ?: $this->price_new;
            }
        }
        throw new \DomainException('Modification is not found.');
    }

    public function addModification($form, $code, $price, $quantity): void
    {
        $modifications = $this->modifications;
        foreach ($modifications as $modification) {
            if ($modification->isCodeEqualTo($code)) {
                throw new \DomainException('Modification already exists.');
            }
        }
        $modifications[] = ShopModification::create($form, $code, $price, $quantity);
        $this->updateModifications($modifications);
    }

    public function editModification($id, $form, $code, $price, $quantity): void
    {
        $modifications = $this->modifications;
        foreach ($modifications as $i => $modification) {
            if ($modification->isIdEqualTo($id)) {
                $modification->edit($form, $code, $price, $quantity);
                $this->updateModifications($modifications);
                return;
            }
        }
        throw new \DomainException('Modification is not found.');
    }

    public function removeModification($id): void
    {
        $modifications = $this->modifications;
        foreach ($modifications as $i => $modification) {
            if ($modification->isIdEqualTo($id)) {
                unset($modifications[$i]);
                $this->updateModifications($modifications);
                return;
            }
        }
        throw new \DomainException('Modification is not found.');
    }

    public function updateModifications(array $modifications): void
    {
        $this->modifications = $modifications;
        $this->setQuantity(array_sum(array_map(function (ShopModification $modification) {
            return $modification->quantity;
        }, $this->modifications)));
    }

    // Categories

    public function assignCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForCategory($id)) {
                return;
            }
        }
        $assignments[] = ShopCategoryAssignment::create($id);
        $this->categoryAssignments = $assignments;
    }

    public function revokeCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForCategory($id)) {
                unset($assignments[$i]);
                $this->categoryAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException('Assignment is not found.');
    }

    public function revokeCategories(): void
    {
        $this->categoryAssignments = [];
    }

    // Tags

    public function assignTag($id): void
    {
        $assignments = $this->tagAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForTag($id)) {
                return;
            }
        }
        $assignments[] = ShopTagAssignment::create($id);
        $this->tagAssignments = $assignments;
    }

    public function revokeTag($id): void
    {
        $assignments = $this->tagAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForTag($id)) {
                unset($assignments[$i]);
                $this->tagAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException('Assignment is not found.');
    }

    public function revokeTags(): void
    {
        $this->tagAssignments = [];
    }

    // Photos

    public function addPhoto(UploadedFile $file): void
    {
        $photos = $this->photos;
        $photos[] = Photo::create($file);
        $this->updatePhotos($photos);
    }

    public function removePhoto($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo) {
            if ($photo->isIdEqualTo($id)) {
                unset($photos[$i]);
                $this->updatePhotos($photos);
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    public function removePhotos(): void
    {
        $this->updatePhotos([]);
    }

    public function movePhotoUp($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo) {
            if ($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i - 1] ?? null) {
                    $photos[$i - 1] = $photo;
                    $photos[$i] = $prev;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    public function movePhotoDown($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo) {
            if ($photo->isIdEqualTo($id)) {
                if ($next = $photos[$i + 1] ?? null) {
                    $photos[$i] = $next;
                    $photos[$i + 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    private function updatePhotos(array $photos): void
    {
        foreach ($photos as $i => $photo) {
            $photo->setSort($i);
        }
        $this->photos = $photos;
        $this->populateRelation('mainPhoto', reset($photos));
    }

    // Related products

    public function assignRelatedProduct($id): void
    {
        $assignments = $this->relatedAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForProduct($id)) {
                return;
            }
        }
        $assignments[] = ShopCategoryAssignment::create($id);
        $this->relatedAssignments = $assignments;
    }

    public function revokeRelatedProduct($id): void
    {
        $assignments = $this->relatedAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForProduct($id)) {
                unset($assignments[$i]);
                $this->relatedAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException('Assignment is not found.');
    }

    // Reviews
    public function getUserReview($userId): ?ShopReview
    {
        foreach ($this->reviews as $review) {
            if ($review->user_id && $review->product_id == $this->id) {
                return $review;
            }
        }
        return null;
    }

    public function addReview($userId, $vote, $text): void
    {
        $reviews = $this->reviews;

        if ($review = $this->getUserReview($userId)) {
            $this->editReview($review->id, $vote, $text);
        } else {
            $reviews[] = ShopReview::create($userId, $this->id, $vote, $text);
            $this->reviews = $reviews;
            $this->save();
            $this->recordEvent(new ShopReviewStatusChangeEvent($this));
            Yii::$app->session->setFlash('success', Yii::t('shop_public', 'Your review has been accepted. After verification it will be published.'));
        }
    }

    public function editReview($id, $vote, $text): void
    {
        $this->doWithReview($id, function (ShopReview $review) use ($vote, $text) {
            $review->edit($vote, $text);
            $review->active = ShopReview::STATUS_DRAFT;
            $review->save();
            $this->recordEvent(new ShopReviewStatusChangeEvent($this));
            Yii::$app->session->setFlash('success', Yii::t('shop_public', 'Your review has been changed. After verification it will be published.'));
        });
    }

    private function doWithReview($id, callable $callback): void
    {
        $reviews = $this->reviews;
        foreach ($reviews as $review) {
            if ($review->isIdEqualTo($id)) {
                $callback($review);
                $this->recordEvent(new ShopReviewStatusChangeEvent($this));
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', 'Review is not found.'));
    }

    public function removeReview($id): void
    {
        $reviews = $this->reviews;
        foreach ($reviews as $i => $review) {
            if ($review->isIdEqualTo($id)) {
                unset($reviews[$i]);
                $this->recordEvent(new ShopReviewStatusChangeEvent($this));
                return;
            }
        }
        throw new \DomainException('Review is not found.');
    }

    public function updateReviews(array $reviews): void
    {
        $amount = 0;
        $total = 0;

        foreach ($reviews as $review) {
            if ($review->isActive()) {
                $amount++;
                $total += $review->getRating();
            }
        }
        $this->reviews = $reviews;
        $this->rating = $amount ? $total / $amount : null;
    }

    // Prices
    public function getCalculatedNewPrice(): int
    {
        if ($this->price_new < 1) {
            return 1;
        }
        return ShopDiscountHelper::discountedPrice($this->price_new, $this);
    }

    public function getCalculatedOldPrice(): ?int
    {
        $price = $this->price_old ?: $this->price_new;
        $price = ShopDiscountHelper::discountedPrice($price, $this);
        if (!$this->price_old && $price == $this->price_new) {
            return null;
        }
        if ($this->price_old) {
            return $this->price_old;
        }
        return !$this->price_old && $price != $this->price_new ? $this->price_new : $price;
    }

    ##########################

    public function getBrand(): ActiveQuery
    {
        return $this->hasOne(ShopBrand::class, ['id' => 'brand_id']);
    }

    public function getType(): ActiveQuery
    {
        return $this->hasOne(ShopProductType::class, ['id' => 'type_id']);
    }

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(ShopCategory::class, ['id' => 'category_id']);
    }

    public function getCategoryAssignments(): ActiveQuery
    {
        return $this->hasMany(ShopCategoryAssignment::class, ['product_id' => 'id']);
    }

    public function getCategories(): ActiveQuery
    {
        return $this->hasMany(ShopCategory::class, ['id' => 'category_id'])->via('categoryAssignments');
    }

    public function getTagAssignments(): ActiveQuery
    {
        return $this->hasMany(ShopTagAssignment::class, ['product_id' => 'id']);
    }

    public function getTags(): ActiveQuery
    {
        return $this->hasMany(ShopTag::class, ['id' => 'tag_id'])->via('tagAssignments');
    }

    public function getModifications(): ActiveQuery
    {
        return $this->hasMany(ShopModification::class, ['product_id' => 'id']);
    }

    public function getValues(): ActiveQuery
    {
        return $this->hasMany(ShopValue::class, ['product_id' => 'id']);
    }

    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['product_id' => 'id'])->orderBy('sort');
    }

    public function getMainPhoto(): ActiveQuery
    {
        return $this->hasOne(Photo::class, ['id' => 'main_photo_id']);
    }

    public function getRelatedAssignments(): ActiveQuery
    {
        return $this->hasMany(ShopRelatedAssignment::class, ['product_id' => 'id']);
    }

    public function getRelateds(): ActiveQuery
    {
        return $this->hasMany(ShopProduct::class, ['id' => 'related_id'])->via('relatedAssignments');
    }

    public function getReviews(): ActiveQuery
    {
        return $this->hasMany(ShopReview::class, ['product_id' => 'id']);
    }

    public function getActiveReviews(): ActiveQuery
    {
        return $this->hasMany(ShopReview::class, ['product_id' => 'id'])->where(['active' => Review::STATUS_ACTIVE]);
    }

    public function getWishlistItems(): ActiveQuery
    {
        return $this->hasMany(ShopWishlistItem::class, ['product_id' => 'id']);
    }

    ##########################

    public static function tableName(): string
    {
        return '{{%shop_products}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['categoryAssignments', 'tagAssignments', 'relatedAssignments', 'modifications', 'values', 'photos', 'reviews'],
            ],
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopProductVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'main_id',
                'tableName'           => '{{%shop_products_lng}}',
                'attributes'          => [
                    'name',
                    'description',
                    'meta_title',
                    'meta_description',
                    'meta_keywords'
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
            [
                'class'     => SluggableRelationBehavior::class,
                'attribute' => 'name',
                'relation'  => 'translation',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function beforeDelete(): bool
    {
        if (parent::beforeDelete()) {
            foreach ($this->photos as $photo) {
                $photo->delete();
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes): void
    {
        $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        if (array_key_exists('mainPhoto', $related)) {
            $this->updateAttributes(['main_photo_id' => $related['mainPhoto'] ? $related['mainPhoto']->id : null]);
        }
    }

    public static function find(): ShopProductQuery
    {
        return new ShopProductQuery(static::class);
    }
}
