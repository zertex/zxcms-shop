<?php

namespace common\modules\shop\entities\product;

use common\modules\shop\entities\ShopCharacteristic;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $product_id
 * @property int $characteristic_id
 * @property string $value
 *
 * @property ShopCharacteristic $characteristic
 */
class ShopValue extends ActiveRecord
{
    public static function create($characteristicId, $value): self
    {
        $object = new static();
        $object->characteristic_id = $characteristicId;
        $object->value = $value;
        return $object;
    }

    public static function blank($characteristicId): self
    {
        $object = new static();
        $object->characteristic_id = $characteristicId;
        return $object;
    }

    public function change($value): void
    {
        $this->value = $value;
    }

    public function isForCharacteristic($id): bool
    {
        return $this->characteristic_id == $id;
    }

    public function getCharacteristic(): ActiveQuery
    {
        return $this->hasOne(ShopCharacteristic::class, ['id' => 'characteristic_id']);
    }

    public static function tableName(): string
    {
        return '{{%shop_values}}';
    }
}
