<?php

namespace common\modules\shop\entities\product\queries;

use common\modules\shop\entities\product\ShopProduct;
use yii\db\ActiveQuery;

class ShopProductQuery extends ActiveQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'status' => ShopProduct::STATUS_ACTIVE,
        ]);
    }

    public function new($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_NEW,
        ]);
    }

    public function sale($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_SALE,
        ]);
    }

    public function discount($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_DISCOUNT,
        ]);
    }

    public function bestseller($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_BESTSELLER,
        ]);
    }

    public function bestprice($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_BEST_PRICE,
        ]);
    }

    public function recommended($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_RECOMMENDED,
        ]);
    }

    public function popular($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_POPULAR,
        ]);
    }

    public function productday($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_PRODUCT_DAY,
        ]);
    }

    public function promo($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_PROMO,
        ]);
    }

    public function hotdeal($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'label' => ShopProduct::LABEL_HOT_DEAL,
        ]);
    }
}
