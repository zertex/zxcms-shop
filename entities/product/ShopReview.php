<?php

namespace common\modules\shop\entities\product;

use core\entities\user\User;
use core\events\EventTrait;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $created_at
 * @property int $user_id
 * @property int $vote
 * @property string $text
 * @property bool $active
 * @property int $product_id
 *
 * @property ShopProduct $product
 * @property User $user
 */
class ShopReview extends ActiveRecord
{
    use EventTrait;

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    public static function create($userId, $productId, int $vote, string $text): self
    {
        $review = new static();
        $review->user_id = $userId;
        $review->vote = $vote;
        $review->text = $text;
        $review->created_at = time();
        $review->active = false;
        $review->product_id = $productId;
        return $review;
    }

    public function edit($vote, $text): void
    {
        $this->vote = $vote;
        $this->text = $text;
    }

    public function activate(): void
    {
        $this->active = self::STATUS_ACTIVE;
    }

    public function draft(): void
    {
        $this->active = self::STATUS_DRAFT;
    }

    public function isActive(): bool
    {
        return $this->active === self::STATUS_ACTIVE;
    }

    public function getRating(): int
    {
        return $this->vote;
    }

    public function isIdEqualTo($id): bool
    {
        return $this->id == $id;
    }

    public static function tableName(): string
    {
        return '{{%shop_reviews}}';
    }

    public function getProduct(): ActiveQuery
    {
        return $this->hasOne(ShopProduct::class, ['id' => 'product_id']);
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
