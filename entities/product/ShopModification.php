<?php

namespace common\modules\shop\entities\product;

use yii\db\ActiveRecord;
use core\behaviors\LanguageBehavior;

/**
 * @property int $id
 * @property int $product_id
 * @property string $code
 * @property string $price
 * @property int $quantity
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 */
class ShopModification extends ActiveRecord
{
    public $_form;

    public static function create($form, $code, $price, $quantity): self
    {
        $modification = new static();
        $modification->code = $code;
        $modification->price = $price;
        $modification->quantity = $quantity;
        $modification->_form = $form;
        return $modification;
    }

    public function edit($form, $code, $price, $quantity): void
    {
        $this->code = $code;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->_form = $form;
    }

    public function checkout($quantity): void
    {
        if ($quantity > $this->quantity) {
            throw new \DomainException('Only ' . $this->quantity . ' items are available.');
        }
        $this->quantity -= $quantity;
    }

    public function isIdEqualTo($id)
    {
        return $this->id == $id;
    }

    public function isCodeEqualTo($code)
    {
        return $this->code === $code;
    }

    public static function tableName(): string
    {
        return '{{%shop_modifications}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopModificationVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'main_id',
                'tableName'           => '{{%shop_modifications_lng}}',
                'attributes'          => [
                    'name',
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
        ];
    }
}
