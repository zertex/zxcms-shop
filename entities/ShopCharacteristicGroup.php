<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;
use core\behaviors\LanguageBehavior;

/**
 * @property integer $id
 * @property integer $sort
 *
 * @property ShopCharacteristic[] $characteristics
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 *
 */
class ShopCharacteristicGroup extends ActiveRecord
{
    public $_form;

    public static function tableName(): string
    {
        return '{{%shop_characteristics_groups}}';
    }

    public static function create($form, $sort): self
    {
        $object = new static();
        $object->sort = $sort;
        $object->_form = $form;
        return $object;
    }

    public function edit($form, $sort): void
    {
        $this->sort = $sort;
        $this->_form = $form;
    }

    public function getCharacteristics()
    {
        return $this->hasMany(ShopCharacteristic::class, ['group_id' => 'id']);
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopCharGroupVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'shop_char_group_id',
                'tableName'           => '{{%shop_characteristics_groups_lng}}',
                'attributes'          => ['name'],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
        ];
    }
}
