<?php

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;

/**
 * @property integer $characteristic_id;
 * @property integer $product_type_id;
 *
 * @property ShopCharacteristic $characteristic
 */
class ShopCharacteristicAssignment extends ActiveRecord
{
    public static function create($characteristicId): self
    {
        $assignment = new static();
        $assignment->characteristic_id = $characteristicId;
        return $assignment;
    }

    public function isForCharacteristic($id): bool
    {
        return $this->characteristic_id == $id;
    }

    public static function tableName(): string
    {
        return '{{%shop_characteristics_assignments}}';
    }

    public function getCharacteristic()
    {
        return $this->hasOne(ShopCharacteristic::class, ['id' => 'characteristic_id']);
    }
}
