<?php

namespace common\modules\shop\entities\order;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use common\modules\shop\entities\product\ShopProduct;

/**
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $modification_id
 * @property string $product_name
 * @property string $product_code
 * @property string $modification_name
 * @property string $modification_code
 * @property int $price
 * @property int $quantity
 * @property int $status
 *
 * @property $product ShopProduct;
 */
class ShopOrderItem extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_HIDDEN = 0;

    public static function create(ShopProduct $product, $modificationId, $price, $quantity)
    {
        $item = new static();
        $item->product_id = $product->id;
        $item->product_name = $product->translation->name;
        $item->product_code = $product->code;
        if ($modificationId) {
            $modification = $product->getModification($modificationId);
            $item->modification_id = $modification->id;
            $item->modification_name = $modification->translation->name;
            $item->modification_code = $modification->code;
        }
        $item->price = $price;
        $item->quantity = $quantity;
        return $item;
    }

    public function getCost(): int
    {
        return $this->price * $this->quantity;
    }

    public static function tableName(): string
    {
        return '{{%shop_order_items}}';
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isHidden(): bool
    {
        return $this->status == self::STATUS_HIDDEN;
    }

    public function activate(): void
    {
        if ($this->isActive()) {
            throw new \DomainException('Order item is already active.');
        }
        $this->status = self::STATUS_ACTIVE;
        $this->save();
    }

    public function hide(): void
    {
        if ($this->isHidden()) {
            throw new \DomainException('Order item is already hidden.');
        }
        $this->status = self::STATUS_HIDDEN;
        $this->save();
    }

    public function getOrderProduct(): ShopProduct
    {
        $product = ShopProduct::findOne($this->product_id);
        $product->price_new = $this->price;
        return $product;
    }

    public function getProduct(): ActiveQuery
    {
        return $this->hasOne(ShopProduct::class, ['id' => 'product_id']);
    }
}
