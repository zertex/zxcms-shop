<?php

namespace common\modules\shop\entities\order;

class ShopStatus
{
    const NEW = 1;                  // Новый
    const PAID = 2;                 // Оплачен
    const SENT = 3;                 // Отправлен
    const COMPLETED = 4;            // Выполнен
    const CANCELLED = 5;            // Отменен продавцом
    const CANCELLED_BY_CUSTOMER = 6;// Отменен покупателем
    const PROCESSING = 7;           // В обработке

    const AWAITING_CONFIRM = 8;     // Ожидает подтверждения
    const AWAITING_PAYMENT = 9;     // Ожидает оплаты
    const AWAITING_SURCHARGE = 10;  // Ожидает доплаты
    const AWAITING_ARRIVE = 11;     // Ожидает поставки

    const COMPLETION = 12;          // Комплектация
    const PROCESSED = 13;           // Обработан
    const REFUNDED = 14;            // Возвращен
    const REFUND = 15;              // Возврат
    const PARTIALLY_REFUND = 16;    // Частичный возврат
    const IN_DELIVERY_SERVICE = 17; // Передан службе доставки
    const DELIVERY_PROCESS = 18;    // Доставляется
    const PICKUP_AVAILABLE = 19;    // Доступен для самовывоза
    const PARTIALLY_REFUNDED = 20;  // Частично возвращен

    const CHANGED = 21;             // Заказ изменен
    const AWAITING_INVOICE = 22;    // Ожидает выставления счета

    public $value;
    public $created_at;
    public $manager_id;
    public $note;

    public function __construct($value, $created_at, $manager_id = null, $note = null)
    {
        $this->value = $value;
        $this->created_at = $created_at;
        $this->manager_id = $manager_id;
        $this->note = $note;
    }
}
