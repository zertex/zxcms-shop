<?php

namespace common\modules\shop\entities\order;

class ShopDeliveryData
{
    public $index;
    public $address;

    public function __construct($index, $address)
    {
        $this->index = $index;
        $this->address = $address;
    }
}
