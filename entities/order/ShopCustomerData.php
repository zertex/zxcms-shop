<?php

namespace common\modules\shop\entities\order;

class ShopCustomerData
{
    public $phone;
    public $name;
    public $company;

    public function __construct($phone, $name, $company = null)
    {
        $this->phone = $phone;
        $this->name = $name;
        $this->company = $company;
    }
}
