<?php

namespace common\modules\shop\entities\order;

use common\modules\shop\events\ShopOrderChangedEvent;
use common\modules\shop\events\ShopStatusChangeNotifyEvent;
use common\modules\shop\forms\order\ShopStatusChangeForm;
use core\events\EventTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use common\modules\shop\entities\ShopDeliveryMethod;
use core\entities\user\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use Yii;

/**
 * @property int $id
 * @property int $created_at
 * @property int $user_id
 * @property int $delivery_method_id
 * @property string $delivery_method_name
 * @property int $delivery_cost
 * @property string $payment_method
 * @property int $cost
 * @property int $discount
 * @property int $discount_ext
 * @property int $note
 * @property int $current_status
 * @property string $cancel_reason
 * @property string $statuses_json
 * @property string $customer_phone
 * @property string $customer_name
 * @property string $delivery_index
 * @property string $delivery_address
 * @property string $customer_company
 * @property ShopCustomerData $customerData
 * @property ShopDeliveryData $deliveryData
 *
 * @property ShopOrderItem[] $items
 * @property ShopOrderItem[] $activeItems
 * @property ShopStatus[] $statuses
 */
class ShopOrder extends ActiveRecord
{
    use EventTrait;

    public $customerData;
    public $deliveryData;
    public $statuses = [];

    public static function create($userId, ShopCustomerData $customerData, array $items, $cost, $discount, $note): self
    {
        $order = new static();
        $order->user_id = $userId;
        $order->customerData = $customerData;
        $order->items = $items;
        $order->cost = $cost;
        $order->discount = $discount;
        $order->note = $note;
        $order->created_at = time();
        $order->addStatus(ShopStatus::NEW);
        return $order;
    }

    public function edit(ShopCustomerData $customerData, $note): void
    {
        $this->customerData = $customerData;
        $this->note = $note;
    }

    public function setDeliveryInfo(ShopDeliveryMethod $method, ShopDeliveryData $deliveryData): void
    {
        $this->delivery_method_id = $method->id;
        $this->delivery_method_name = $method->translation->name;
        $this->delivery_cost = $method->cost;
        $this->deliveryData = $deliveryData;
    }

    public function pay($method): void
    {
        if ($this->isStatus(ShopStatus::PAID)) {
            throw new \DomainException('Order is already paid.');
        }
        $this->payment_method = $method;
        $this->addStatus(ShopStatus::PAID);
    }
/*
    public function send(): void
    {
        if ($this->isSent()) {
            throw new \DomainException('Order is already sent.');
        }
        $this->addStatus(Status::SENT);
    }

    public function processing(): void
    {
        if ($this->isProcessing())
        {
            throw new \DomainException('Order is already processing.');
        }
        $this->addStatus(Status::COMPLETED);
    }

    public function complete(): void
    {
        if ($this->isCompleted()) {
            throw new \DomainException('Order is already completed.');
        }
        $this->addStatus(Status::COMPLETED);
    }

    public function cancel($reason): void
    {
        if ($this->isCancelled()) {
            throw new \DomainException('Order is already cancelled.');
        }
        $this->cancel_reason = $reason;
        $this->addStatus(Status::CANCELLED);
    }*/

    public function getTotalCost(): int
    {
        return $this->cost + $this->delivery_cost;
    }

    public function canBePaid(): bool
    {
        return $this->isStatus(ShopStatus::NEW);
    }
/*
    public function isNew(): bool
    {
        return $this->current_status == Status::NEW;
    }

    public function isPaid(): bool
    {
        return $this->current_status == Status::PAID;
    }

    public function isSent(): bool
    {
        return $this->current_status == Status::SENT;
    }

    public function isCompleted(): bool
    {
        return $this->current_status == Status::COMPLETED;
    }

    public function isProcessing(): bool
    {
        return $this->current_status == Status::PROCESSING;
    }

    public function isCancelled(): bool
    {
        return $this->current_status == Status::CANCELLED;
    }
*/
    private function addStatus($value, $note = null): void
    {
        $this->statuses[] = new ShopStatus($value, time(), Yii::$app->user->id, $note);
        $this->current_status = $value;
    }

    public function addStatusById(ShopStatusChangeForm $form): void
    {
        if ($this->isStatus($form->status_id)) {
            throw new \DomainException(Yii::t('shop', 'Status is already applied.'));
        }
        if ($form->note && $form->status_id == ShopStatus::CANCELLED) {
            $this->cancel_reason = $form->note;
        }
        $this->addStatus($form->status_id, $form->note);
        $this->save();
        if ($form->mail_user) {
            $this->recordEvent(new ShopStatusChangeNotifyEvent(User::findOne($this->user_id), $this));
        }
        $this->recordEvent(new ShopOrderChangedEvent($this));
    }

    public function isStatus(int $id): bool
    {
        return $this->current_status == $id;
    }

    ##########################

    public function getUser(): ActiveQuery
    {
        return $this->hasMany(User::class, ['id' => 'user_id']);
    }

    public function getDeliveryMethod(): ActiveQuery
    {
        return $this->hasOne(ShopDeliveryMethod::class, ['id' => 'delivery_method_id']);
    }

    public function getItems(): ActiveQuery
    {
        return $this->hasMany(ShopOrderItem::class, ['order_id' => 'id']);
    }

    public function getActiveItems(): ActiveQuery
    {
        return $this->hasMany(ShopOrderItem::class, ['order_id' => 'id'])->where(['status' => ShopOrderItem::STATUS_ACTIVE]);
    }

    ##########################

    public static function tableName(): string
    {
        return '{{%shop_orders}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['items'],
            ],
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterFind(): void
    {
        $this->statuses = array_map(function ($row) {
            return new ShopStatus(
                $row['value'],
                $row['created_at'],
                isset($row['manager_id']) ? $row['manager_id'] : null,
                isset($row['note']) ? $row['note'] : null
            );
        }, Json::decode($this->getAttribute('statuses_json')));

        $this->customerData = new ShopCustomerData(
            $this->getAttribute('customer_phone'),
            $this->getAttribute('customer_name'),
            $this->getAttribute('customer_company')
        );

        $this->deliveryData = new ShopDeliveryData(
            $this->getAttribute('delivery_index'),
            $this->getAttribute('delivery_address')
        );

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('statuses_json', Json::encode(array_map(function (ShopStatus $status) {
            return [
                'value' => $status->value,
                'created_at' => $status->created_at,
                'manager_id' => $status->manager_id,
                'note' => $status->note,
            ];
        }, $this->statuses)));

        $this->setAttribute('customer_phone', $this->customerData->phone);
        $this->setAttribute('customer_name', $this->customerData->name);
        $this->setAttribute('customer_company', $this->customerData->company);

        $this->setAttribute('delivery_index', $this->deliveryData->index);
        $this->setAttribute('delivery_address', $this->deliveryData->address);

        return parent::beforeSave($insert);
    }
}
