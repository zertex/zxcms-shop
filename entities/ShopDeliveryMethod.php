<?php

namespace common\modules\shop\entities;

use common\modules\shop\entities\queries\ShopDeliveryMethodQuery;
use yii\db\ActiveRecord;
use core\behaviors\LanguageBehavior;

/**
 * @property int $id
 * @property int $cost
 * @property int $min_weight
 * @property int $max_weight
 * @property int $sort
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 */
class ShopDeliveryMethod extends ActiveRecord
{
    public $_form;

    public static function create($form, $cost, $minWeight, $maxWeight, $sort): self
    {
        $method = new static();
        $method->cost = $cost;
        $method->min_weight = $minWeight;
        $method->max_weight = $maxWeight;
        $method->sort = $sort;
        $method->_form = $form;
        return $method;
    }

    public function edit($form, $cost, $minWeight, $maxWeight, $sort): void
    {
        $this->cost = $cost;
        $this->min_weight = $minWeight;
        $this->max_weight = $maxWeight;
        $this->sort = $sort;
        $this->_form = $form;
    }

    public function isAvailableForWeight($weight): bool
    {
        return (!$this->min_weight || $this->min_weight <= $weight) && (!$this->max_weight || $weight <= $this->max_weight);
    }

    public static function tableName(): string
    {
        return '{{%shop_delivery_methods}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'ShopDeliveryMethodVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'main_id',
                'tableName'           => '{{%shop_delivery_methods_lng}}',
                'attributes'          => [
                    'name',
                ],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
        ];
    }

    public static function find(): ShopDeliveryMethodQuery
    {
        return new ShopDeliveryMethodQuery(static::class);
    }
}
