<?php
/**
 * Created by Error202
 * Date: 22.09.2018
 */

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;
use core\entities\user\User;

/**
 * Class ShopWishlistItem
 * @package common\modules\shop\entities
 *
 * @property int $user_id
 * @property int $product_id
 */

class ShopWishlistItem extends ActiveRecord
{
    public static function create($productId)
    {
        $item = new static();
        $item->product_id = $productId;
        return $item;
    }

    public function isForProduct($productId): bool
    {
        return $this->product_id == $productId;
    }

    public static function tableName(): string
    {
        return '{{%shop_wishlist_items}}';
    }

    public static function getAllByProductInWishList($product_id): iterable
    {
        // todo Проверить получение пользователей по списку избранного

        //$orders = Order::find()->with(Dynamic::find()->where(['active' => 1]))->all();
        $wl_table = ShopWishlistItem::tableName();
        return User::find()
                   ->alias('u')
                   ->leftJoin($wl_table, [$wl_table . '.product_id' => $product_id, 'u.id' => $wl_table . '.user_id'])
                   //->with(Dynamic::find()->where(['active' => 1]))
                   //->joinWith('wishlistItems w', false, 'INNER JOIN')
                   //->andWhere(['w.product_id' => $product_id])
                   ->each();
    }
}
