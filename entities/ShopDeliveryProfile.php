<?php
/**
 * Created by Error202
 * Date: 06.10.2017
 */

namespace common\modules\shop\entities;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class CustomerProfile
 * @package app\modules\shop\entities
 *
 * @property int $id
 * @property int $user_id
 * @property int $method_id
 * @property string $post_index
 * @property string $post_address
 * @property string $title
 */
class ShopDeliveryProfile extends ActiveRecord
{
    public static function create($method_id, $post_index, $post_address): self
    {
        $method = new static();
        $method->title = $post_index . ', ' . $post_address;
        $method->method_id = $method_id;
        $method->post_index = $post_index;
        $method->post_address = $post_address;
        $method->user_id = Yii::$app->user->id;
        return $method;
    }


    public static function tableName(): string
    {
        return '{{%shop_delivery_profiles}}';
    }
}
