<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopProductType;
use common\modules\shop\events\ShopProductTypeDeleteEvent;
use core\repositories\NotFoundException;

class ShopProductTypeRepository
{
    public function get($id): ShopProductType
    {
        if (!$brand = ShopProductType::findOne($id)) {
            throw new NotFoundException('Product type is not found.');
        }
        return $brand;
    }

    public function save(ShopProductType $type): void
    {
        if (!$type->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopProductType $type): void
    {
        $type->recordEvent(new ShopProductTypeDeleteEvent($type));
        if (!$type->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
