<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopCharacteristic;
use core\repositories\NotFoundException;

class ShopCharacteristicRepository
{
    public function get($id): ShopCharacteristic
    {
        if (!$characteristic = ShopCharacteristic::findOne($id)) {
            throw new NotFoundException('Characteristic is not found.');
        }
        return $characteristic;
    }

    public function save(ShopCharacteristic $characteristic): void
    {
        if (!$characteristic->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopCharacteristic $characteristic): void
    {
        if (!$characteristic->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
