<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\entities\ShopCharacteristicGroup;
use core\repositories\NotFoundException;

class ShopCharacteristicGroupRepository
{
    public function get($id): ShopCharacteristicGroup
    {
        if (!$characteristic = ShopCharacteristicGroup::findOne($id)) {
            throw new NotFoundException('Characteristic group is not found.');
        }
        return $characteristic;
    }

    public function save(ShopCharacteristicGroup $characteristic): void
    {
        if (!$characteristic->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopCharacteristicGroup $characteristic): void
    {
        if (!$characteristic->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function existsByGroup($id): bool
    {
        return ShopCharacteristic::find()->andWhere(['group_id' => $id])->exists();
    }
}
