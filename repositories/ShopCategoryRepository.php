<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\events\ShopCategoryDeleteEvent;
use core\repositories\NotFoundException;
use yii\caching\TagDependency;

class ShopCategoryRepository
{
    public function get($id): ShopCategory
    {
        if (!$category = ShopCategory::findOne($id)) {
            throw new NotFoundException('Category is not found.');
        }
        return $category;
    }

    public function save(ShopCategory $category): void
    {
        if (!$category->save()) {
            throw new \RuntimeException('Saving error.');
        }
        TagDependency::invalidate(\Yii::$app->cache, 'categories_descendants');
    }

    public function remove(ShopCategory $category): void
    {
        $category->recordEvent(new ShopCategoryDeleteEvent($category));
        if (!$category->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
