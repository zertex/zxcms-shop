<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopDiscount;
use core\repositories\NotFoundException;
use yii\caching\TagDependency;

class ShopDiscountRepository
{
    public function get($id): ShopDiscount
    {
        if (!$discount = ShopDiscount::findOne($id)) {
            throw new NotFoundException('Discount is not found.');
        }
        return $discount;
    }

    public function save(ShopDiscount $discount): void
    {
        if (!$discount->save()) {
            throw new \RuntimeException('Saving error.');
        }
        TagDependency::invalidate(\Yii::$app->cache, 'discounts_active_date_count_tag');
    }

    public function remove(ShopDiscount $discount): void
    {
        if (!$discount->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        TagDependency::invalidate(\Yii::$app->cache, 'discounts_active_date_count_tag');
    }
}
