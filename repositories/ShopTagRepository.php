<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopTag;
use core\repositories\NotFoundException;

class ShopTagRepository
{
    public function get($id): ShopTag
    {
        if (!$tag = ShopTag::findOne($id)) {
            throw new NotFoundException('Tag is not found.');
        }
        return $tag;
    }

    public function findByName($name): ?ShopTag
    {
        return ShopTag::findOne(['name' => $name]);
    }

    public function save(ShopTag $tag): void
    {
        if (!$tag->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopTag $tag): void
    {
        if (!$tag->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
