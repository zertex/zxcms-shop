<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopDeliveryMethod;
use core\repositories\NotFoundException;

class ShopDeliveryMethodRepository
{
    public function get($id): ShopDeliveryMethod
    {
        if (!$method = ShopDeliveryMethod::findOne($id)) {
            throw new NotFoundException('DeliveryMethod is not found.');
        }
        return $method;
    }

    public function findByName($name): ?ShopDeliveryMethod
    {
        return ShopDeliveryMethod::findOne(['name' => $name]);
    }

    public function save(ShopDeliveryMethod $method): void
    {
        if (!$method->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopDeliveryMethod $method): void
    {
        if (!$method->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
