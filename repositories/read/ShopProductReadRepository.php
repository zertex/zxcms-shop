<?php

namespace common\modules\shop\repositories\read;

use common\modules\shop\entities\ShopCharacteristicAssignment;
use common\modules\shop\entities\ShopProductType;
//use Elasticsearch\Client;
use common\modules\shop\entities\ShopBrand;
use common\modules\shop\entities\ShopCategory;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\entities\ShopTag;
use common\modules\shop\forms\search\ShopSearchForm;
//use common\modules\shop\forms\search\ShopValueForm;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\ActiveQuery;
//use yii\db\Connection;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ShopProductReadRepository
{
    //private $client;

    /*public function __construct(Client $client)
    {
        $this->client = $client;
    }*/

    public function count(): int
    {
        return ShopProduct::find()->active()->count();
    }

    public function getAllByRange(int $offset, int $limit): array
    {
        return ShopProduct::find()->alias('p')->active('p')->orderBy(['id' => SORT_ASC])->limit($limit)->offset($offset)->all();
    }

    public function findRandomSalesLimited(int $limit = 2): array
    {
        return ShopProduct::find()->active()->andWhere(['<>', 'price_new', 'price_old'])->andWhere(['>', 'price_old', '0'])->orderBy('RAND()')->limit($limit)->all();
    }

    /**
     * @return iterable|ShopProduct[]
     */
    public function getAllIterator(): iterable
    {
        return ShopProduct::find()->alias('p')->active('p')->with('mainPhoto', 'brand')->each();
    }

    public function getAll(): DataProviderInterface
    {
        $query = ShopProduct::find()->alias('p')->active('p')->with('mainPhoto');
        return $this->getProvider($query);
    }

    public function getAllByCategory(ShopCategory $category): DataProviderInterface
    {
        $query = ShopProduct::find()->alias('p')->active('p')->with('mainPhoto', 'category');
        $ids = ArrayHelper::merge([$category->id], $category->getDescendants()->select('id')->column());
        $query->joinWith(['categoryAssignments ca'], false);
        $query->andWhere(['or', ['p.category_id' => $ids], ['ca.category_id' => $ids]]);
        $query->groupBy('p.id');
        return $this->getProvider($query);
    }

    public function getAllByBrand(ShopBrand $brand): DataProviderInterface
    {
        $query = ShopProduct::find()->alias('p')->active('p')->with('mainPhoto');
        $query->andWhere(['p.brand_id' => $brand->id]);
        return $this->getProvider($query);
    }

    public function getAllByProductType(ShopProductType $type): DataProviderInterface
    {
        $query = ShopProduct::find()->alias('p')->active('p')->with('mainPhoto');
        $query->andWhere(['p.type_id' => $type->id]);
        return $this->getProvider($query);
    }

    public function findBySlug($slug): ?ShopProduct
    {
        return ShopProduct::find()->andWhere(['slug' => $slug])->one();
    }

    public function getAllByTag(ShopTag $tag): DataProviderInterface
    {
        $query = ShopProduct::find()->alias('p')->active('p')->with('mainPhoto');
        $query->joinWith(['tagAssignments ta'], false);
        $query->andWhere(['ta.tag_id' => $tag->id]);
        $query->groupBy('p.id');
        return $this->getProvider($query);
    }

    public function getFeatured($limit): array
    {
        return ShopProduct::find()->with('mainPhoto')->orderBy(['id' => SORT_DESC])->limit($limit)->all();
    }

    public function find($id): ?ShopProduct
    {
        //$db = \Yii::$app->db;
        return ShopProduct::getDb()->cache(function ($db) use ($id) {
            return ShopProduct::find()->active()->andWhere(['id' => $id])->one();
        }, 60);
        //return Product::find()->active()->andWhere(['id' => $id])->one();
    }

    public function findMany(array $ids): ?array
    {
        return ShopProduct::getDb()->cache(function ($db) use ($ids) {
            return ShopProduct::find()->active()->andWhere(['in', 'id', $ids])->all();
        }, 60);
        //return Product::find()->active()->andWhere(['in', 'id', $ids])->all();
    }

    private function getProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [
                    'id' => [
                        'asc' => ['p.id' => SORT_ASC],
                        'desc' => ['p.id' => SORT_DESC],
                    ],
                    'name' => [
                        'asc' => ['p.name' => SORT_ASC],
                        'desc' => ['p.name' => SORT_DESC],
                    ],
                    'price' => [
                        'asc' => ['p.price_new' => SORT_ASC],
                        'desc' => ['p.price_new' => SORT_DESC],
                    ],
                    'rating' => [
                        'asc' => ['p.rating' => SORT_ASC],
                        'desc' => ['p.rating' => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => [
                'pageSizeLimit' => [15, 100],
            ]
        ]);
    }

    public function search(ShopSearchForm $form, ShopCategory $category = null): DataProviderInterface
    {
        $pagination = new Pagination([
            'pageSizeLimit' => [15, 100],
            'validatePage' => false,
        ]);

        $sort = new Sort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'name',
                'price',
                'rating',
            ],
        ]);

        // todo заменить на нормальный подбр товаров и разделов
        $ids = ArrayHelper::getColumn(ShopProduct::find()->all(), 'id');
        $categoryIds = ArrayHelper::getColumn(ShopCategory::find()->all(), 'id');

        if ($ids) {
            $query = ShopProduct::find()
                                ->active()
                                ->with('mainPhoto')
                                ->andWhere(['id' => $ids])
                                ->orderBy(new Expression('FIELD(id,' . implode(',', $ids) . ')'));
        } else {
            $query = ShopProduct::find()->andWhere(['id' => 0]);
        }

        $catsFull = [];
        if ($categoryIds) {
            $cats = ShopCategory::find()
                                ->andWhere(['id' => $categoryIds])
                                ->orderBy(new Expression('FIELD(id,' . implode(',', $categoryIds) . ')'))
                                ->all();
            foreach ($cats as $cat) {
                if ($cat->isRoot()) {
                    continue;
                }
                $catsLocal = [];
                foreach ($cat->parents as $parent) {
                    if (!$parent->isRoot()) {
                        $catsLocal[] = ['label' => $parent->translation->name, 'url' => ['category', 'id' => $parent->id]];
                    }
                }
                $catsLocal[] = ['label' => $cat->translation->name, 'url' => ['category', 'id' => $cat->id]];

                if (!empty($catsLocal)) {
                    $catsFull[] = $catsLocal;
                }
            }
        }

        return new SimpleActiveDataProvider([
            'query' => $query,
            'totalCount' => count($ids),
            'pagination' => $pagination,
            'sort' => $sort,
            'categories' => $catsFull,
        ]);
    }

    /*
    public function search(ShopSearchForm $form, ShopCategory $category = null): DataProviderInterface
    {
        $pagination = new Pagination([
            'pageSizeLimit' => [15, 100],
            'validatePage' => false,
        ]);

        $sort = new Sort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'name',
                'price',
                'rating',
            ],
        ]);

        //foreach ($form->values as $val)
        //{
        //    if ($val->isFilled())
        //    {
        //        print_r($val); die;
        //    }
        //}

        $response = $this->client->search([
            'index' => 'shop',
            'type' => 'products',
            'body' => [
                '_source' => ['id'],
                'from' => $pagination->getOffset(),
                'size' => $pagination->getLimit(),
                'sort' => array_map(function ($attribute, $direction) {
                    return [$attribute => ['order' => $direction === SORT_ASC ? 'asc' : 'desc']];
                }, array_keys($sort->getOrders()), $sort->getOrders()),
                'query' => [
                    'bool' => [
                        'must' => array_merge(
                            array_filter([
                                [
                                    'range' => [
                                        'price' => [
                                            'gte' => ($form->price_from ? : null),
                                            'lte' => ($form->price_to ? : null),
                                        ],
                                    ],
                                ],
                                $category ? ['term' => ['categories' => $category->id]] : false,
                                //!empty($form->category) ? ['term' => ['categories' => $form->category]] : false,
                                !empty($form->brand) && !is_array($form->brand) ? ['term' => ['brand' => $form->brand]] : false,
                                !empty($form->brand) && is_array($form->brand) ? ['terms' => ['brand' => $form->brand]] : false,
                                !empty($form->type) && is_array($form->type) ? ['terms' => ['type' => $form->type]] : false,
                                !empty($form->text) ? ['multi_match' => [
                                    'query' => $form->text,
                                    'fields' => [ 'name^3', 'description' ]
                                ]] : false,
                            ]),
                            array_map(function (ShopValueForm $value) {
                                $mustChar = [
                                    ['match' => ['values.characteristic' => $value->getId()]],
                                ];
                                if (isset($value->equal) && !empty($value->equal)) {
                                    foreach ($value->equal as $item) {
                                        $mustChar[] = [
                                            'match' => [
                                                'values.value_string' => $item,
                                            ],
                                        ];
                                    }
                                }
                                if (isset($value->from) && !empty($value->from)) {
                                    $mustChar[] = ['range' => ['values.value_int' => ['gte' => $value->from]]];
                                }
                                if (isset($value->to) && !empty($value->to)) {
                                    $mustChar[] = ['range' => ['values.value_int' => ['lte' => $value->to]]];
                                }

                                return ['nested' => [
                                    'path' => 'values',
                                    'query' => [
                                        'bool' => [
                                            'must' => $mustChar,
                                        ],
                                    ],
                                ]];
                            }, array_filter($form->values, function (ShopValueForm $value) { return $value->isFilled(); }))
                        )
                    ],
                ],
                'aggs' => [
                    'group_by_categories' => [
                        'terms' => [
                            'field'  => 'category',
                        ],
                    ],
                ],
            ],
        ]);
        $ids = ArrayHelper::getColumn($response['hits']['hits'], '_source.id');

        $categoryIds = ArrayHelper::getColumn($response['aggregations']['group_by_categories']['buckets'], 'key');

        if ($ids) {
            $query = ShopProduct::find()
                ->active()
                ->with('mainPhoto')
                ->andWhere(['id' => $ids])
                ->orderBy(new Expression('FIELD(id,' . implode(',', $ids) . ')'));
        } else {
            $query = ShopProduct::find()->andWhere(['id' => 0]);
        }

        $catsFull = [];
        if ($categoryIds) {
            $cats = ShopCategory::find()
                ->andWhere(['id' => $categoryIds])
                ->orderBy(new Expression('FIELD(id,' . implode(',', $categoryIds) . ')'))
                ->all();
            foreach ($cats as $cat) {
                if ($cat->isRoot()) {
                    continue;
                }
                $catsLocal = [];
                foreach ($cat->parents as $parent) {
                    if (!$parent->isRoot()) {
                        $catsLocal[] = ['label' => $parent->name, 'url' => ['category', 'id' => $parent->id]];
                    }
                }
                $catsLocal[] = ['label' => $cat->name, 'url' => ['category', 'id' => $cat->id]];

                if (!empty($catsLocal)) {
                    $catsFull[] = $catsLocal;
                }
            }
        }

        return new SimpleActiveDataProvider([
            'query' => $query,
            'totalCount' => $response['hits']['total'],
            'pagination' => $pagination,
            'sort' => $sort,
            'categories' => $catsFull,
        ]);
    }
    */

    public function getWishList($userId): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => ShopProduct::find()
                ->alias('p')->active('p')
                ->joinWith('wishlistItems w', false, 'INNER JOIN')
                ->andWhere(['w.user_id' => $userId]),
            'sort' => false,
        ]);
    }

    public function prepareCharacteristicGroupedList($id)
    {
        $product = ShopProduct::findOne($id);
        $charIds = array_map(function (ShopCharacteristicAssignment $charAssignment) use ($product) {
            return $charAssignment->characteristic_id;
        }, $product->type->characteristicAssignments);

        // Идентификаторы характеристик, принадлежащих типу товара
        $typeChars = ArrayHelper::getColumn($product->type->characteristicAssignments, 'characteristic_id');

        // Группируем характеристики
        $groups = [];
        $groupTemp = '';
        foreach ($product->values as $value) {
            if (!in_array($value->characteristic_id, $typeChars)) {
                continue;
            }

            $group = $value->characteristic->characteristicGroup->translation->name;
            if ($group != $groupTemp) {
                $groups[] = [
                    'group' => true,
                    'label' => $group,
                    'rowOptions' => ['class'=>'info'],
                ];
                $groupTemp = $group;
            }
            if (in_array($value->characteristic_id, $charIds)) {
                $groups[] = [
                    'label' => $value->characteristic->translation->name,
                    'value' => $value->value,
                ];
            }
        }
        return $groups;
    }
}
