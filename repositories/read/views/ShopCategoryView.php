<?php

namespace common\modules\shop\repositories\read\views;

use common\modules\shop\entities\ShopCategory;

class ShopCategoryView
{
    public $category;
    public $count;

    public function __construct(ShopCategory $category, $count)
    {
        $this->category = $category;
        $this->count = $count;
    }
}
