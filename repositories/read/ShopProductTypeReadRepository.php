<?php

namespace common\modules\shop\repositories\read;

use common\modules\shop\entities\ShopProductType;

class ShopProductTypeReadRepository
{
    public function find($id): ?ShopProductType
    {
        return ShopProductType::findOne($id);
    }

    public function findBySlug($slug): ?ShopProductType
    {
        return ShopProductType::find()->andWhere(['slug' => $slug])->one();
    }
}
