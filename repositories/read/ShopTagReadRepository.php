<?php

namespace common\modules\shop\repositories\read;

use common\modules\shop\entities\ShopTag;

class ShopTagReadRepository
{
    public function find($id): ?ShopTag
    {
        return ShopTag::findOne($id);
    }

    public function findBySlug($slug): ?ShopTag
    {
        return ShopTag::find()->andWhere(['slug' => $slug])->one();
    }
}
