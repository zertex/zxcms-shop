<?php

namespace common\modules\shop\repositories\read;

//use Elasticsearch\Client;
use common\modules\shop\entities\ShopCategory;
use common\modules\shop\repositories\read\views\ShopCategoryView;
use yii\helpers\ArrayHelper;

class ShopCategoryReadRepository
{
    /*private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }*/

    public function getRoot(): ShopCategory
    {
        return ShopCategory::find()->roots()->one();
    }

    /**
     * @return ShopCategory[]
     */
    public function getAll(): array
    {
        return ShopCategory::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->all();
    }

    public function find($id): ?ShopCategory
    {
        return ShopCategory::find()->andWhere(['id' => $id])->andWhere(['>', 'depth', 0])->one();
    }

    public function findBySlug($slug): ?ShopCategory
    {
        return ShopCategory::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

    public function getRandomCategoriesLimited(int $limit, ShopCategory $category = null): array
    {
        if ($category) {
            return ShopCategory::find()->andWhere(['depth' => $category->depth+1])->andWhere(['>', 'lft', $category->lft])->andWhere(['<', 'rgt', $category->rgt])->orderBy('RAND()')->limit($limit)->all();
        } else {
            return ShopCategory::find()->andWhere(['depth' => 1])->orderBy('RAND()')->limit($limit)->all();
        }
    }

    public function getAllTree()
    {
        $root = $this->getRoot();
        return $root->descendants;
    }

    public function getTreeWithSubsOf(ShopCategory $category = null): array
    {
        $query = ShopCategory::find()->andWhere(['>', 'depth', 0])->orderBy('lft');

        if ($category) {
            $criteria = ['or', ['depth' => 1]];
            foreach (ArrayHelper::merge([$category], $category->parents) as $item) {
                $criteria[] = ['and', ['>', 'lft', $item->lft], ['<', 'rgt', $item->rgt], ['depth' => $item->depth + 1]];
            }
            $query->andWhere($criteria);
        } else {
            $query->andWhere(['depth' => 1]);
        }

        /*$aggs = $this->client->search([
            'index' => 'shop',
            'type' => 'products',
            'body' => [
                'size' => 0,
                'aggs' => [
                    'group_by_category' => [
                        'terms' => [
                            'size' => 100,
                            'field' => 'categories',
                        ]
                    ]
                ],
            ],
        ]);*/

        //$counts = ArrayHelper::map($aggs['aggregations']['group_by_category']['buckets'], 'key', 'doc_count');

        /*return array_map(function (ShopCategory $category) use ($counts) {
            return new ShopCategoryView($category, ArrayHelper::getValue($counts, $category->id, 0));
        }, $query->all());*/

        return array_map(function (ShopCategory $category) {
            return new ShopCategoryView($category, 0);
        }, $query->all());
    }
}
