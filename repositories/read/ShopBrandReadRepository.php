<?php

namespace common\modules\shop\repositories\read;

use common\modules\shop\entities\ShopBrand;

class ShopBrandReadRepository
{
    public function find($id): ?ShopBrand
    {
        return ShopBrand::findOne($id);
    }

    public function findBySlug($slug): ?ShopBrand
    {
        return ShopBrand::find()->andWhere(['slug' => $slug])->one();
    }
}
