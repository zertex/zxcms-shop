<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\entities\product\ShopReview;
use core\repositories\NotFoundException;

class ShopReviewRepository
{
    public function get($id): ShopReview
    {
        if (!$review = ShopReview::findOne($id)) {
            throw new NotFoundException('Review is not found.');
        }
        return $review;
    }

    public function save(ShopReview $review): void
    {
        if (!$review->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopReview $review): void
    {
        if (!$product = ShopProduct::findOne($review->product_id)) {
            throw new NotFoundException('Product is not found.');
        }

        $product->removeReview($review->id);
        if (!$product->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }
}
