<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\events\ShopProductRemoveEvent;
use common\modules\shop\events\ShopProductSavedEvent;
use core\repositories\NotFoundException;

//use shop\repositories\events\EntityPersisted;
//use shop\repositories\events\EntityRemoved;
//use app\modules\shop\repositories\NotFoundException;

class ShopProductRepository
{
    public function get($id): ShopProduct
    {
        if (!$product = ShopProduct::findOne($id)) {
            throw new NotFoundException('Product is not found.');
        }
        return $product;
    }

    public function existsByBrand($id): bool
    {
        return ShopProduct::find()->andWhere(['brand_id' => $id])->exists();
    }

    public function existsByType($id): bool
    {
        return ShopProduct::find()->andWhere(['type_id' => $id])->exists();
    }

    public function existsByMainCategory($id): bool
    {
        return ShopProduct::find()->andWhere(['category_id' => $id])->exists();
    }

    public function save(ShopProduct $product): void
    {
        if (!$product->save()) {
            throw new \RuntimeException('Saving error.');
        }
            $product->recordEvent(new ShopProductSavedEvent($product));
            //Yii::$app->eventManager->fire(ShopModule::EVENT_PRODUCT_SAVED, new ProductSavedEvent($product), $product);
            //$this->dispatcher->dispatchAll($product->releaseEvents());
    }

    public function remove(ShopProduct $product): void
    {
        if (!$product->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $product->recordEvent(new ShopProductRemoveEvent($product));
        //Yii::$app->eventManager->fire(ShopModule::EVENT_PRODUCT_REMOVE, new ProductRemoveEvent($product), $product);
        //$this->dispatcher->dispatchAll($product->releaseEvents());
        //$this->dispatcher->dispatch(new EntityRemoved($product));
    }
}
