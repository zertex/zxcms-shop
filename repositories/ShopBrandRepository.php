<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\ShopBrand;
use common\modules\shop\events\ShopBrandDeleteEvent;
use core\repositories\NotFoundException;

class ShopBrandRepository
{
    public function get($id): ShopBrand
    {
        if (!$brand = ShopBrand::findOne($id)) {
            throw new NotFoundException('Brand is not found.');
        }
        return $brand;
    }

    public function save(ShopBrand $brand): void
    {
        if (!$brand->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopBrand $brand): void
    {
        $brand->recordEvent(new ShopBrandDeleteEvent($brand));
        if (!$brand->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
