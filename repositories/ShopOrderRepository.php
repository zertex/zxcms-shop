<?php

namespace common\modules\shop\repositories;

use common\modules\shop\entities\order\ShopOrder;
use core\repositories\NotFoundException;

class ShopOrderRepository
{
    public function get($id): ShopOrder
    {
        if (!$order = ShopOrder::findOne($id)) {
            throw new NotFoundException('Order is not found.');
        }
        return $order;
    }

    public function save(ShopOrder $order): void
    {
        if (!$order->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(ShopOrder $order): void
    {
        if (!$order->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
