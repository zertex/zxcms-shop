<?php

return [
    'version'     => '1.0.1',
    'name'        => 'shop',
    'description' => 'Shop for site with comments and slug',
    'module'      => 'ShopModule',
    'permissions' => [
        'ShopManagement' => 'Shop'
    ],
    'git_path'    => 'https://gitlab.com/zertex/zxcms-shop',
    'git_name'    => 'zxcms-shop',
    'enabled'     => false,
];
