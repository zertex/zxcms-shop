<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

namespace common\modules\shop\widgets;

use core\forms\menu\MenuItemForm;
use yii\base\Widget;

class MenuItemCreatorWidget extends Widget
{
    public $menu_id;

    public function run()
    {
        $form             = new MenuItemForm();
        $form->module     = 'shop';
        $form->name       = \Yii::t('shop', 'Shop');
        $form->title_attr = \Yii::t('shop', 'Shop');
        $form->menu_id    = $this->menu_id;
        $form->url        = '/shop/catalog/index';

        return $this->render('menu-item/creator', [
            'model' => $form,
        ]);
    }
}
