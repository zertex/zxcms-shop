<?php
/**
 * Created by Error202
 * Date: 21.06.2018
 */

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $categories \common\modules\shop\repositories\read\views\ShopCategoryView[]
 */

?>

<!-- Categories Widget -->
<div class="card my-4">
    <h5 class="card-header"><?= Yii::t('shop_public', 'Categories') ?></h5>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled mb-0 blog-categories-widget-list">
                    <?php foreach ($categories as $category) : ?>
                        <li>
                            <?= Html::a($category->category->translation->name, ['/shop/catalog/category', 'id' => $category->category->id]) ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>