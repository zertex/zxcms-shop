<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

/**
 * @var $this \yii\web\View
 * @var $model \core\forms\menu\MenuItemForm
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;

$block_name       = Yii::t('shop', 'Shop');
$block_title_attr = Yii::t('shop', 'Shop');

$js = <<<JS
	function selectShopProductOn() {
    	$("#product_select").removeAttr('disabled');
    	updateShopUrl('product');
	}

	function selectShopProductOff() {
    	$("#product_select").attr('disabled', 'disabled');
    	updateShopUrl('shop');
	}
	
	function updateShopUrl(type) {
    	if (type === 'shop') {
    	    $("#shop_menu_item_url").val('/shop/category/index');
    	    $("#shop_menu_item_url_params").val('');
    	    $("#shop_menu_item_name").val('{$block_name}');
    	    $("#shop_menu_item_title_attr").val('$block_title_attr');
    	}
    	else {
    	    var select = $("#product_select");
    	    var data = select.select2('data');
    	    var selected_product = select.val();
    	    $("#shop_menu_item_url").val('/shop/catalog/product/');
    	    $("#shop_menu_item_url_params").val('{"id":'+selected_product+'}');
    	    $("#shop_menu_item_name").val(data[0].text);
    	    $("#shop_menu_item_title_attr").val(data[0].text);
    	}
	}
JS;
$this->registerJs($js, $this::POS_HEAD);

$fetchUrl = Url::to(['/shop/manage/product/product-search']);
?>

<div class="menu_item_widget">

    <div class="form-group">
        <div class="radio">
            <?= Html::label(Html::radio('shop_item_type', true, [
                    'value'   => 'shop_home',
                    'onclick' => new JsExpression('selectShopProductOff()'),
                ]) . Yii::t('shop', 'Shop Home')) ?>
        </div>

        <div class="radio">
            <?= Html::label(Html::radio('shop_item_type', false, [
                    'value'   => 'shop_product',
                    'onclick' => new JsExpression('selectShopProductOn()'),
                ]) . Yii::t('shop', 'Shop Product')) ?>
        </div>

        <div>
            <?= \kartik\widgets\Select2::widget([
                'name'          => 'product_select',
                'value'         => '',
                'options'       => [
                    'placeholder' => Yii::t('shop', 'Select product...'),
                    'id'          => 'product_select',
                    'onchange'    => new JsExpression("updateShopUrl('product')"),
                ],
                'pluginOptions' => [
                    'disabled'          => true,
                    'ajax'              => [
                        'url'      => $fetchUrl,
                        'dataType' => 'json',
                        'data'     => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup'      => new JsExpression('function (markup) { return markup; }'),
                    'templateResult'    => new JsExpression('function(tag) { return tag.text; }'),
                    'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
                ],
            ]) ?>
        </div>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->hiddenInput([
        'id' => 'shop_menu_item_name',
    ])->label(false) ?>

    <?= $form->field($model, 'title_attr')->hiddenInput([
        'id' => 'shop_menu_item_title_attr',
    ])->label(false) ?>

    <?= $form->field($model, 'module')->hiddenInput([
    ])->label(false) ?>

    <?= $form->field($model, 'menu_id')->hiddenInput([
    ])->label(false) ?>

    <?= $form->field($model, 'url')->hiddenInput([
        'id' => 'shop_menu_item_url',
    ])->label(false) ?>

    <?= $form->field($model, 'url_params')->hiddenInput([
        'value' => '',
        'id'    => 'shop_menu_item_url_params',
    ])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('buttons', 'Add to menu'), [
            'class' => 'btn btn-info btn-sm pull-right'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>