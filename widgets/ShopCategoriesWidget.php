<?php

namespace common\modules\shop\widgets;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\repositories\read\ShopCategoryReadRepository;
use common\modules\shop\repositories\read\views\ShopCategoryView;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

class ShopCategoriesWidget extends Widget
{
    /** @var ShopCategory|null */
    public $active;

    private $_categories;

    public function __construct(ShopCategoryReadRepository $categories, $config = [])
    {
        parent::__construct($config);
        $this->_categories = $categories;
    }

    public function run(): string
    {
        $menuArray = ArrayHelper::toArray($this->_categories->getTreeWithSubsOf($this->active), [
            ShopCategoryView::class => [
                'label' => function ($view) {
                    $indent = ($view->category->depth > 1 ? str_repeat('&nbsp;&nbsp;&nbsp;', $view->category->depth - 1) : '');
                    return $indent . $view->category->translation->name;
                },
                'url' => function ($view) {
                    return ['/shop/catalog/category', 'id' => $view->category->id];
                },
            ],
        ]);

        return $this->render('categories-menu', [
            'categories' => $this->_categories->getTreeWithSubsOf($this->active),
        ]);

        /*return Menu::widget([
            'items' => $menuArray,
            'encodeLabels' => false,
            'options' => [
                'class' => 'list_links',
            ],
        ]);*/
    }
}
