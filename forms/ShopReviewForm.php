<?php

namespace common\modules\shop\forms;

use yii\base\Model;
use Yii;

class ShopReviewForm extends Model
{
    public $vote;
    public $text;

    public function rules(): array
    {
        return [
            [['vote', 'text'], 'required'],
            [['vote'], 'in', 'range' => array_keys($this->votesList())],
            ['text', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'vote' => Yii::t('shop_public', 'Vote'),
            'text' => Yii::t('shop_public', 'Review'),
        ];
    }

    public function votesList(): array
    {
        return [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
        ];
    }
}
