<?php

namespace common\modules\shop\forms\order;

use yii\base\Model;
use Yii;

class ShopStatusChangeForm extends Model
{
    public $status_id;
    public $order_id;
    public $note;
    public $mail_user;

    public function rules(): array
    {
        return [
            [['status_id', 'order_id'], 'required'],
            [['status_id', 'order_id'], 'integer'],
            [['note'], 'string'],
            [['mail_user'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'mail-user' => Yii::t('shop', 'Inform the buyer of the status change'),
        ];
    }
}
