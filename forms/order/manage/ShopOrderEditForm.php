<?php

namespace common\modules\shop\forms\order\manage;

use common\modules\shop\entities\order\ShopOrder;
use core\forms\CompositeForm;
use Yii;

/**
 * @property ShopDeliveryForm $delivery
 * @property ShopCustomerForm $customer
 */
class ShopOrderEditForm extends CompositeForm
{
    public $note;

    public function __construct(ShopOrder $order, array $config = [])
    {
        $this->note = $order->note;
        $this->delivery = new ShopDeliveryForm($order);
        $this->customer = new ShopCustomerForm($order);
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['note'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'note' => Yii::t('shop', 'Note'),
        ];
    }

    protected function internalForms(): array
    {
        return ['delivery', 'customer'];
    }
}
