<?php

namespace common\modules\shop\forms\order\manage;

use common\modules\shop\entities\ShopDeliveryMethod;
use common\modules\shop\entities\order\ShopOrder;
use common\modules\shop\helpers\PriceHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

class ShopDeliveryForm extends Model
{
    public $method;
    public $index;
    public $address;

    public function __construct(ShopOrder $order, array $config = [])
    {
        $this->method = $order->delivery_method_id;
        $this->index = $order->deliveryData->index;
        $this->address = $order->deliveryData->address;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['method'], 'integer'],
            [['index', 'address'], 'required'],
            [['index'], 'string', 'max' => 255],
            [['address'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'method' => Yii::t('shop', 'Delivery Method'),
            'index' => Yii::t('shop', 'Delivery Post Index'),
            'address' => Yii::t('shop', 'Delivery Address'),
        ];
    }

    public function deliveryMethodsList(): array
    {
        $methods = ShopDeliveryMethod::find()->orderBy('sort')->all();

        return ArrayHelper::map($methods, 'id', function (ShopDeliveryMethod $method) {
            return $method->translation->name . ' (' . PriceHelper::format($method->cost) . ')';
        });
    }
}
