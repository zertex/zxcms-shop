<?php

namespace common\modules\shop\forms\order\manage;

use common\modules\shop\entities\order\ShopOrder;
use yii\base\Model;
use Yii;

class ShopCustomerForm extends Model
{
    public $phone;
    public $name;

    public function __construct(ShopOrder $order, array $config = [])
    {
        $this->phone = $order->customerData->phone;
        $this->name = $order->customerData->name;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['phone', 'name'], 'required'],
            [['phone', 'name'], 'string', 'max' => 255],
        ];
    }
    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('shop', 'Phone'),
            'name' => Yii::t('shop', 'Customer'),
        ];
    }
}
