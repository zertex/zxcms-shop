<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopDiscount;
use common\modules\shop\validators\ShopDiscountRecipientValidator;
use common\modules\shop\validators\ShopDiscountTypeValidator;
use core\components\LanguageDynamicModel;
use yii\helpers\ArrayHelper;
use Yii;

class ShopDiscountForm extends LanguageDynamicModel
{
    public $id;
    public $percent;
    public $name;
    public $date_from;
    public $date_to;
    public $status;
    public $sort;
    public $count;
    public $type;
    public $method;
    public $code;
    public $value;
    public $recipient;
    public $value_recipient;

    public $for_count;
    public $for_to_date;

    private $_discount;

    public function __construct(ShopDiscount $discount = null, array $attributes = [], $config = [])
    {
        if ($discount) {
            $this->id = $discount->id;
            $this->percent = $discount->percent;
            $this->date_from = $discount->date_from;
            $this->date_to = $discount->date_to;
            $this->status = $discount->status;
            $this->sort = $discount->sort;
            $this->count = $discount->count;
            $this->type = $discount->type;
            $this->method = $discount->method;
            $this->code = $discount->code;
            $this->value = $discount->value;
            $this->recipient = $discount->recipient;
            $this->value_recipient = $discount->value_recipient;
            $this->for_count = $discount->count == -1 ? true : false;
            $this->for_to_date = $discount->status ? true : false;

            $this->_discount = $discount;
        }
        parent::__construct($discount, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name', 'percent', 'date_from', 'type', 'method', 'sort', 'code', 'recipient'], 'required'],
                [['name', 'code', 'value', 'value_recipient'], 'string', 'max' => 255],
                [['active', 'sort', 'count', 'method', 'id'], 'integer'],
                [['date_from', 'date_to'], 'safe'],
                [['for_count'], 'safe'],
                ['type', ShopDiscountTypeValidator::class],
                ['recipient', ShopDiscountRecipientValidator::class],
                [['code'], 'unique', 'targetClass' =>  ShopDiscount::class, 'filter' => $this->_discount ? ['<>', 'id', $this->_discount->id] : null],
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('shop', 'Name'),
                'percent' => Yii::t('shop', 'Amount'),
                'date_from' => Yii::t('shop', 'From'),
                'date_to' => Yii::t('shop', 'To'),
                'code' => Yii::t('shop', 'Discount code'),
                'value' => Yii::t('shop', 'Value'),
                'active' => Yii::t('shop', 'Active Discount'),
                'sort' => Yii::t('shop', 'Sort'),
                'count' => Yii::t('shop', 'Use count'),
                'type' => Yii::t('shop', 'Type'),
                'method' => Yii::t('shop', 'Method'),
                'for_to_date' => Yii::t('shop', 'No limit'),
                'for_count' => Yii::t('shop', 'No limit'),
                'recipient' => Yii::t('shop', 'Discount Recipient'),
                'value_recipient' => Yii::t('shop', 'Value'),
            ]
        );
    }

    public static function typeList(): array
    {
        return [
            ShopDiscount::TYPE_ALL_PRODUCTS => Yii::t('shop', 'All products'),
            ShopDiscount::TYPE_CATEGORY => Yii::t('shop', 'Category'),
            ShopDiscount::TYPE_PRODUCT => Yii::t('shop', 'Product'),
            ShopDiscount::TYPE_PRODUCT_TYPE => Yii::t('shop', 'Product Type'),
            ShopDiscount::TYPE_BRAND => Yii::t('shop', 'Brand'),
            ShopDiscount::TYPE_PROMO_CODE => Yii::t('shop', 'Promo Code'),
            ShopDiscount::TYPE_ORDER => Yii::t('shop', 'Order'),
        ];
    }

    public static function recipientList(): array
    {
        return [
            ShopDiscount::FOR_ALL_USERS => Yii::t('shop', 'All users'),
            ShopDiscount::FOR_USERS_GROUP => Yii::t('shop', 'Users group'),
            ShopDiscount::FOR_USER => Yii::t('shop', 'User'),
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->date_from = strtotime($this->date_from);
            $this->date_to = strtotime($this->date_to);
            return true;
        }
        return false;
    }

    public static function typeName($type): string
    {
        return ArrayHelper::getValue(self::typeList(), $type);
    }

    public static function recipientName($recipient): string
    {
        return ArrayHelper::getValue(self::recipientList(), $recipient);
    }

    public static function methodList(): array
    {
        return [
            ShopDiscount::METHOD_PERCENT => Yii::t('shop', 'Percent'),
            ShopDiscount::METHOD_FIXED => Yii::t('shop', 'Fixed amount'),
        ];
    }

    public static function methodName($method): string
    {
        return ArrayHelper::getValue(self::methodList(), $method);
    }

    public function getNextSort()
    {
        if ($this->sort) {
            return $this->sort;
        }
        $sort = ShopDiscount::find()->orderBy(['sort' => SORT_DESC])->limit(1)->one();
        return $sort ? $sort->sort + 1 : 1;
    }
}
