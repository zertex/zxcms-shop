<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopBrand;
use core\components\LanguageDynamicModel;
use core\validators\SlugValidator;
use Yii;

class ShopBrandForm extends LanguageDynamicModel
{
    public $name;
    public $slug;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    private $_brand;

    public function __construct(ShopBrand $brand = null, array $attributes = [], $config = [])
    {
        if ($brand) {
            $this->slug = $brand->slug;
            $this->_brand = $brand;
        }
        parent::__construct($brand, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'required'],
                [['name', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['meta_description'], 'string'],
                ['slug', SlugValidator::class],
                [['slug'], 'unique', 'targetClass' => ShopBrand::class, 'filter' => $this->_brand ? ['<>', 'id', $this->_brand->id] : null]
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('shop', 'Brand Name'),
                'slug' => Yii::t('shop', 'SEO Link'),
                'meta_title'       => Yii::t('shop', 'META Title'),
                'meta_description' => Yii::t('shop', 'META Description'),
                'meta_keywords'    => Yii::t('shop', 'META Keywords'),
            ]
        );
    }
}
