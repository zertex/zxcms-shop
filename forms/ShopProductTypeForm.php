<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\entities\ShopProductType;
use core\validators\SlugValidator;
use core\components\LanguageDynamicModel;
use Yii;
use yii\helpers\ArrayHelper;

class ShopProductTypeForm extends LanguageDynamicModel
{
    public $name;
    public $slug;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    private $_type;

    public function __construct(ShopProductType $type = null, array $attributes = [], $config = [])
    {
        if ($type) {
            $this->slug = $type->slug;
            $this->_type = $type;
        }
        parent::__construct($type, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'required'],
                [['name', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['meta_description'], 'string'],
                ['slug', SlugValidator::class],
                [['slug'], 'unique', 'targetClass' => ShopProductType::class, 'filter' => $this->_type ? ['<>', 'id', $this->_type->id] : null]
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('shop', 'Product Type Name'),
                'slug' => Yii::t('shop', 'SEO Link'),
                'meta_title'       => Yii::t('shop', 'META Title'),
                'meta_description' => Yii::t('shop', 'META Description'),
                'meta_keywords'    => Yii::t('shop', 'META Keywords'),
            ]
        );
    }


    public function groupsList(): array
    {
        return ArrayHelper::map(ShopCharacteristicGroup::find()->orderBy('sort')->asArray()->all(), 'id', 'name');
    }
}
