<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\helpers\ShopCharacteristicHelper;
use core\components\LanguageDynamicModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @property array $variants
 */
class ShopCharacteristicForm extends LanguageDynamicModel
{
    public $name;
    public $type;
    public $required;
    public $default;
    public $variants_json;
    public $sort;
    public $group_id;
    public $widget;

    private $_characteristic;

    public function __construct(ShopCharacteristic $characteristic = null, array $attributes = [], $config = [])
    {
        if ($characteristic) {
            $this->type = $characteristic->type;
            $this->required = $characteristic->required;
            //$this->textVariants = implode(PHP_EOL, $characteristic->variants);
            $this->sort = $characteristic->sort;
            $this->group_id = $characteristic->group_id;
            $this->widget = $characteristic->widget;
            $this->_characteristic = $characteristic;
        } else {
            $this->sort = ShopCharacteristic::find()->max('sort') + 1;
        }
        parent::__construct($characteristic, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name', 'type', 'sort', 'group_id'], 'required'],
                [['required'], 'boolean'],
                [['name', 'default'], 'string', 'max' => 255],
                [['variants_json'], 'string'],
                [['sort', 'group_id', 'widget'], 'integer'],
                //[['name'], 'unique', 'targetAttribute' =>  ['name', 'group_id'], 'targetClass' => ShopCharacteristic::class, 'filter' => $this->_characteristic ? ['<>', 'id', $this->_characteristic->id] : null]
                //[['name', 'group_id'], 'unique', 'targetClass' => Characteristic::class, 'filter' => $this->_characteristic ? [['<>', 'id', $this->_characteristic->id],['<>', 'id', $this->_characteristic->group_id]] : null]
                //[['name', 'group_id'], 'unique', 'targetAttribute' => ['name', 'group_id']]
                //[['name'], 'unique', 'targetClass' => Characteristic::class, 'filter' => $this->_characteristic ? [['<>', 'id', $this->_characteristic->id], ['<>', 'group_id', $this->_characteristic->group_id]] : null],
                //[['group_id'], 'unique', 'targetClass' => CharacteristicGroup::class, 'filter' => $this->_characteristic ? [['<>', 'id', $this->_characteristic->id],['<>', 'id', $this->_characteristic->group_id]] : null]
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('shop', 'Characteristic Name'),
                'type' => Yii::t('shop', 'Characteristic Type'),
                'widget' => Yii::t('shop', 'Widget'),
                'sort' => Yii::t('shop', 'Sort'),
                'group_id' => Yii::t('shop', 'Characteristic Group'),
                'required' => Yii::t('shop', 'Required'),
                'default' => Yii::t('shop', 'Default'),
                'variants_json' => Yii::t('shop', 'Text Variants'),
            ]
        );
    }

    public function typesList(): array
    {
        return ShopCharacteristicHelper::typeList();
    }

    public function widgetsList(): array
    {
        return ShopCharacteristicHelper::widgetList();
    }

    public function groupsList(): array
    {
        return ArrayHelper::map(ShopCharacteristicGroup::find()->all(), 'id', 'translation.name');
    }

    public function getVariants(): array
    {
        return preg_split('#\s+#i', $this->variants_json);
    }
}
