<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopCategory;
use core\validators\SlugValidator;
use core\components\LanguageDynamicModel;
use yii\helpers\ArrayHelper;
use Yii;

class ShopCategoryForm extends LanguageDynamicModel
{
    public $name;
    public $slug;
    public $title;
    public $description;
    public $parentId;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    private $_category;

    public function __construct(ShopCategory $category = null, array $attributes = [], $config = [])
    {
        if ($category) {
            $this->slug      = $category->slug;
            $this->parentId  = $category->parent ? $category->parent->id : null;
            $this->_category = $category;
        }
        parent::__construct($category, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'required'],
                [['parentId'], 'integer'],
                [['name', 'slug', 'title', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['description', 'meta_description'], 'string'],
                ['slug', SlugValidator::class],
                [
                    ['slug'],
                    'unique',
                    'targetClass' => ShopCategory::class,
                    'filter'      => $this->_category ? ['<>', 'id', $this->_category->id] : null
                ]
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'parentId'    => Yii::t('shop', 'Parent Category'),
                'name'        => Yii::t('shop', 'Category Name'),
                'slug'        => Yii::t('shop', 'SEO Link'),
                'title'       => Yii::t('shop', 'Category Title'),
                'description' => Yii::t('shop', 'Category Description'),
                'meta_title'       => Yii::t('shop', 'META Title'),
                'meta_description' => Yii::t('shop', 'META Description'),
                'meta_keywords'    => Yii::t('shop', 'META Keywords'),
            ]
        );
    }

    public function parentCategoriesList(): array
    {
        return ArrayHelper::map(ShopCategory::find()->orderBy('lft')->all(), 'id', function (ShopCategory $category) {
            return ($category->depth > 1 ? str_repeat('-- ', $category->depth - 1) . ' ' : '') . $category->translation->name;
        });
    }
}
