<?php

namespace common\modules\shop\forms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\ShopCategory;

class ShopCategorySearch extends Model
{
    public $id;
    public $name;
    public $slug;
    public $title;

    public $t_name;
    public $t_title;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'slug', 'title', 't_name', 't_title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopCategory::find()->andWhere(['>', 'depth', 0])->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['lft' => SORT_ASC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_categories_lng.name' => SORT_ASC],
            'desc' => ['shop_categories_lng.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['t_title'] = [
            'asc' => ['shop_categories_lng.title' => SORT_ASC],
            'desc' => ['shop_categories_lng.title' => SORT_DESC],
        ];

        $this->load($params);

        $query->andWhere(['shop_categories_lng.language' => \Yii::$app->language]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'shop_categories_lng.name', $this->t_name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'shop_categories_lng.title', $this->t_title]);

        return $dataProvider;
    }
}
