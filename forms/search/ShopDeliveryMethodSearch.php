<?php

namespace common\modules\shop\forms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\ShopDeliveryMethod;

class ShopDeliveryMethodSearch extends Model
{
    public $id;
    public $name;
    public $cost;
    public $min_weight;
    public $max_weight;

    public $t_name;

    public function rules(): array
    {
        return [
            [['id', 'cost', 'min_weight', 'max_weight'], 'integer'],
            [['name', 't_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopDeliveryMethod::find()->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_delivery_methods_lng.name' => SORT_ASC],
            'desc' => ['shop_delivery_methods_lng.name' => SORT_DESC],
            'default' => SORT_ASC,
        ];

        $this->load($params);

        $query->andWhere(['shop_delivery_methods_lng.language' => \Yii::$app->language]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cost' => $this->cost,
            'min_weight' => $this->min_weight,
            'max_weight' => $this->max_weight,
        ]);

        $query
            ->andFilterWhere(['like', 'shop_delivery_methods_lng.name', $this->t_name]);

        return $dataProvider;
    }
}
