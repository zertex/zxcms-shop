<?php

namespace common\modules\shop\forms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\ShopBrand;

class ShopBrandSearch extends Model
{
    public $id;
    public $name;
    public $slug;

    public $t_name;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'slug', 't_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopBrand::find()->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['t_name' => SORT_ASC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_brands_lng.name' => SORT_ASC],
            'desc' => ['shop_brands_lng.name' => SORT_DESC],
            'default' => SORT_ASC,
        ];

        $this->load($params);

        $query->andWhere(['shop_brands_lng.language' => \Yii::$app->language]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'shop_brands_lng.name', $this->t_name])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
