<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\ShopCharacteristic;
use yii\base\Model;

/**
 * @property integer $id
 */
class ShopValueForm extends Model
{
    public $from;
    public $to;
    public $equal;

    private $_characteristic;

    public function __construct(ShopCharacteristic $characteristic, $config = [])
    {
        $this->_characteristic = $characteristic;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return array_filter([
            $this->_characteristic->isString() ? ['equal', 'string'] : false,
            $this->_characteristic->isInteger() || $this->_characteristic->isFloat()? [['from', 'to'], 'integer'] : false
        ]);
    }

    public function isFilled(): bool
    {
        return !empty($this->from) || !empty($this->to) || !empty($this->equal);
    }

    public function variantsList(): array
    {
        return $this->_characteristic->translation->variants ? array_combine($this->_characteristic->translation->variants, $this->_characteristic->translation->variants) : [];
    }

    public function getCharacteristicName(): string
    {
        return $this->_characteristic->translation->name;
    }

    public function getWidget(): int
    {
        return $this->_characteristic->widget;
    }

    public function getId(): int
    {
        return $this->_characteristic->id;
    }

    public function formName(): string
    {
        return 'v';
    }
}
