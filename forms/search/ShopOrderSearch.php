<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\order\ShopOrder;
use common\modules\shop\helpers\ShopOrderHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ShopOrderSearch extends Model
{
    public $id;
    public $current_status;

    public function rules(): array
    {
        return [
            [['id', 'current_status'], 'integer'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopOrder::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'current_status' => $this->current_status,
        ]);

        return $dataProvider;
    }

    public function statusList(): array
    {
        return ShopOrderHelper::statusList();
    }
}
