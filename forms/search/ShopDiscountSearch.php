<?php

namespace common\modules\shop\forms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\ShopDiscount;

class ShopDiscountSearch extends Model
{
    public $id;
    public $percent;
    public $name;
    public $from_date;
    public $to_date;
    public $active;
    public $count;
    public $type;
    public $recipient;
    public $method;
    public $code;


    public function rules(): array
    {
        return [
            [['id', 'type', 'recipient', 'method', 'count', 'active', 'percent'], 'integer'],
            [['name', 'code'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopDiscount::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'method' => $this->method,
            'type' => $this->type,
            'recipient' => $this->recipient,
            'count' => $this->count,
            'percent' =>$this->percent,
            'active' => $this->active,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}
