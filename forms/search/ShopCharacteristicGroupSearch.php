<?php

namespace common\modules\shop\forms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\ShopCharacteristicGroup;

class ShopCharacteristicGroupSearch extends Model
{
    public $id;
    public $name;
    public $type;
    public $required;

    public $t_name;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 't_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopCharacteristicGroup::find()->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_characteristics_groups_lng.name' => SORT_ASC],
            'desc' => ['shop_characteristics_groups_lng.name' => SORT_DESC],
        ];

        $this->load($params);

        $query->andWhere(['shop_characteristics_groups_lng.language' => \Yii::$app->language]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'shop_characteristics_groups_lng.name', $this->name]);

        return $dataProvider;
    }
}
