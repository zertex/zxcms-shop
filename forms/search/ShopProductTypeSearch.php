<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\ShopProductType;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ShopProductTypeSearch extends Model
{
    public $id;
    public $name;
    public $slug;

    public $t_name;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'slug', 't_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopProductType::find()->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['t_name' => SORT_ASC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_product_types_lng.name' => SORT_ASC],
            'desc' => ['shop_product_types_lng.name' => SORT_DESC],
        ];

        $this->load($params);

        $query->andWhere(['shop_product_types_lng.language' => \Yii::$app->language]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'shop_product_types_lng.name', $this->t_name])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
