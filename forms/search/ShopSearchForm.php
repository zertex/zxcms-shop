<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\entities\ShopCharacteristic;
use core\forms\CompositeForm;
use yii\helpers\ArrayHelper;

/**
 * @property ShopValueForm[] $values
 */
class ShopSearchForm extends CompositeForm
{
    public $text;
    public $category;
    public $brand;

    public $price_from;
    public $price_to;
    public $type;

    public function __construct(array $config = [])
    {
        $this->values = array_map(function (ShopCharacteristic $characteristic) {
            return new ShopValueForm($characteristic);
        }, ShopCharacteristic::find()->orderBy('sort')->all());
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['text'], 'string'],
            [['category', 'brand', 'price_from', 'price_to', 'type'], 'integer'],
        ];
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(ShopCategory::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 1 ? str_repeat('-- ', $category['depth'] - 1) . ' ' : '') . $category['name'];
        });
    }

    public function formName(): string
    {
        return '';
    }

    protected function internalForms(): array
    {
        return ['values'];
    }
}
