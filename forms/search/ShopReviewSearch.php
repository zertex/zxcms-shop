<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\product\ShopReview;
use common\modules\shop\helpers\ShopReviewHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ShopReviewSearch extends Model
{
    public $id;
    public $user_id;
    public $product_id;
    public $text;
    public $vote;
    public $active;

    public function rules(): array
    {
        return [
            [['id', 'vote', 'active'], 'integer'],
            [['text'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopReview::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'user_id' => $this->user_id,
            //'product_id' => $this->product_id,
            'vote' => $this->vote,
            'active' => $this->active,
        ]);

        $query
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }

    public function statusList(): array
    {
        return ShopReviewHelper::statusList();
    }

    public function voteList(): array
    {
        return ShopReviewHelper::voteList();
    }
}
