<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\helpers\ShopCharacteristicHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\ShopCharacteristic;
use yii\helpers\ArrayHelper;

class ShopCharacteristicSearch extends Model
{
    public $id;
    public $name;
    public $type;
    public $group_id;
    public $required;
    public $widget;

    public $t_name;

    public function rules(): array
    {
        return [
            [['id', 'required', 'group_id'], 'integer'],
            [['name', 'type', 'widget', 't_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopCharacteristic::find()->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_characteristics_lng.name' => SORT_ASC],
            'desc' => ['shop_characteristics_lng.name' => SORT_DESC],
        ];

        $this->load($params);

        $query->andWhere(['shop_characteristics_lng.language' => \Yii::$app->language]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'widget' => $this->widget,
            'group_id' => $this->group_id,
            'required' => $this->required,
        ]);

        $query
            ->andFilterWhere(['like', 'shop_characteristics_lng.name', $this->t_name]);

        return $dataProvider;
    }

    public function groupsList(): array
    {
        return ArrayHelper::map(ShopCharacteristicGroup::find()->all(), 'id', 'translation.name');
    }

    public function typesList(): array
    {
        return ShopCharacteristicHelper::typeList();
    }

    public function widgetsList(): array
    {
        return ShopCharacteristicHelper::widgetList();
    }

    public function requiredList(): array
    {
        return [
            1 => \Yii::$app->formatter->asBoolean(true),
            0 => \Yii::$app->formatter->asBoolean(false),
        ];
    }
}
