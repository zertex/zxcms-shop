<?php

namespace common\modules\shop\forms\search;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\helpers\ShopProductHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\entities\product\ShopProduct;
use yii\helpers\ArrayHelper;

class ShopProductSearch extends Model
{
    public $id;
    public $code;
    public $name;
    public $category_id;
    public $brand_id;
    public $quantity;
    public $status;
    public $price_new;

    public $t_name;

    public function rules(): array
    {
        return [
            [['id', 'category_id', 'brand_id', 'status', 'quantity', 'price_new'], 'integer'],
            [['code', 'name', 't_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = ShopProduct::find()->with('mainPhoto', 'category')->joinWith('translations');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $dataProvider->sort->attributes['t_name'] = [
            'asc' => ['shop_products_lng.name' => SORT_ASC],
            'desc' => ['shop_products_lng.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'brand_id' => $this->brand_id,
            'status' => $this->status,
            'quantity' => $this->quantity,
            'price_new' => $this->price_new,
        ]);

        $query
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'shop_products_lng.name', $this->t_name]);

        return $dataProvider;
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(ShopCategory::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->all(), 'id', function (ShopCategory $category) {
            return ($category->depth > 1 ? str_repeat('-- ', $category->depth - 1) . ' ' : '') . $category->translation->name;
        });
    }

    public function statusList(): array
    {
        return ShopProductHelper::statusList();
    }
}
