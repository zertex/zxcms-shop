<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\entities\ShopCharacteristicGroup;
use common\modules\shop\entities\ShopProductType;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * @property array $newNames
 */
class ShopCharacteristicAssignmentForm extends Model
{
    public $existing = [];

    public function __construct(ShopProductType $productType = null, $config = [])
    {
        if ($productType) {
            $this->existing = ArrayHelper::getColumn($productType->characteristicAssignments, 'characteristic_id');
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            ['existing', 'each', 'rule' => ['integer']],
            ['existing', 'default', 'value' => []],
        ];
    }

    public function attributeLabels()
    {
        return [
            'existing' => Yii::t('shop', 'Characteristics'),
        ];
    }

    public function characteristicList(): array
    {
        return ArrayHelper::map(ShopCharacteristic::find()->orderBy(['group_id' => SORT_ASC, 'sort' => SORT_ASC])->asArray()->all(), 'id', 'name');
    }

    public function characteristicGroupedList(): array
    {
        $select2 = [];
        foreach (ShopCharacteristicGroup::find()->orderBy(['sort' => SORT_ASC])->all() as $cg) {
            /* @var $cg ShopCharacteristicGroup */
            $select2[$cg->translation->name] = ArrayHelper::map($cg->characteristics, 'id', 'translation.name');
        }
        return $select2;
    }
}
