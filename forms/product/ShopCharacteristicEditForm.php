<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\entities\product\ShopProduct;
use core\forms\CompositeForm;

/**
 * @property ShopValueForm[] $values
 */
class ShopCharacteristicEditForm extends CompositeForm
{
    private $_product;

    public function __construct(ShopProduct $product, $config = [])
    {
        $this->values = array_map(function (ShopCharacteristic $characteristic) use ($product) {
            return new ShopValueForm($characteristic, $product->getValue($characteristic->id));
        }, ShopCharacteristic::find()->orderBy('sort')->all());
        $this->_product = $product;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [];
    }

    protected function internalForms(): array
    {
        return ['values'];
    }
}
