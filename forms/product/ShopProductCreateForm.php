<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\ShopBrand;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\entities\ShopProductType;
use core\forms\CompositeLanguageForm;
use yii\helpers\ArrayHelper;
use core\validators\SlugValidator;
use Yii;

/**
 * @property ShopPriceForm $price
 * @property ShopQuantityForm $quantity
 * @property ShopCategoriesForm $categories
 * @property ShopPhotosForm $photos
 * @property ShopTagsForm $tags
 */
class ShopProductCreateForm extends CompositeLanguageForm
{
    public $brandId;
    public $code;
    public $name;
    public $description;
    public $slug;
    public $typeId;
    public $weight;
    public $label;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    private $_product;

    public function __construct(ShopProduct $product = null, array $attributes = [], $config = [])
    {
        $this->price = new ShopPriceForm();
        $this->quantity = new ShopQuantityForm();
        $this->categories = new ShopCategoriesForm();
        $this->photos = new ShopPhotosForm();
        $this->tags = new ShopTagsForm();
        $this->_product = $product;
        parent::__construct($product, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['brandId', 'code', 'name', 'weight', 'typeId'], 'required'],
                [['code', 'name', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['brandId', 'typeId', 'label'], 'integer'],
                [['code', 'slug'], 'unique', 'targetClass' => ShopProduct::class],
                [['description', 'meta_description'], 'string'],
                ['weight', 'integer', 'min' => 0],
                ['slug', SlugValidator::class],
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'brandId' => Yii::t('shop', 'Brand Name'),
                'typeId' => Yii::t('shop', 'Product Type'),
                'code' => Yii::t('shop', 'Code'),
                'name' => Yii::t('shop', 'Product Name'),
                'description' => Yii::t('shop', 'Product Description'),
                'slug' => Yii::t('shop', 'SEO Link'),
                'weight' => Yii::t('shop', 'Weight'),
                'label' => Yii::t('shop', 'Label'),
                'meta_title'       => Yii::t('shop', 'META Title'),
                'meta_description' => Yii::t('shop', 'META Description'),
                'meta_keywords'    => Yii::t('shop', 'META Keywords'),
            ]
        );
    }

    public function brandsList(): array
    {
        $brands = ShopBrand::find()->joinWith('translations')->orderBy('name')->all();
        return ArrayHelper::map($brands, 'id', 'translation.name');
        //return ArrayHelper::map(ShopBrand::find()->orderBy('name')->asArray()->all(), 'id', 'translate.name');
    }

    public function typesList(): array
    {
        $types = ShopProductType::find()->joinWith('translations')->orderBy('name')->all();
        return ArrayHelper::map($types, 'id', 'translation.name');
        //return ArrayHelper::map(ShopProductType::find()->orderBy('name')->asArray()->all(), 'id', 'translate.name');
    }

    protected function internalForms(): array
    {
        return ['price', 'quantity', 'photos', 'categories', 'tags'];
    }
}
