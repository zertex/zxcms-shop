<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\ShopCharacteristic;
use common\modules\shop\entities\product\ShopValue;
use yii\base\Model;

/**
 * @property integer $id
 */
class ShopValueForm extends Model
{
    public $value;

    public $group = false;
    public $groupLabel = '';

    private $_characteristic;

    public function __construct(ShopCharacteristic $characteristic, ShopValue $value = null, $config = [])
    {
        if ($value) {
            $this->value = $value->value;
        }
        $this->_characteristic = $characteristic;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return array_filter([
            $this->_characteristic->required ? ['value', 'required'] : false,
            $this->_characteristic->isString() ? ['value', 'string', 'max' => 255] : false,
            $this->_characteristic->isInteger() ? ['value', 'integer'] : false,
            $this->_characteristic->isFloat() ? ['value', 'number'] : false,
            ['value', 'safe'],
        ]);
    }

    public function attributeLabels(): array
    {
        return [
            'value' => $this->_characteristic->translation->name,
        ];
    }

    public function variantsList(): array
    {
        return $this->_characteristic->variants ? array_combine($this->_characteristic->variants, $this->_characteristic->variants) : [];
    }

    public function getId(): int
    {
        return $this->_characteristic->id;
    }
}
