<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\product\ShopModification;
use core\components\LanguageDynamicModel;
use Yii;

class ShopModificationForm extends LanguageDynamicModel
{
    public $code;
    public $name;
    public $price;
    public $quantity;

    public function __construct(ShopModification $modification = null, array $attributes = [], $config = [])
    {
        if ($modification) {
            $this->code = $modification->code;
            $this->price = $modification->price;
            $this->quantity = $modification->quantity;
        }
        parent::__construct($modification, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['code', 'name', 'quantity'], 'required'],
                ['name', 'string', 'max' => 255],
                [['price'], 'integer'],
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'code' => Yii::t('shop', 'Code'),
                'name' => Yii::t('shop', 'Modification Name'),
                'price' => Yii::t('shop', 'Price'),
                'quantity' => Yii::t('shop', 'Quantity'),
            ]
        );
    }
}
