<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\entities\ShopTag;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * @property array $newNames
 */
class ShopTagsForm extends Model
{
    public $existing = [];
    public $textNew;

    public function __construct(ShopProduct $product = null, $config = [])
    {
        if ($product) {
            $this->existing = ArrayHelper::getColumn($product->tagAssignments, 'tag_id');
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            ['existing', 'each', 'rule' => ['integer']],
            ['existing', 'default', 'value' => []],
            ['textNew', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'existing' => Yii::t('shop', 'Existing Tags'),
            'textNew' => Yii::t('shop', 'Create Tag'),
        ];
    }

    public function tagsList(): array
    {
        return ArrayHelper::map(ShopTag::find()->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    public function getNewNames(): array
    {
        return array_filter(array_map('trim', preg_split('#\s*,\s*#i', $this->textNew)));
    }
}
