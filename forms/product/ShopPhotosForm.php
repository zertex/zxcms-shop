<?php

namespace common\modules\shop\forms\product;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ShopPhotosForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public function rules(): array
    {
        return [
            ['files', 'each', 'rule' => ['image']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'files' => Yii::t('shop', 'Files'),
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->files = UploadedFile::getInstances($this, 'files');
            return true;
        }
        return false;
    }
}
