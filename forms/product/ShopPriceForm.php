<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\product\ShopProduct;
use yii\base\Model;
use Yii;

/**
 * @property ShopCategoriesForm $categories
 * @property ShopTagsForm $tags
 * @property ShopValueForm[] $values
 */
class ShopPriceForm extends Model
{
    public $old;
    public $new;

    public function __construct(ShopProduct $product = null, $config = [])
    {
        if ($product) {
            $this->new = $product->price_new;
            $this->old = $product->price_old;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['new'], 'required'],
            [['old', 'new'], 'integer', 'min' => 0],
        ];
    }

    public function attributeLabels()
    {
        return [
            'new' => Yii::t('shop', 'Price New'),
            'old' => Yii::t('shop', 'Price Old'),
        ];
    }
}
