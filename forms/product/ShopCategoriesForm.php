<?php

namespace common\modules\shop\forms\product;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\entities\product\ShopProduct;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

class ShopCategoriesForm extends Model
{
    public $main;
    public $others = [];

    public function __construct(ShopProduct $product = null, $config = [])
    {
        if ($product) {
            $this->main = $product->category_id;
            $this->others = ArrayHelper::getColumn($product->categoryAssignments, 'category_id');
        }
        parent::__construct($config);
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(ShopCategory::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->all(), 'id', function (ShopCategory $category) {
            return ($category->depth > 1 ? str_repeat('-- ', $category->depth - 1) . ' ' : '') . $category->translation->name;
        });
    }

    public function rules(): array
    {
        return [
            ['main', 'required'],
            ['main', 'integer'],
            ['others', 'each', 'rule' => ['integer']],
            ['others', 'default', 'value' => []],
        ];
    }
    public function attributeLabels()
    {
        return [
            'main' => Yii::t('shop', 'Main Category'),
            'others' => Yii::t('shop', 'Others'),
        ];
    }
}
