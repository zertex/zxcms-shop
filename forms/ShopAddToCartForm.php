<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\product\ShopModification;
use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\helpers\PriceHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

class ShopAddToCartForm extends Model
{
    public $modification;
    public $quantity;

    private $_product;

    public function __construct(ShopProduct $product, $config = [])
    {
        $this->_product = $product;
        $this->quantity = 1;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return array_filter([
            $this->_product->modifications ? ['modification', 'required'] : false,
            ['quantity', 'required'],
            Yii::$app->params['settings']['shop']['quantity_use'] == 1 ? ['quantity', 'integer', 'max' => $this->_product->quantity] : false,
        ]);
    }

    public function attributeLabels()
    {
        return [
            'modification' => Yii::t('shop_public', 'Modification'),
            'quantity' => Yii::t('shop_public', 'Quantity'),
        ];
    }

    public function modificationsList(): array
    {
        return ArrayHelper::map($this->_product->modifications, 'id', function (ShopModification $modification) {
            return $modification->code . ' - ' . $modification->translation->name . ' (' . PriceHelper::format($modification->price ?: $this->_product->price_new) . ')';
        });
    }

    public function modificationsListSelect2(): array
    {
        return ArrayHelper::map($this->_product->modifications, 'id', function (ShopModification $modification) {
            return $modification->code . '||' . $modification->translation->name . '||' . PriceHelper::format($modification->price ?: $this->_product->price_new) . '||' . $modification->quantity;
        });
    }
}
