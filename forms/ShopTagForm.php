<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopTag;
use core\validators\SlugValidator;
use yii\base\Model;
use Yii;

class ShopTagForm extends Model
{
    public $name;
    public $slug;

    private $_tag;

    public function __construct(ShopTag $tag = null, $config = [])
    {
        if ($tag) {
            $this->name = $tag->name;
            $this->slug = $tag->slug;
            $this->_tag = $tag;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['slug', SlugValidator::class],
            [['name', 'slug'], 'unique', 'targetClass' => ShopTag::class, 'filter' => $this->_tag ? ['<>', 'id', $this->_tag->id] : null]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Tag Name'),
            'slug' => Yii::t('shop', 'SEO Link'),
        ];
    }
}
