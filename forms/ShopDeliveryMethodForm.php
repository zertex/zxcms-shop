<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopDeliveryMethod;
use core\components\LanguageDynamicModel;
use Yii;

class ShopDeliveryMethodForm extends LanguageDynamicModel
{
    public $name;
    public $cost;
    public $minWeight;
    public $maxWeight;
    public $sort;

    private $_delivery_method;

    public function __construct(ShopDeliveryMethod $method = null, array $attributes = [], $config = [])
    {
        if ($method) {
            $this->cost = $method->cost;
            $this->minWeight = $method->min_weight;
            $this->maxWeight = $method->max_weight;
            $this->sort = $method->sort;
            $this->_delivery_method = $method;
        } else {
            $this->sort = ShopDeliveryMethod::find()->max('sort') + 1;
        }
        parent::__construct($method, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name', 'cost', 'sort'], 'required'],
                [['name'], 'string', 'max' => 255],
                [['cost', 'minWeight', 'maxWeight', 'sort'], 'integer'],
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('shop', 'Method Name'),
                'cost' => Yii::t('shop', 'Cost'),
                'minWeight' => Yii::t('shop', 'Min Weight'),
                'maxWeight' => Yii::t('shop', 'Max Weight'),
                'sort' => Yii::t('shop', 'Sort'),
            ]
        );
    }
}
