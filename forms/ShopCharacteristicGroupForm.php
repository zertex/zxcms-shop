<?php

namespace common\modules\shop\forms;

use common\modules\shop\entities\ShopCharacteristicGroup;
use core\components\LanguageDynamicModel;
use Yii;

class ShopCharacteristicGroupForm extends LanguageDynamicModel
{
    public $name;
    public $sort;

    private $_characteristic;

    public function __construct(ShopCharacteristicGroup $characteristic = null, array $attributes = [], $config = [])
    {
        if ($characteristic) {
            $this->sort = $characteristic->sort;
            $this->_characteristic = $characteristic;
        } else {
            $this->sort = ShopCharacteristicGroup::find()->max('sort') + 1;
        }
        parent::__construct($characteristic, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name', 'sort'], 'required'],
                [['sort'], 'integer'],
                ['name', 'string', 'max' => 255],
                //[['name'], 'unique', 'targetClass' => ShopCharacteristicGroup::class, 'filter' => $this->_characteristic ? ['<>', 'id', $this->_characteristic->id] : null]
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('shop', 'Characteristic Group Name'),
                'sort' => Yii::t('shop', 'Sort'),
            ]
        );
    }
}
