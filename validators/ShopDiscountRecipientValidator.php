<?php

namespace common\modules\shop\validators;

use common\modules\shop\entities\ShopDiscount;
use yii\validators\Validator;

class ShopDiscountRecipientValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = \Yii::t('shop', 'Value for recipient is empty');
    }

    public function validateAttribute($model, $attribute)
    {
        /* @var $model ShopDiscount */
        if (!in_array($model->recipient, [1]) && !$model->value_recipient) {
            $this->addError($model, 'value_recipient', $this->message);
        }
    }
}
