<?php

namespace common\modules\shop\validators;

use common\modules\shop\entities\ShopDiscount;
use yii\validators\Validator;

class ShopDiscountTypeValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = \Yii::t('shop', 'Value for type is empty');
    }

    public function validateAttribute($model, $attribute)
    {
        /* @var $model ShopDiscount */
        if (!in_array($model->type, [1,6,7]) && !$model->value) {
            $this->addError($model, 'value', $this->message);
        }
    }
}
