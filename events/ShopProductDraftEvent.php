<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\product\ShopProduct;
//use Elasticsearch\ClientBuilder;
//use app\modules\shop\services\search\ProductIndexer;
use yii\base\Event;

class ShopProductDraftEvent extends Event
{
    public $product;

    public function __construct(ShopProduct $product, array $config = [])
    {
        parent::__construct($config);
        $this->product = $product;
    }

    public function ShopProductDraft($event)
    {
    }
}
