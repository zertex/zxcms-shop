<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\ShopWishlistItem;
use core\entities\user\User;
use common\modules\shop\entities\product\ShopProduct;
use core\repositories\user\UserRepository;
use yii\base\Event;
use common\modules\shop\events\jobs\ProductAppearedInStockJob;
use Yii;

class ShopProductAppearedInStockEvent extends Event
{
    public $product;

    public function __construct(ShopProduct $product, array $config = [])
    {
        parent::__construct($config);
        $this->product = $product;
    }

    public function ShopProductAppearedInStock($event)
    {
        //$userRepository = new UserRepository();
        if ($event->product->isActive()) {
            //foreach ($userRepository->getAllByProductInWishList($event->product->id) as $user) {
            //foreach ($this->_user_repository->getAllByProductInWishList($event->product->id) as $user) {
            foreach (ShopWishlistItem::getAllByProductInWishList($event->product->id) as $user) {
                /* @var $user User */
                if ($user->isActive()) {
                    try {
                        self::sendEmailNotification($user, $event->product);
                    } catch (\Exception $e) {
                        $this->errorHandler->handleException($e);
                    }
                }
            }
        }
    }

    private static function sendEmailNotification(User $user, ShopProduct $product)
    {
        if (!$user->email) {
            return;
        }

        Yii::$app->queue->push(new ProductAppearedInStockJob([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]));
    }
}
