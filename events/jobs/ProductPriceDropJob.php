<?php
/**
 * Created by Error202
 * Date: 08.10.2017
 */

namespace common\modules\shop\events\jobs;

use common\modules\shop\entities\product\ShopProduct;
use core\entities\user\User;
use yii\base\BaseObject;
use Yii;
use yii\queue\JobInterface;

class ProductPriceDropJob extends BaseObject implements JobInterface
{
    public $user_id;
    public $product_id;

    public function execute($queue)
    {
        $user = User::findOne($this->user_id);
        $product = ShopProduct::findOne($this->product_id);
// todo Настройит письма из шаблона или из модуля
        $mailer = Yii::$app->mailer;
        $sent = $mailer
            ->compose(
                ['html' => '@app/web/templates/default/mail/shop/product/price-drop-html', 'text' => '@app/web/templates/default/mail/shop/product/price-drop-text'],
                ['user' => $user, 'product' => $product]
            )
            ->setTo($user->email)
            ->setSubject('Product price is dropped')
            ->send();
        if (!$sent) {
            throw new \RuntimeException('Email sending error to ' . $user->email);
        }
    }
}
