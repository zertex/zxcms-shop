<?php
/**
 * Created by Error202
 * Date: 08.10.2017
 */

namespace common\modules\shop\events\jobs;

use common\modules\shop\entities\product\ShopProduct;
use core\entities\user\User;
use yii\base\BaseObject;
use Yii;
use yii\queue\JobInterface;

class ProductAppearedInStockJob extends BaseObject implements JobInterface
{
    public $user_id;
    public $product_id;

    public function execute($queue)
    {
        $user = User::findOne($this->user_id);
        $product = ShopProduct::findOne($this->product_id);

        // todo выбирать файл писем, если есть из шаблона иначе встроить в модуль
        $mailer = Yii::$app->mailer;
        $sent = $mailer
            ->compose(
                ['html' => '@app/web/templates/default/mail/shop/product/available-html', 'text' => '@app/web/templates/default/mail/shop/product/available-text'],
                ['user' => $user, 'product' => $product]
            )
            ->setTo($user->email)
            ->setSubject('Product is available')
            ->send();

        if (!$sent) {
            throw new \RuntimeException('Email sending error to ' . $user->email);
        }
    }
}
