<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\ShopWishlistItem;
use core\entities\user\User;
use common\modules\shop\entities\product\ShopProduct;
use yii\base\Event;
use common\modules\shop\events\jobs\ProductPriceDropJob;
use Yii;

class ShopProductPriceDropEvent extends Event
{
    public $product;

    public function __construct(ShopProduct $product, array $config = [])
    {
        parent::__construct($config);
        $this->product = $product;
    }

    public function ShopProductPriceDrop($event)
    {
        if ($event->product->isActive()) {
            foreach (ShopWishlistItem::getAllByProductInWishList($event->product->id) as $user) {
                /* @var $user User */
                if ($user->isActive()) {
                    try {
                        self::sendEmailNotification($user, $event->product);
                    } catch (\Exception $e) {
                        $this->errorHandler->handleException($e);
                    }
                }
            }
        }
    }

    private static function sendEmailNotification(User $user, ShopProduct $product)
    {
        if (!$user->email) {
            return;
        }

        Yii::$app->queue->push(new ProductPriceDropJob([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]));
    }
}
