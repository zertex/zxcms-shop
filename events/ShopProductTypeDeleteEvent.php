<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\ShopProductType;
use yii\base\Event;
use common\modules\shop\entities\ShopDiscount;

class ShopProductTypeDeleteEvent extends Event
{
    public $type;

    public function __construct(ShopProductType $type, array $config = [])
    {
        parent::__construct($config);
        $this->type = $type;
    }

    public function ShopProductTypeDelete($event)
    {
        ShopDiscount::deleteAll(['type' => ShopDiscount::TYPE_PRODUCT_TYPE, 'value' => $event->type->id]);
    }
}
