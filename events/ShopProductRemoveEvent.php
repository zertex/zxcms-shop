<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\product\ShopProduct;
//use Elasticsearch\ClientBuilder;
//use app\modules\shop\services\search\ProductIndexer;
use yii\base\Event;

class ShopProductRemoveEvent extends Event
{
    public $product;

    public function __construct(ShopProduct $product, array $config = [])
    {
        parent::__construct($config);
        $this->product = $product;
    }

    public function ShopProductRemove($event)
    {
        /*$client = $client = ClientBuilder::create()->build();

        $params = [
            'id' => $event->product->id,
            'index' => 'shop',
            'type' => 'products',
        ];

        $exists = $client->exists($params);

        $indexer = new ProductIndexer($client);

        if ($exists)
        {
            $indexer->remove($event->product);
        }
*/
    }
}
