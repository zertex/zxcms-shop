<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\product\ShopProduct;
use common\modules\shop\repositories\ShopProductRepository;
use yii\base\Event;

class ShopReviewStatusChangeEvent extends Event
{
    public $product;

    public function __construct(ShopProduct $product, array $config = [])
    {
        parent::__construct($config);
        $this->product = $product;
    }

    public function ShopReviewStatusChange($event)
    {
        $event->product->updateReviews($event->product->reviews);
        (new ShopProductRepository())->save($event->product);
    }
}
