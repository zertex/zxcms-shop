<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

//use core\helpers\PhoneHelper;
use common\modules\shop\helpers\ShopOrderHelper;
use core\entities\user\User;
use common\modules\shop\entities\order\ShopOrder;
//use app\components\sms\SmsSender;
use yii\base\Event;
use Yii;

class ShopStatusChangeNotifyEvent extends Event
{
    public $user;
    public $order;

    public function __construct(User $user, ShopOrder $order, array $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
        $this->order = $order;
    }

    public function ShopStatusChangeNotify($event)
    {
        if (!$event->user->email) {
            return;
        }

        // todo email file from template or module
        $mailer = \Yii::$app->mailer;
        $sent = $mailer
            ->compose(
                ['html' => '@app/web/templates/default/mail/shop/order/status-html', 'text' => '@app/web/templates/default/mail/shop/order/status-text'],
                ['user' => $event->user, 'order' => $event->order]
            )
            ->setTo($event->user->email)
            ->setSubject(Yii::t('shop_public', 'Status changed for order {num} on {status}', ['num' => $event->order->id, 'status' => ShopOrderHelper::statusName($event->order->current_status)]))
            ->send();

        //if (PhoneHelper::isCorrect($event->user->phone && Yii::$app->settings->data['system']['sms_use'] == 1)) {
            /* @ var $smsSender SmsSender */
            //$smsSender = Yii::$container->get(SmsSender::class);
            //$smsSender->send(PhoneHelper::normalizePhone($event->user->phone), Yii::t('shop_public', 'Status changed for order {num} on {status}', ['num' => $event->order->id, 'status' => OrderHelper::statusName($event->order->current_status)]));
        //}

        if (!$sent) {
            throw new \RuntimeException('Email sending error.');
        }
    }
}
