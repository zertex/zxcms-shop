<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\ShopCategory;
use yii\base\Event;
use common\modules\shop\entities\ShopDiscount;
use yii\caching\TagDependency;
use Yii;

class ShopCategoryDeleteEvent extends Event
{
    public $category;

    public function __construct(ShopCategory $category, array $config = [])
    {
        parent::__construct($config);
        $this->category = $category;
    }

    public function ShopCategoryDelete($event)
    {
        TagDependency::invalidate(Yii::$app->cache, 'categories_descendants');
        ShopDiscount::deleteAll(['type' => ShopDiscount::TYPE_CATEGORY, 'value' => $event->category->id]);
    }
}
