<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\ShopBrand;
use yii\base\Event;
use common\modules\shop\entities\ShopDiscount;

class ShopBrandDeleteEvent extends Event
{
    public $brand;

    public function __construct(ShopBrand $brand, array $config = [])
    {
        parent::__construct($config);
        $this->brand = $brand;
    }

    public function ShopBrandDelete($event)
    {
        ShopDiscount::deleteAll(['type' => ShopDiscount::TYPE_BRAND, 'value' => $event->brand->id]);
    }
}
