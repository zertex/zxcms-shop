<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

use common\modules\shop\entities\product\ShopProduct;
//use Elasticsearch\ClientBuilder;
//use app\modules\shop\services\search\ProductIndexer;
use yii\base\Event;

class ShopProductSavedEvent extends Event
{
    public $product;

    public function __construct(ShopProduct $product, array $config = [])
    {
        parent::__construct($config);
        $this->product = $product;
    }

    public function ShopProductSaved($event)
    {
        /*$client = $client = ClientBuilder::create()->build();
        $params = [
            'id' => $event->product->id,
            'index' => 'shop',
            'type' => 'products',
        ];
        $exists = $client->exists($params);
        $indexer = new ProductIndexer($client);
        // indexing if not exists and active
        if (!$exists && $event->product->isActive()) {
            $indexer->index($event->product);
        }
        // reindex if exists and active
        elseif ($exists && $event->product->isActive())
        {
            $indexer->remove($event->product);
            $indexer->index($event->product);
        }
        // remove id existas and draft
        elseif ($exists && $event->product->isDraft())
        {
            $indexer->remove($event->product);
        }
*/
    }
}
