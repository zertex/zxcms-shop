<?php
/**
 * Created by Error202
 * Date: 31.08.2017
 */

namespace common\modules\shop\events;

//use core\entities\user\User;
use common\modules\shop\entities\order\ShopOrder;
//use Elasticsearch\ClientBuilder;
//use app\modules\shop\services\search\OrderIndexer;
use yii\base\Event;

class ShopOrderDeleteEvent extends Event
{
    public $order;

    public function __construct(ShopOrder $order, array $config = [])
    {
        parent::__construct($config);
        $this->order = $order;
    }

    public function ShopOrderDelete($event)
    {
        /*$client = $client = ClientBuilder::create()->build();

        $params = [
            'id' => $event->order->id,
            'index' => 'shop-orders',
            'type' => 'orders',
        ];

        $exists = $client->exists($params);

        $indexer = new OrderIndexer($client);

        if ($exists)
        {
            $indexer->remove($event->order);
        }*/
    }
}
